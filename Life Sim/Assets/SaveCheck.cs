﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.IO;

public class SaveCheck : MonoBehaviour
{
    void Start()
    {
        if (File.Exists(Application.persistentDataPath + "/lifeSim.Life"))
        {
            LoadScreenManager.LoadScene(4);
            SaveLoad.saveLoad.Load();
            Debug.Log("Found save, loaded automatically!");
        }
        else
        {
            SceneManager.LoadScene(1);
            Debug.Log("No save found, starting new game.");
        }
    }
}
