﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HigherLowerMinigame : MonoBehaviour
{
    public GameObject[] cards;
    public GameObject[] tableCards;

    public float[] rewards;

    public HighLowCard current;
    public HighLowCard last;

    public GameObject higherButton;
    public GameObject lowerButton;
    public GameObject startButton;
    public GameObject cashoutButton;
    public GameObject quitButton;

    public Sprite cardBack;

    public Text rewardText;

    public int points;

    public float bet;

    public float reward;

    void Start()
    {
        higherButton.SetActive(false);
        lowerButton.SetActive(false);
    }

    void Update()
    {
        if (PlayerStats.money >= 25)
        {
            startButton.GetComponent<Button>().interactable = true;
        }
        else
        {
            startButton.GetComponent<Button>().interactable = false;
        }
    }

    public void StartGame()
    {
        PlayerStats.money -= 25;

        foreach (GameObject card in tableCards)
        {
            card.GetComponent<Image>().sprite = cardBack;
        }

        points = 0;
        last = null;

        current = PickRandomCard();
        tableCards[0].GetComponent<Image>().sprite = current.icon;


        higherButton.SetActive(true);
        lowerButton.SetActive(true);
        startButton.SetActive(false);
        quitButton.SetActive(false);

        rewardText.text = "£0.00";
    }

    HighLowCard PickRandomCard()
    {
        return cards[Random.Range(0, (cards.Length))].GetComponent<HighLowCard>();
    }

    public void Higher()
    {
        higherButton.SetActive(false);
        lowerButton.SetActive(false);

        last = current;
        current = PickRandomCard();
        tableCards[points + 1].GetComponent<Image>().sprite = current.icon;

        if (current.value >= last.value)
        {
            Win();
        }
        else
        {
            Lose();
        }
    }

    public void Lower()
    {
        higherButton.SetActive(false);
        lowerButton.SetActive(false);

        last = current;
        current = PickRandomCard();
        tableCards[points + 1].GetComponent<Image>().sprite = current.icon;

        if (current.value <= last.value)
        {
            Win();
        }
        else
        {
            Lose();
        }
    }

    void Win()
    {
        cashoutButton.SetActive(true);

        points++;

        if (points < 10)
        {
            higherButton.SetActive(true);
            lowerButton.SetActive(true);
        }
        reward = rewards[points];

        rewardText.text = "Reward: £" + reward.ToString("F2");

        quitButton.SetActive(true);
    }

    void Lose()
    {
        cashoutButton.SetActive(false);

        rewardText.text = "You Lose!";
        startButton.SetActive(true);

        quitButton.SetActive(true);
    }

    public void Cashout()
    {
        PlayerStats.money += reward;

        startButton.SetActive(true);
        cashoutButton.SetActive(false);

        higherButton.SetActive(false);
        lowerButton.SetActive(false);

        rewardText.text = "You cashed out!";
    }
}
