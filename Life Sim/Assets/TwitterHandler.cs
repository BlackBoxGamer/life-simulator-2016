﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TwitterHandler : MonoBehaviour
{
    public Button tweetUs;
    public Button followButton;

    public bool brandon;
    public bool alfie;
    public bool klaus;
    public bool aaron;
    public bool FVG;

    private const string TWITTER_ADDRESS = "http://twitter.com/intent/tweet";
    private const string FOLLOW_ADDRESS = "https://twitter.com/intent/follow";
    private const string TWEET_LANGUAGE = "en";

    void Start()
    {
        if (brandon && PlayerStats.followedBrandon)
            followButton.interactable = false;
        if (alfie && PlayerStats.followedAlfie)
            followButton.interactable = false;
        if (klaus && PlayerStats.followedKlaus)
            followButton.interactable = false;
        if (aaron && PlayerStats.followedAaron)
            followButton.interactable = false;
        if (FVG && PlayerStats.followedFVG)
            followButton.interactable = false;
    }

    public void ShareToTwitter()
    {
        Application.OpenURL(TWITTER_ADDRESS +
                    "?text=" + WWW.EscapeURL("I'm playing Life Simulator 2016! #lifesim Download it here! https://play.google.com/store/apps/details?id=com.FVG.LifeSim") +
                    "&amp;lang=" + WWW.EscapeURL(TWEET_LANGUAGE));

        PlayerStats.money += 100;
        tweetUs.interactable = false;
        PlayerStats.tweetedUs = true;
    }

    public void ShareFeedback()
    {
        Application.OpenURL(TWITTER_ADDRESS +
            "?text=" + WWW.EscapeURL("@Brannydonowt #lifesim Feedback - ") +
            "&amp;lang=" + WWW.EscapeURL(TWEET_LANGUAGE));

        PlayerStats.money += 25;
        tweetUs.interactable = false;
        PlayerStats.tweetedUs = true;
    }

    public void OpenTwitter(string USER_ID)
    {
        Application.OpenURL(FOLLOW_ADDRESS +
            "?user_id=" + USER_ID);

        PlayerStats.money += 50;
        followButton.interactable = false;

        if (aaron)
            PlayerStats.followedAaron = true;
        if (alfie)
            PlayerStats.followedAlfie = true;
        if (brandon)
            PlayerStats.followedBrandon = true;
        if (klaus)
            PlayerStats.followedKlaus = true;
    }
}
