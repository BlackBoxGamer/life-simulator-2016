﻿using UnityEngine;
using System.Collections;

public class FirstTimeLoadUp : MonoBehaviour
{
    public bool welcomePanel;

    public GameObject homePanel;

    public LevelManager LM;

    void Awake()
    {
        LM = GameObject.FindGameObjectWithTag("LevelManager").GetComponent<LevelManager>();

        if (PlayerStats.timesPlayed > 1)
        {
            gameObject.SetActive(false);

            if (welcomePanel)
            {
                LM.activePanel = homePanel;
                homePanel.SetActive(true);
            }
        }
    }
}
