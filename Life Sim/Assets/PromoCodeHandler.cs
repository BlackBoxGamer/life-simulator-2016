﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PromoCodeHandler : MonoBehaviour
{
    public string code;

    public InputField input;

    public string[] codes;

    public GameObject rewardPanel;
    public Text rewardTitle;
    public Text rewardDesc;

    int pos = 0;

    public void TryCode()
    {
        foreach (string c in codes)
        {
            if (input.text == c)
            {
                code = codes[pos];
                CheckReward();
                break;
            }
            else
            {
                if (pos > codes.Length)
                {
                    ShowReward("NOT VALID", "You have entered an invalid code. Codes can only be redeemed once and are found on our Twitter pages.");
                    pos = 0;
                }
                else
                {
                    pos++;
                }
            }
        }
    }

    void CheckReward()
    {
        if (pos == 0 && PlayerStats.code1 == false)
        {
            // Reward 1
            PlayerStats.code1 = true;
            MoneyReward(150f);
            ShowReward("Success!", "You just gained £150! Be sure to check Twitter often for more codes!");
        }
        else if (pos == 0 && PlayerStats.code1 == true)
        {
            ShowReward("Already Redeemed", "This code has already been redeemed. Keep a look out for more codes being released on Twitter.");
        }

        if (pos == 1 && PlayerStats.code2 == false)
        {
            // Reward 2
            PlayerStats.code2 = true;
            MoneyReward(150);
            ShowReward("Success!", "You just gained £150! Be sure to check Twitter often for more codes!");
        }
        else if (pos == 1 && PlayerStats.code2 == true)
        {
            ShowReward("Already Redeemed", "This code has already been redeemed. Keep a look out for more codes being released on Twitter.");
        }

        if (pos == 2 && PlayerStats.code3 == false)
        {
            // Reward 3
            PlayerStats.code3 = true;
            FoodReward(25, 25);
            ShowReward("Success!", "You just gained 25 Food and 25 Drinks! Don't eat them all at once!");
        }
        else if (pos == 2 && PlayerStats.code3 == true)
        {
            ShowReward("Already Redeemed", "This code has already been redeemed. Keep a look out for more codes being released on Twitter.");
        }

        if (pos == 3 && PlayerStats.code4 == false)
        {
            // Reward 4
            PlayerStats.code4 = true;
            GymReward(30);
            ShowReward("Success!", "You just gained 30 days free gym membership, enjoy! (If you are homeless, it will apply once you get a home.");
        }
        else if (pos == 3 && PlayerStats.code4 == true)
        {
            ShowReward("Already Redeemed", "This code has already been redeemed. Keep a look out for more codes being released on Twitter.");
        }

        if (pos == 4 && PlayerStats.code5 == false)
        {
            // Reward 5
            PlayerStats.code5 = true;
            RentReward(14);
            ShowReward("Success!", "You have just been given a 14 day extend on your rent bill! (This only applies if you have a home.");
            
        }
        else if (pos == 4 && PlayerStats.code5 == true)
        {
            ShowReward("Already Redeemed", "This code has already been redeemed. Keep a look out for more codes being released on Twitter.");
        }

        if (pos == 5)
        {
            DeveloperReward();
            ShowReward("Dev mode enabled", "You have successfully enabled developer mode.");
        }

        pos = 0;
    }

    void ShowReward(string title, string description)
    {
        rewardTitle.text = title;
        rewardDesc.text = description;

        rewardPanel.SetActive(true);
    }

    void MoneyReward(float money)
    {
        PlayerStats.money += money;
    }

    void FoodReward(int drinks, int food)
    {
        PlayerInventory.numEnergyDrinks += drinks;
        PlayerInventory.numFood += food;
    }

    void RentReward(int days)
    {
        PlayerStats.rentPayDay = PlayerStats.days + days;
    }

    void GymReward(int days)
    {
        PlayerInventory.hasGymMembership = true;
        PlayerStats.gymRenewalDay = PlayerStats.days + days;
    }

    void JobReward(int id)
    {
        PlayerStats.jobId = id;
    }

    void DeveloperReward()
    {
        PlayerStats.money = 99999999;
        PlayerStats.knowledge = 9999999;
        PlayerStats.biology = 99999;
        PlayerStats.history = 99999;
        PlayerStats.physics = 99999;
        PlayerStats.strength = 99999;
        PlayerStats.chemistry = 99999;
        PlayerStats.business = 999999;
        PlayerStats.ict = 999999;
        PlayerStats.maths = 99999;
        PlayerStats.english = 999999;
        PlayerStats.geography = 999999;
        PlayerStats.computing = 999999;
        PlayerStats.creativity = 999999;
        PlayerStats.experience = 99999;
        PlayerInventory.numFood = 9999;
        PlayerInventory.numEnergyDrinks = 99999;
    }
}
