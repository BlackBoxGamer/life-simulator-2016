﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class MiniGestureRecognizer : MonoBehaviour
{
    public SwipeControl SwipeControl;

    public bool homeOptionsScreen;

    public EasyTween homeOptionsScreenGO; 
    public EasyTween statBarGO;

    void Start()
    {
        SwipeControl.GetComponent<SwipeControl>().SetMethodToCall(MyCallbackMethod);
    }

    public void MyCallbackMethod(SwipeControl.SWIPE_DIRECTION iDirection)
    {
        switch (iDirection)
        {
            case SwipeControl.SWIPE_DIRECTION.SD_LEFT:
                if (homeOptionsScreenGO.IsObjectOpened())
                {
                    homeOptionsScreenGO.OpenCloseObjectAnimation();
                }
                break;

            case SwipeControl.SWIPE_DIRECTION.SD_RIGHT:
                if (!homeOptionsScreenGO.IsObjectOpened())
                {
                    homeOptionsScreenGO.OpenCloseObjectAnimation();
                }
                break;
        }
    }

}