﻿using UnityEngine;
using System.Collections;
using UberAudio;
public class PlaySoundOnClick : MonoBehaviour
{

    public string soundToPlay;

    public void Click()
    {
        UberAudio.AudioManager.Instance.Play(soundToPlay);
       
    }
}
