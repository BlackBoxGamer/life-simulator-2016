﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InvertColours : MonoBehaviour
{
    public LevelManager LM;

    public bool inverted = false;

    public GameObject selectedImage;
    public Button myButton;

    void Start()
    {
        selectedImage = transform.parent.GetChild(2).gameObject;
    }

    public void Invert()
    {
        if (LM.activeButton != null)
        {
            LM.activeButton.GetComponent<InvertColours>().selectedImage.SetActive(false);
            LM.activeButton.GetComponent<InvertColours>().inverted = false;
        }
        if (!inverted)
        {
            selectedImage.SetActive(true);
            inverted = !inverted;
        }
        else if (inverted)
        {
            selectedImage.SetActive(false);
            inverted = !inverted;
        }

        if (GetComponent<InvertColours>() != null)
        {
            LM.activeButton = this.gameObject;
        }
        else
        {
            LM.activeButton = this.transform.parent.gameObject;
        }
    }

    
}
