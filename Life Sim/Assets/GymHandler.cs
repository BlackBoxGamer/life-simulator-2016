﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class GymHandler : MonoBehaviour
{
    public GameObject membershipPanel;

    public void BuyMembership()
    {
        PlayerStats.money -= 15f;
        PlayerStats.gymMembership = 15;
        PlayerStats.gymRenewalDay = PlayerStats.days + 8;
        PlayerInventory.hasGymMembership = true;
    }

    public void DeclineMembership()
    {
        GetComponent<CloseTab>().Open();
        membershipPanel.SetActive(false);
    }
}

