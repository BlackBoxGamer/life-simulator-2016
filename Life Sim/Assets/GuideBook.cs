﻿using UnityEngine;
using System.Collections;

public class GuideBook : MonoBehaviour
{
    public GameObject current;


    public void SwitchPanel(GameObject next)
    {
        if (current != null)
            current.SetActive(false);

        next.SetActive(true);
        current = next;
    }
}
