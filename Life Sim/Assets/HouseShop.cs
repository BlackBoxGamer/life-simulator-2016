﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HouseShop : MonoBehaviour
{
    public float initialCost;
    public float weeklyRent;

    public string propertyName;
    public string propertyDesc;
    public Sprite propertyPreview;
    public Button propertyButton;
    public Image buttonImage;

    public Text titleText;
    public Text descText;
    public Text initCostText;
    public Text rentText;
    public Image previewImage;

    public Color cantBuy;
    public Color canBuy;

    public bool property1;
    public bool property2;
    public bool property3;

    void Start()
    {
        buttonImage = propertyButton.gameObject.GetComponent<Image>();
    }

    public void BuyHouse()
    {
        PlayerStats.money -= initialCost;
        PlayerStats.rent = weeklyRent;
        PlayerStats.rentPayDay = PlayerStats.days + 8;

        titleText.gameObject.SetActive(false);
        descText.gameObject.SetActive(false);
        previewImage.gameObject.SetActive(false);
        rentText.gameObject.SetActive(false);
        initCostText.gameObject.SetActive(false);
        propertyButton.gameObject.SetActive(false);
    }

    public void ShowInfo()
    {
        titleText.gameObject.SetActive(true);
        descText.gameObject.SetActive(true);
        previewImage.gameObject.SetActive(true);
        rentText.gameObject.SetActive(true);
        initCostText.gameObject.SetActive(true);

        titleText.text = propertyName;
        descText.text = propertyDesc;
        previewImage.sprite = propertyPreview;
        rentText.text = "Weekly rent: £" + weeklyRent;
        initCostText.text = "Initial Cost: £" + initialCost;

        if (property1 && PlayerInventory.hasLevel1Home)
        {
            propertyButton.interactable = false;
            buttonImage.color = cantBuy;
            propertyButton.gameObject.GetComponentInChildren<Text>().text = "Owned";
            propertyButton.gameObject.GetComponentInChildren<Text>().color = Color.white;
        }
        else if (property2 && PlayerInventory.hasLevel2Home)
        {
            propertyButton.interactable = false;
            buttonImage.color = cantBuy;
            propertyButton.gameObject.GetComponentInChildren<Text>().text = "Owned";
            propertyButton.gameObject.GetComponentInChildren<Text>().color = Color.white;
        }
        else if (property3 && PlayerInventory.hasLevel3Home)
        {
            propertyButton.interactable = false;
            buttonImage.color = cantBuy;
            propertyButton.gameObject.GetComponentInChildren<Text>().text = "Owned";
            propertyButton.gameObject.GetComponentInChildren<Text>().color = Color.white;
        }
        else if (PlayerStats.money >= initialCost)
        {
            if (PlayerStats.hasHome)
            {
                propertyButton.interactable = true;
                buttonImage.color = canBuy;
                propertyButton.gameObject.GetComponentInChildren<Text>().text = "Upgrade";
                propertyButton.gameObject.GetComponentInChildren<Text>().color = Color.black;
            }
            else
            {
                propertyButton.interactable = true;
                buttonImage.color = canBuy;
                propertyButton.gameObject.GetComponentInChildren<Text>().text = "Buy";
                propertyButton.gameObject.GetComponentInChildren<Text>().color = Color.black;
            }
        }
        else
        {
            if (PlayerStats.hasHome)
            {
                propertyButton.interactable = false;
                buttonImage.color = cantBuy;
                propertyButton.gameObject.GetComponentInChildren<Text>().text = "Upgrade";
                propertyButton.gameObject.GetComponentInChildren<Text>().color = Color.white;
            }
            else
            {
                propertyButton.interactable = false;
                buttonImage.color = cantBuy;
                propertyButton.gameObject.GetComponentInChildren<Text>().text = "Buy";
                propertyButton.gameObject.GetComponentInChildren<Text>().color = Color.white;
            }
        }

        propertyButton.gameObject.SetActive(true);
    }
}
