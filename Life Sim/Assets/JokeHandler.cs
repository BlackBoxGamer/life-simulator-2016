﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class JokeHandler : MonoBehaviour
{
    public string[] jokes;
    public string[] jokesAnswers;

    public string[] safeJokes;
    public string[] safeAnswers; 

    public Text jokeText;
    public Text jokeAnswerText;

    public int numJokes;
    public int numSafeJokes;

    public void OpenPanel()
    {
        if (PlayerStats.disableOffensiveJokes)
        {
            int joke = Random.Range(0, numJokes - 1);
            jokeText.text = jokes[joke];
            jokeAnswerText.text = jokesAnswers[joke];
        }
        else
        {
            int safeJoke = Random.Range(0, numSafeJokes - 1);
            jokeText.text = safeJokes[safeJoke];
            jokeAnswerText.text = safeAnswers[safeJoke];
        }
    }



}
