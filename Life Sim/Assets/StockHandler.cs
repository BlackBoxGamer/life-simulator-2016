﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab.ClientModels;
using PlayFab;
using MovementEffects;
using UnityEngine.UI;

public class StockHandler : MonoBehaviour 
{
	// This script handles the stocks - allowing for purchasing, selling, UI handling and everything else.

	// This is a list of stock entries, each will be enabled if a position is needed.
	public StockEntry[] stockSectionSpaces;

	// This is the stock that is currently selected.
	public StockEntry currentlySelectedStock;

	// This is the loadscreen
	public GameObject loadScreen;

	// This handles the coroutine timing.
	float numTimesRan;

	//This is for conversions
	public InputField userInput;

	public int conversionRate;
	public int outputValue;

	public Text outputText;
	public Text currentTokenText;

	public Button convertButton;

	public int numTokens;

	void Start()
	{
		LoadStocks ();
	}

	public void LoadStocks()
	{
		GetCatalogItemsRequest request = new GetCatalogItemsRequest () {
			CatalogVersion = "Stocks",
		};

		PlayFabClientAPI.GetCatalogItems (request, (result) => {
			int pos = 0;
			numTimesRan = 0;

			foreach (var i in result.Catalog) {
				StockEntry item = stockSectionSpaces [pos];

				item.itemID = i.ItemId;
				item.itemClass = i.ItemClass;
				item.tag = i.Tags [0];
				item.DisplayName = i.DisplayName;
				item.Description = i.Description;
				item.imageUrl = i.ItemImageUrl;
				uint cost = 0;
				i.VirtualCurrencyPrices.TryGetValue ("ST", out cost);
				item.cost = (int)cost;
				item.customData = i.CustomData;
				item.catalogVersion = i.CatalogVersion;

				Image img = item.transform.GetChild (1).GetComponent<Image> ();

				numTimesRan += 0.1f;
				Timing.RunCoroutine (_GetImage (i.ItemImageUrl, img, item.gameObject));

				pos++;
			}
			loadScreen.SetActive (false);
		},
			(error) => {
				Debug.Log ("Error creating a list of catalogue items");
				Debug.Log (error.ErrorMessage);
				loadScreen.SetActive (false);
			});
	}

	public void PurchaseStock()
	{
		loadScreen.SetActive (true);

		PurchaseItemRequest request = new PurchaseItemRequest () {
			ItemId = currentlySelectedStock.itemID,
			VirtualCurrency = "ST",
			Price = currentlySelectedStock.cost,
			CatalogVersion = currentlySelectedStock.catalogVersion,
		};

		PlayFabClientAPI.PurchaseItem (request, (result) => 
			{
				Debug.Log("Player succesfully bought: " + currentlySelectedStock.DisplayName);
				PlayerStats.stockTokens -= currentlySelectedStock.cost;
				currentlySelectedStock.SelectStock();
			},
			(error) => {
				Debug.Log ("Error Purchasing");
				Debug.Log(error.ErrorMessage);
				loadScreen.SetActive(false);
			});
	}

	void ConsumeRequest(StockEntry item, int num)
	{
		ConsumeItemRequest request = new ConsumeItemRequest () {
			ItemInstanceId = item.instanceId,
			ConsumeCount = num,
		};

		PlayFabClientAPI.ConsumeItem (request, (result) => 
			{
				CalculateProfit(num);
			},
			(error) => {
				Debug.Log ("Error Selling Stock");
				Debug.Log(error.ErrorMessage);
				loadScreen.SetActive (false);
			});
	}

	public void SellStock()
	{
		loadScreen.SetActive (true);
		ConsumeRequest (currentlySelectedStock, 1);
	}

	public void SellAllStocks()
	{
		loadScreen.SetActive (true);
		ConsumeRequest (currentlySelectedStock, currentlySelectedStock.numOfItem);
	}
		
	void CalculateProfit(int num)
	{
		int profit = currentlySelectedStock.cost * num;

		AddCurrency (profit);
	}

	void AddCurrency(int amount)
	{
		AddUserVirtualCurrencyRequest request = new AddUserVirtualCurrencyRequest () {
			VirtualCurrency = "ST",
			Amount = amount,
		};

		PlayFabClientAPI.AddUserVirtualCurrency (request, (result) => {
			Debug.Log ("Successfully updated user currency balance");
			PlayerStats.stockTokens = result.Balance;
			currentlySelectedStock.SelectStock();
		}, (error) => {
			Debug.Log (error.ErrorMessage);
			loadScreen.SetActive(false);
		});
	}

	IEnumerator<float> _GetImage(string url, Image img, GameObject obj)
	{
		yield return Timing.WaitForSeconds (numTimesRan);

		WWW www = new WWW(url);
		yield return Timing.WaitUntilDone(www);
		img.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
		img.gameObject.transform.parent.gameObject.SetActive (true);
		obj.GetComponent<StockEntry> ().icon = img.sprite;
		www.Dispose();
		www = null;
	}

	// This portion of the script is dedicated to the conversion functionality.
	public void LoadConversionPanel()
	{
		currentTokenText.text = PlayerStats.stockTokens.ToString();

		userInput.text = PlayerStats.stockTokens.ToString();

		ReadInput ();
	}

	public void ReadInput()
	{
		numTokens = int.Parse(userInput.text);
		outputValue = numTokens * conversionRate;

		outputText.text = "£" + outputValue.ToString ();

		if (numTokens <= PlayerStats.stockTokens && numTokens > 25)
			convertButton.interactable = true;
		else
			convertButton.interactable = false;
	}

	public void Convert()
	{
		loadScreen.SetActive (true);

		SubtractUserVirtualCurrencyRequest request = new SubtractUserVirtualCurrencyRequest () {
			VirtualCurrency = "ST",
			Amount = numTokens,
		};

		PlayFabClientAPI.SubtractUserVirtualCurrency (request, (result) => {
			Debug.Log ("Successfully updated user Stock Token balance");
			PlayerStats.stockTokens = result.Balance;
			AddPound();
		}, (error) => {
			Debug.Log (error.ErrorMessage);
			loadScreen.SetActive(false);
		});
	}

	void AddPound()
	{
		AddUserVirtualCurrencyRequest request = new AddUserVirtualCurrencyRequest () {
			VirtualCurrency = "PO",
			Amount = outputValue,
		};

		PlayFabClientAPI.AddUserVirtualCurrency (request, (result) => {
			Debug.Log ("Successfully updated user Pound balance");
			PlayerStats.money = result.Balance;
			loadScreen.SetActive(false);
			LoadConversionPanel();
		}, (error) => {
			Debug.Log (error.ErrorMessage);
			loadScreen.SetActive(false);
		});
	}

}
