﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RandomEventHandler : MonoBehaviour
{
    // This script handles all random events in the game.
    // To Do: 
    // - Make nice UI with popup animation that will display the event title, description and what happens.
    // - Make a chance of events happening every hour, then chances of events happening any using Random.Range.

    public bool eventShowing;

    public GameObject eventPanel;
    public EasyTween eventPanelAnim;
    public Text eventTitleText;
    public Text eventDescText;

    public AudioClip notificationSound;
    public AudioSource audio;

    public int nextEventDay = 0;

    void Start()
    {
        eventPanelAnim = eventPanel.GetComponent<EasyTween>();
        eventTitleText = eventPanel.transform.GetChild(1).GetComponent<Text>();
        eventDescText = eventPanel.transform.GetChild(2).GetComponent<Text>();

        audio = GetComponent<AudioSource>();
    }

    public void CloseEvent()
    {
        StartCoroutine("Timer");
    }

    public IEnumerator Timer()
    {
        eventPanelAnim.OpenCloseObjectAnimation();

        yield return new WaitForSeconds(0.3f);

        eventPanel.SetActive(false);
        eventShowing = false;

        StopAllCoroutines();

    }

    public void OnTimeChanged()
    {
        if (nextEventDay <= PlayerStats.days)
        {
            int playEvent = Random.Range(0, 31);

            if (playEvent == 1)
            {
                int eventType = Random.Range(0, 101);

                if (eventType <= 25)
                {
                    CommonGoodEvent();
                }
                else if (eventType <= 50 && eventType > 25)
                {
                    CommonBadEvent();
                }
                else if (eventType <= 65 && eventType > 50)
                {
                    RareGoodEvent();
                }
                else if (eventType <= 80 && eventType > 65)
                {
                    RareBadEvent();
                }
                else if (eventType <= 85 && eventType > 80)
                {
                    if (PlayerStats.days >= 65)
                        SuperRareGoodEvent();
                }
                else if (eventType <= 89 && eventType > 85)
                {
                    if (PlayerStats.days >= 65)
                        SuperRareBadEvent();
                }
                else if (eventType == 99)
                {
                    if (PlayerStats.days >= 150)
                        ExtremelyRareGoodEvent();
                }
                else if (eventType == 100)
                {
                    if (PlayerStats.days >= 150)
                        ExtremelyRareBadevent();
                }

                nextEventDay = PlayerStats.days + 3;
            }
        }
    }

    void CommonGoodEvent()
    {
        // Always set 1 above number of events you have.
        int eventNum = Random.Range(0, 11);

        PlayEvent();

        Debug.Log("Common Good Event");

        if (eventNum == 0)
        {
            eventTitleText.text = "Free Cash!";
            eventDescText.text = "You found a " + PlayerStats.currency + "20 note on the floor, hooray! \n \n +" + PlayerStats.currency + "20";
            PlayerStats.money += 20;
        }

        if (eventNum == 1)
        {
            eventTitleText.text = "Lucky Learner";
            eventDescText.text = "You find a book on the floor and decide to give it a read \n \n +2 Knowledge!";
            PlayerStats.knowledge += 2;
        }

        if (eventNum == 2)
        {
            eventTitleText.text = "Friend Finder";
            eventDescText.text = "You meet somebody on the street \n \n +1 Social";
            PlayerStats.social += 1;
        }
        if (eventNum == 3)
        {
            eventTitleText.text = "Aarons Approval";
            eventDescText.text = "Your friend Aaron approves of you \n \n +20 Mood";
            PlayerStats.mood += 20;
        }
        if (eventNum == 4)
        {
            eventTitleText.text = "Lucky Day";
            eventDescText.text = "Today's your lucky day! You feel super-confident and win on a scratchcard. \n \n +" + PlayerStats.currency + "5";
            PlayerStats.money += 5;
        }
        if (eventNum == 5)
        {
            eventTitleText.text = "Half Life 3";
            eventDescText.text = "You magically progress an extra day! Weird?! \n \n + 1 Day";
            PlayerStats.days += 1;
        }
        if (eventNum == 6)
        {
            eventTitleText.text = "Gabens Gift";
            eventDescText.text = "You are inspired by the work of others, you feel that you can do much more now \n \n +2 Creativity";
            PlayerStats.creativity += 1;
        }
        if (eventNum == 7)
        {
            eventTitleText.text = "Mathematical Mood";
            eventDescText.text = "You're in the mood to get some maths done, so you do! \n \n +1 Maths";
            PlayerStats.maths += 1;
        }

        if (eventNum == 8)
        {
            eventTitleText.text = "Shakespeare's apprentice";
            eventDescText.text = "Who art thou? You decide to read up on some literature \n \n +1 English";
            PlayerStats.english += 1;
        }

        if (eventNum == 9)
        {
            eventTitleText.text = "Geo Guesser";
            eventDescText.text = "There's an awesome game online that you decide to try out, now you know more about the world! \n \n +1 Geography";
            PlayerStats.days += 1;
        }

        if (eventNum == 10)
        {
            eventTitleText.text = "Elemental Hero";
            eventDescText.text = "Today you can harness the powers of the earth, the 3 most powerful tools to man. \n \n +1 Physics \n +1 Chemistry \n +1 Biology";
            PlayerStats.chemistry += 1;
            PlayerStats.biology += 1;
            PlayerStats.physics += 1;
        }
    }

    void CommonBadEvent()
    {
        // Number of possible events
        int eventNum = Random.Range(0, 9);

        PlayEvent();

        Debug.Log("Common Bad Event");

        if (eventNum == 0)
        {
            eventTitleText.text = "Pickpocketed!";
            eventDescText.text = "Some money was pinched from your pocket! \n \n -" + PlayerStats.currency + "15";
            PlayerStats.money -= 15;
        }

        if (eventNum == 1)
        {
            eventTitleText.text = "Head Banger";
            eventDescText.text = "You fall and bang your head on the road! \n \n -2 Knowledge";
            PlayerStats.knowledge -= 2;
        }

        if (eventNum == 2)
        {
            eventTitleText.text = "Pointless Pull-Over";
            eventDescText.text = "A police officer pulls you over for no reason \n \n -25 Mood";
            PlayerStats.mood -= 25;
        }
        if (eventNum == 3)
        {
            eventTitleText.text = "A Good Deed";
            eventDescText.text = "You buy some food for a homeless person \n \n -" + PlayerStats.currency + "5 \n +10 Life Score";
            PlayerStats.money -= 5;
            PlayerStats.lifeScore += 10;
        }
        if (eventNum == 4)
        {
            eventTitleText.text = "MW2 Feelings";
            eventDescText.text = "You thought you saw a ghost \n \n -10 Mood";
            PlayerStats.mood -= 10;
        }
        if (eventNum == 5)
        {
            eventTitleText.text = "Forget-me-not";
            eventDescText.text = "You seem to have forgotten something... I can't remember what it is either \n \n -1 Knowledge";
            PlayerStats.knowledge -= 1;
        }
        if (eventNum == 6)
        {
            eventTitleText.text = "Birthday Wishes!";
            eventDescText.text = "A friend of your has their birthday today, you're going to have to send them some money... \n \n -" + PlayerStats.currency + "20";
            PlayerStats.money -= 20;
        }
        if (eventNum == 7)
        {
            eventTitleText.text = "A Jar of Dirt";
            eventDescText.text = "You use your jar of dirt to hide something, but it goes missing! \n \n -25 Health";
            PlayerStats.health -= 25;
        }
        if (eventNum == 8)
        {
            eventTitleText.text = "An Arrow to the Knee";
            eventDescText.text = "I used to be a player like you, but then I took an... \n \n -25 Health \n -25 Energy";
            PlayerStats.health -= 25;
            PlayerStats.energy -= 25;
        }
    }

    void RareGoodEvent()
    {
        int eventNum = Random.Range(0, 8);

        PlayEvent();

        Debug.Log("Rare Good Event");

        if (eventNum == 0)
        {
            eventTitleText.text = "Bank Error!";
            eventDescText.text = "Your bank made a mistake, everybody has extra cash for some reason! \n \n +" + PlayerStats.currency + "75";
            PlayerStats.money += 75;
        }

        if (eventNum == 1)
        {
            eventTitleText.text = "Super Motivated";
            eventDescText.text = "A random pedestrian gives you an inspirational talk, you're pumped! \n \n + Energy Refilled";
            PlayerStats.energy = 100;
        }

        if (eventNum == 2)
        {
            eventTitleText.text = "Lost Property";
            eventDescText.text = "There's a 10-Pack of energy drinks! \n \n +10 Drinks";
            PlayerInventory.numEnergyDrinks += 10;
        }
        if (eventNum == 3)
        {
            eventTitleText.text = "Facebook Friended";
            eventDescText.text = "Somebody recommended you on Facebook! \n \n +5 Social";
            PlayerStats.social += 5;
        }
        if (eventNum == 4)
        {
            eventTitleText.text = "Todd Howard Power";
            eventDescText.text = "You inherit the powers of a creative genius \n \n +3 Creativity";
            PlayerStats.creativity += 3;
        }
        if (eventNum == 5)
        {
            eventTitleText.text = "Young, Wild & Free";
            eventDescText.text = "You're living life to the fullest, here's some Life Score \n \n +50 Life Score";
            PlayerStats.lifeScore += 50;
        }
        if (eventNum == 6)
        {
            eventTitleText.text = "I am Groot";
            eventDescText.text = "You felt inspired to work out, so you did! \n \n +3 Strength";
            PlayerStats.strength += 3;
        }
        if (eventNum == 7)
        {
            eventTitleText.text = "Chinatown Wars";
            eventDescText.text = "Damon the delivery man dropped off some noodles, said they were for you. \n \n +5 Food";
            PlayerInventory.numFood += 5;
        }

    }

    void RareBadEvent()
    {
        int eventNum = Random.Range(0, 10);

        PlayEvent();

        Debug.Log("Rare Bad Event");

        if (eventNum == 0)
        {
            eventTitleText.text = "Robbery Simulator";
            eventDescText.text = "You get mugged, the mugger knocks you out and steals your wallet \n \n + 2 Hours \n -" + PlayerStats.currency + PlayerStats.money / 6;
            PlayerStats.money -= PlayerStats.money / 6;
        }

        if (eventNum == 1)
        {
            eventTitleText.text = "Amateur Redditor";
            eventDescText.text = "Somebody told the boss that you were on Reddit at work! \n \n -2 Boss Happiness";
            PlayerStats.bossHappiness -= 2;
        }

        if (eventNum == 2)
        {
            eventTitleText.text = "A Slow Day";
            eventDescText.text = "You're feeling pretty groggy today, you don't want to do much... \n \n -40 Energy";
            PlayerStats.energy -= 40;
        }
        if (eventNum == 3)
        {
            eventTitleText.text = "Qasim Disease";
            eventDescText.text = "Your friend isn't happy with your options. He lectures you for some time \n \n +3 hours";
            PlayerStats.hours += 3;
        }
        if (eventNum == 4)
        {
            eventTitleText.text = "Natural Disaster";
            eventDescText.text = "There is an earthquake, you have to pay for damages \n \n -" + PlayerStats.currency + "100";
            PlayerStats.money -= 100;
        }
        if (eventNum == 5)
        {
            eventTitleText.text = "Dy'vine Intervention";
            eventDescText.text = "Some of your creative genius is stolen by a dyvine entity \n \n -2 Creativity";
            PlayerStats.creativity -= 2;
        }

        if (eventNum == 6)
        {
            eventTitleText.text = "Error Code: Darazs";
            eventDescText.text = "Your network is slower than usual so you struggle to communicate with friends \n \n -2 Social";
            PlayerStats.social -= 2;
        }

        if (eventNum == 7)
        {
            eventTitleText.text = "The Klaus";
            eventDescText.text = "A contract you signed a while ago had some unprecedented expenses \n \n -" + PlayerStats.currency + "75";
            PlayerStats.money -= 75;
        }
        if (eventNum == 8)
        {
            eventTitleText.text = "The Quote";
            eventDescText.text = "A childhood friend reminds you of something you said when you were younger, you feel ashamed \n \n -50 Mood";
            PlayerStats.mood -= 50;
        }
        if (eventNum == 9)
        {
            eventTitleText.text = "Scott's Hardware";
            eventDescText.text = "Your phone charger broke, so you went to the hardware store for a new one \n \n -" + PlayerStats.currency + "30";
            PlayerStats.money -= 30;
        }

    }

    void SuperRareGoodEvent()
    {
        int eventNum = Random.Range(0, 4);

        Debug.Log("Super Rare Good Event");

        PlayEvent();

        if (eventNum == 0)
        {
            eventTitleText.text = "New & Improved";
            eventDescText.text = "The Gods have decided to bestow great power upon you \n \n +5 All Stats";
            PlayerStats.strength += 5; 
            PlayerStats.knowledge += 5;
            PlayerStats.social += 5;
            PlayerStats.creativity += 5;
            // Maybe add more later
        }

        if (eventNum == 1)
        {
            if (PlayerInventory.hasMicrowave == false)
            {
                eventTitleText.text = "Strange Parcel";
                eventDescText.text = "A strange parcel arrived at your door, it's not yours but you signed for it anyway. \n \n + Microwave";
                PlayerInventory.hasMicrowave = true;
            }
            else
            {
                eventTitleText.text = "Another Microwave";
                eventDescText.text = "Once again a microwave has been delivered to your doorstep, how odd... May as well sell it - you've already got one! \n \n +" + PlayerStats.currency + "100";
                PlayerStats.money += 100;
            }
        }

        if (eventNum == 2)
        {
            eventTitleText.text = "Pot of Gold";
            eventDescText.text = "You found a pot of gold at the end of a rainbow! \n \n +" + PlayerStats.currency + "500";
            PlayerStats.money += 500;
        }
        if (eventNum == 3)
        {
            eventTitleText.text = "Frozen Vortex Gift";
            eventDescText.text = "A gift from the Gods of the game! \n \n +1000 Life Score";
            PlayerStats.lifeScore += 1000;
        }
    }

    void SuperRareBadEvent()
    {
        int eventNum = Random.Range(0, 3);

        Debug.Log("Super Rare Bad Event");

        PlayEvent();

        if (eventNum == 0)
        {
            eventTitleText.text = "Black Monday";
            eventDescText.text = "The stock markets have crashed - you're left with only half of your cash! \n \n -50% Cash";
            PlayerStats.money = PlayerStats.money / 2;
        }

        if (eventNum == 1)
        {
            eventTitleText.text = "Brain Dead";
            eventDescText.text = "An evil scientist has brainwashed your city, everybody is left with less knowledge! \n \n -25% Knowledge";
            PlayerStats.knowledge = PlayerStats.knowledge - (PlayerStats.knowledge / 4);
        }

        if (eventNum == 2)
        {
            eventTitleText.text = "The Strolling Dead";
            eventDescText.text = "A zombie virus has taken over your city, it's fixed now but most of your friend died \n \n -25% Social";
            PlayerStats.social = PlayerStats.social - (PlayerStats.social / 4);
        }
    }

    void ExtremelyRareGoodEvent()
    {
        int eventNum = Random.Range(0, 2);

        Debug.Log("Extremely Rare Good Event");

        PlayEvent();

        if (eventNum == 0)
        {
            eventTitleText.text = "Double Rollover!";
            eventDescText.text = "You did it! You won the lottery! \n \n +" + PlayerStats.currency + " A Lot!";
            PlayerStats.money = PlayerStats.money + 1000000;
        }

        if (eventNum == 1)
        {
            eventTitleText.text = "The Hawking Machine";
            eventDescText.text = "You become embodied with the power of a genius \n \n +150 Knowledge";
            PlayerStats.knowledge += 150;
        }
    }

    void ExtremelyRareBadevent()
    {
        int eventNum = Random.Range(0, 1);

        Debug.Log("Extremely Rare Bad Event");

        PlayEvent();

        if (eventNum == 0)
        {
            eventTitleText.text = "RIP Internet";
            eventDescText.text = "IT'S ALL GONE! Your life is going to suffer to adapt to the new ways of the world! \n \n -10 All Stats!";
            PlayerStats.strength -= 25;
            PlayerStats.knowledge -= 25;
            PlayerStats.social -= 25;
            PlayerStats.creativity -= 25;
        }
    }

    void PlayEvent()
    {
        eventShowing = true;
        eventPanel.SetActive(true);
        eventPanelAnim.OpenCloseObjectAnimation();
        audio.PlayOneShot(notificationSound);
    }
}
