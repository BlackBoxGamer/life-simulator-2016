﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class LotteryManager : MonoBehaviour
{
    public int num1 = 1;
    public int num2 = 2;
    public int num3 = 3;
    public int num4 = 4;
    public int num5 = 5;

    public int lotto1 = 1;
    public int lotto2 = 2;
    public int lotto3 = 3;
    public int lotto4 = 4;
    public int lotto5 = 5;

    public Text num1Text;
    public Text num2Text;
    public Text num3Text;
    public Text num4Text;
    public Text num5Text;

    int numbersCorrect;

    public Text lottoNum1Text;
    public Text lottoNum2Text;
    public Text lottoNum3Text;
    public Text lottoNum4Text;
    public Text lottoNum5Text;

    public Text yourNum1Text;
    public Text yourNum2Text;
    public Text yourNum3Text;
    public Text yourNum4Text;
    public Text yourNum5Text;

    public List<int> lottoNumbers; 

    int timer = 1000;

    public Text rewardText;

    public float reward;

    bool isNum2;
    bool isNum3;
    bool isNum4;
    bool isNum5;

    void Start()
    {
        num1Text.text = "1";
        num2Text.text = "2";
        num3Text.text = "3";
        num4Text.text = "4";
        num5Text.text = "5";
    }

    public void SelectNumbers()
    {
        PlayerStats.num1 = num1;
        PlayerStats.num2 = num2;
        PlayerStats.num3 = num3;
        PlayerStats.num4 = num4;
        PlayerStats.num5 = num5;
    }

    public void StartLotto()
    {
        yourNum1Text.text = PlayerStats.num1.ToString();
        yourNum2Text.text = PlayerStats.num2.ToString();
        yourNum3Text.text = PlayerStats.num3.ToString();
        yourNum4Text.text = PlayerStats.num4.ToString();
        yourNum5Text.text = PlayerStats.num5.ToString();

        InvokeRepeating("ChangeNumbers", 0, 0.01f);
    }

    void ChangeNumbers()
    {
        if (timer > 800)
        {
            lotto1 = lottoNumbers[Random.Range(0, 59)];
            lottoNumbers.Remove(lotto1);
            lottoNum1Text.text = lotto1.ToString();
        }

        if (timer > 600)
        {
            lotto2 = lottoNumbers[Random.Range(0, 59)];
            lottoNumbers.Remove(lotto2);
            lottoNum2Text.text = lotto2.ToString();
        }

        if (timer > 400)
        {
            lotto3 = lottoNumbers[Random.Range(0, 59)];
            lottoNumbers.Remove(lotto3);
            lottoNum3Text.text = lotto3.ToString();
        }

        if (timer > 200)
        {
            lotto3 = lottoNumbers[Random.Range(0, 59)];
            lottoNumbers.Remove(lotto3);
            lottoNum4Text.text = lotto4.ToString();
        }

        if (timer > 0)
        {
            lotto5 = lottoNumbers[Random.Range(0, 59)];
            lottoNumbers.Remove(lotto5);
            lottoNum5Text.text = lotto5.ToString();
        }
        else
        {
            CancelInvoke("ChangeNumbers");

            lottoNumbers.Add(lotto1);

            if (lottoNumbers.Contains(lotto2))
            {
                isNum2 = true;
                ValidateNumbers();
            } else { lottoNumbers.Add(lotto2); }
            if (lottoNumbers.Contains(lotto3))
            {
                isNum3 = true;
                ValidateNumbers();
            }
            else { lottoNumbers.Add(lotto3); }
            if (lottoNumbers.Contains(lotto4))
            {
                isNum4 = true;
                ValidateNumbers();
            }
            else { lottoNumbers.Add(lotto4); }
            if (lottoNumbers.Contains(lotto5))
            {
                isNum5 = true;
                ValidateNumbers();
            }
            else { lottoNumbers.Add(lotto5); }
            CheckLotto();
        }

        timer--;
    }

    void ValidateNumbers()
    {
        // Check The numbers for doubles
        if (lottoNumbers.Contains(lotto2) && isNum2)
        {
            lotto2 = Random.Range(1, 60);                                         
            lottoNum2Text.text = lotto2.ToString();
            lottoNumbers.Add(lotto2);
            isNum2 = false;
            Debug.Log("Number is duplicate, fixing.");
        }
        if (lottoNumbers.Contains(lotto3) && isNum3)
        {
            lotto3 = Random.Range(1, 60);
            lottoNum3Text.text = lotto3.ToString();
            lottoNumbers.Add(lotto3);
            isNum3 = false;
            Debug.Log("Number is duplicate, fixing.");
        }

        if (lottoNumbers.Contains(lotto4) && isNum4)
        {
            lotto4 = Random.Range(1, 60);
            lottoNum4Text.text = lotto4.ToString();
            lottoNumbers.Add(lotto4);
            isNum4 = false;
            Debug.Log("Number is duplicate, fixing.");
        }

        if (lottoNumbers.Contains(lotto5) && isNum5)
        {
            lotto5 = Random.Range(1, 60);
            lottoNum5Text.text = lotto5.ToString();
            lottoNumbers.Add(lotto5);
            isNum5 = false;
            Debug.Log("Number is duplicate, fixing.");
        }

        ChangeNumbers();
    }

    public void CheckLotto()
    {
        if (lottoNumbers.Contains(num1))
            numbersCorrect++;
        if (lottoNumbers.Contains(num2))
            numbersCorrect++;
        if (lottoNumbers.Contains(num3))
            numbersCorrect++;
        if (lottoNumbers.Contains(num4))
            numbersCorrect++;
        if (lottoNumbers.Contains(num5))
            numbersCorrect++;

        // Set reward

        if (numbersCorrect == 5)
        {
            reward = 1000000;
        }
        else if (numbersCorrect == 4)
        {
            reward = 750000;
        }
        else if (numbersCorrect == 3)
        {
            reward = 500000;
        }
        else if (numbersCorrect == 2)
        {
            reward = 50000;
        }
        else if (numbersCorrect == 1)
        {
            reward = 10000;
        }
        else
        {
            reward = 0;
        }

        rewardText.gameObject.SetActive(true);

        if (reward > 0)
        {
            rewardText.text = "Congratulations, you won £" + reward + "!";
        }
        else
        {
            rewardText.text = "Unlucky, you didn't win anything this time...";
        }

        PlayerStats.money += reward;

        lottoNumbers.Clear();
        numbersCorrect = 0;
        reward = 0;
        timer = 1000;
    }

    public void ChangeNum1(bool increase)
    {
        if (increase)
        {
            if (num1 < 59)
                num1++;
        }
        else
        {
            if (num1 > 1)
             num1--;
        }

        num1Text.text = num1.ToString();
    }

    public void ChangeNum2(bool increase)
    {
        if (increase)
        {
            if (num2 < 59)
                num2++;
        }
        else
        {
            if (num2 > 1)
                num2--;
        }

        num2Text.text = num2.ToString();
    }

    public void ChangeNum3(bool increase)
    {
        if (increase)
        {
            if (num3 < 59)
                num3++;
        }
        else
        {
            if (num3 > 1)
                num3--;
        }

        num3Text.text = num3.ToString();
    }

    public void ChangeNum4(bool increase)
    {
        if (increase)
        {
            if (num4 < 59)
                num4++;
        }
        else
        {
            if (num4 > 1)
                num4--;
        }

        num4Text.text = num4.ToString();
    }

    public void ChangeNum5(bool increase)
    {
        if (increase)
        {
            if (num5 < 59)
                num5++;
        }
        else
        {
            if (num5 > 1)
                num5--;
        }

        num5Text.text = num5.ToString();
    }
}
