﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ChallengeHandler : MonoBehaviour 
{
    [Space(5)]
    [Header("Challenge Components")]
	private Text titleText;
	private Text descriptionText;
	private Text rewardText;

	private Image completed;

	public Sprite tick;
	public Sprite cross;
	public Color green;
	public Color red;


    [Header("Challenge Information")]
	public string title;
	public string description;

	public bool hasCompleted;
	public int reward;

	public int challengeID;

	public int maxAmount;
	public int curAmount;

    [Header("Challenge Type")]
	public bool moneyMilestone;
    public bool moneySpentMilestone;
	public bool jobMilestone;
	public bool knowledgeMilestone;
	public bool billMilestone;
    public bool attendedClassesMilestone;
    public bool promotionsMilestone;
    public bool houseBought;

    [Header("Challenge ID")]
	public bool challenge1;
	public bool challenge2;
	public bool challenge3;
	public bool challenge4;
	public bool challenge5;
	public bool challenge6;
	public bool challenge7;
	public bool challenge8;
	public bool challenge9;
	public bool challenge10;
    public bool challenge11;
    public bool challenge12;
    public bool challenge13;
    public bool challenge14;
    public bool challenge15;
    public bool challenge16;
    public bool challenge17;
    public bool challenge18;
    public bool challenge19;
    public bool challenge20;

	void Awake()
	{
		titleText = transform.GetChild (0).GetComponent<Text>();
		descriptionText = transform.GetChild (1).GetComponent<Text> ();
		rewardText = transform.GetChild (2).GetComponent<Text> ();
		completed = transform.GetChild (3).GetComponent<Image> ();
	}

	void Start()
	{
        reward = reward *= (int)PlayerStats.conversionRate;

        if (moneyMilestone)
        {
            maxAmount = maxAmount *= (int)PlayerStats.conversionRate;
            description = "Make " + PlayerStats.currency + maxAmount.ToString() + "!";
        }
        if (moneySpentMilestone)
        {
            maxAmount = maxAmount *= (int)PlayerStats.conversionRate;
            description = "Spend " + PlayerStats.currency + maxAmount.ToString() + "!";
        }

        titleText.text = title;
		descriptionText.text = description;
		rewardText.text = "+" + PlayerStats.currency + reward;

		if (challenge1 && ChallengeManager.challenge1)
			Completed ();
		if (challenge2 && ChallengeManager.challenge2)
			Completed ();
		if (challenge3 && ChallengeManager.challenge3)
			Completed ();
		if (challenge4 && ChallengeManager.challenge4)
			Completed ();
		if (challenge5 && ChallengeManager.challenge5)
			Completed ();
		if (challenge6 && ChallengeManager.challenge6)
			Completed ();
		if (challenge7 && ChallengeManager.challenge7)
			Completed ();
		if (challenge8 && ChallengeManager.challenge8)
			Completed ();
		if (challenge9 && ChallengeManager.challenge9)
			Completed ();
		if (challenge10 && ChallengeManager.challenge10)
			Completed ();
        if (challenge11 && ChallengeManager.challenge11)
            Completed();
        if (challenge12 && ChallengeManager.challenge12)
            Completed();
        if (challenge13 && ChallengeManager.challenge13)
            Completed();
        if (challenge14 && ChallengeManager.challenge14)
            Completed();
        if (challenge15 && ChallengeManager.challenge15)
            Completed();
        if (challenge16 && ChallengeManager.challenge16)
            Completed();
        if (challenge17 && ChallengeManager.challenge17)
            Completed();
        if (challenge18 && ChallengeManager.challenge18)
            Completed();
        if (challenge19 && ChallengeManager.challenge19)
            Completed();
        if (challenge20 && ChallengeManager.challenge20)
            Completed();
	}
		
	public void ChallengeComplete()
	{
			hasCompleted = true;
			PlayerStats.money += reward;
			ChallengeManager.Set (challengeID);
	}

	public void Completed()
	{
		hasCompleted = true;
	}

	void Update()
	{
		if (hasCompleted && completed.sprite != tick) {
			completed.sprite = tick;
			completed.color = green;
		} else if (!hasCompleted && completed.sprite != cross) 
		{
			completed.sprite = cross;
			completed.color = red;
		}

		if (!hasCompleted) 
		{
			if (moneyMilestone) 
			{
				if (PlayerStats.money >= maxAmount) 
				{
					ChallengeComplete ();
				}
			}

            if (moneySpentMilestone)
            {
                if (PlayerStats.moneySpent >= maxAmount)
                {
                    ChallengeComplete();
                }
            }

			if (jobMilestone) 
			{
				if (PlayerStats.numJobs >= maxAmount) 
				{
					ChallengeComplete ();
				}
			}

			if (knowledgeMilestone) 
			{
				if (PlayerStats.knowledge >= maxAmount) 
				{
					ChallengeComplete ();
				}
			}

			if (billMilestone) 
			{
				if (PlayerStats.billsPayed >= maxAmount) 
				{
					ChallengeComplete ();
				}
			}

            if (attendedClassesMilestone)
            {
                if (PlayerStats.classesAttended >= maxAmount)
                {
                    ChallengeComplete();
                }
            }

            if (promotionsMilestone)
            {
                if (PlayerStats.promotions >= maxAmount)
                {
                    ChallengeComplete();
                }
            }

            if (houseBought)
            {
                if (PlayerStats.hasHome)
                {
                    ChallengeComplete();
                }
            }
		}
	}




}
