﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CalenderDayHandler : MonoBehaviour
{
    public int dayNum;
    public int dayPlus;

    bool rent;
    bool gymMembership;
    bool internet;

    Text dayText;
    Text infoText;

    int currentMonth;

    GameObject crossImage;
    GameObject checkImage;

    public GameObject kickOutPanel;

    bool actionDone;

    void Start()
    {
        kickOutPanel = GameObject.FindGameObjectWithTag("LevelManager").GetComponent<LevelManager>().kickOutPanel;
        dayText = transform.GetChild(0).GetComponent<Text>();
        infoText = transform.GetChild(1).GetComponent<Text>();
        infoText.text = "";
        crossImage = transform.GetChild(2).gameObject;
        checkImage = transform.GetChild(3).gameObject;
        dayNum = (30 - dayNum) - 1;
        NewMonth();
    }

    public void NewMonth()
    {
        dayPlus = (30 * (PlayerStats.months + 1)) - dayNum;
        dayText.text = "Day " + dayPlus.ToString();
        currentMonth = PlayerStats.months;

        crossImage.SetActive(false);
        checkImage.SetActive(false);
    }

    public void Action()
    {
        if (rent)
        {
            PlayerStats.money -= PlayerStats.rent;
            PlayerStats.rentPayDay = PlayerStats.days + 8;
            actionDone = true;
            checkImage.SetActive(true);
        }

        if (internet)
        {
            PlayerStats.money -= PlayerStats.internetFee;
            PlayerStats.internetPayDay = PlayerStats.days + 8;
            actionDone = true;
            checkImage.SetActive(true);
        }

        if (gymMembership)
        {
            PlayerStats.money -= PlayerStats.gymMembership;
            PlayerStats.gymRenewalDay = PlayerStats.days + 8;
            actionDone = true;
            checkImage.SetActive(true);
        }
    }

    public void Update()
    {
        if (currentMonth < PlayerStats.months)
        {
            NewMonth();
        }

        if (PlayerStats.days > dayPlus && !actionDone)
        {
            crossImage.SetActive(true);
        } else { crossImage.SetActive(false); }

        if (PlayerStats.days == dayPlus)
        {
            GetComponentInChildren<Text>().color = Color.red;
            GetComponent<Button>().interactable = true;
        }
        else if (GetComponentInChildren<Text>().color == Color.red)
        {
            GetComponentInChildren<Text>().color = Color.black;
            GetComponent<Button>().interactable = false;
        }
        else
        {
            GetComponent<Button>().interactable = false;
        }

        // House rent handler
        if (dayPlus == PlayerStats.rentPayDay - 1 && PlayerStats.days <= dayPlus)
        {
            rent = true;
            infoText.text = "£" + PlayerStats.rent + " Rent";
        }
        else if (dayPlus == PlayerStats.rentPayDay && PlayerStats.days == dayPlus)
        {
            rent = true;
            infoText.text = "£" + PlayerStats.rent + " Rent";
        }
        else if (dayPlus == PlayerStats.rentPayDay + 1 && PlayerStats.days >= dayPlus)
        {
            rent = true;
            infoText.text = "£" + PlayerStats.rent + " Rent, LAST DAY!";
        }
        else if (dayPlus == PlayerStats.rentPayDay + 2 && PlayerStats.days == dayPlus)
        {
            transform.parent.parent.gameObject.SetActive(false);

            if (PlayerStats.hasHome)
                kickOutPanel.SetActive(true);
        }
        else
        {
            rent = false;
            infoText.text = "";
        }

        // Gym membership handler
        if (dayPlus == PlayerStats.gymRenewalDay)
        {
            gymMembership = true;
            if (!rent)
            {
                infoText.text = "£" + PlayerStats.gymMembership + " gym membership";
            }
            else
            {
                infoText.text = infoText.text + "\n\n £" + PlayerStats.gymMembership + " gym membership";
            }
        }
        else
        {
            gymMembership = false;
        }

        // Internet Renewal Handler
        if (dayPlus == PlayerStats.internetPayDay)
        {
            internet = true;
            if (!gymMembership && !rent)
            {
                infoText.text = "£" + PlayerStats.internetFee + " internet fee";
            } else
            {
                infoText.text = infoText.text + "\n\n £" + PlayerStats.internetFee + " internet fee";
            }
        }
        else
        {
            internet = false;
        }
    }
}
