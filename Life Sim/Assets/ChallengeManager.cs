﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ChallengeManager
{
	public static bool challenge1;
	public static bool challenge2;
	public static bool challenge3;
	public static bool challenge4;
	public static bool challenge5;
	public static bool challenge6;
	public static bool challenge7;
	public static bool challenge8;
	public static bool challenge9;
	public static bool challenge10;
    public static bool challenge11;
    public static bool challenge12;
    public static bool challenge13;
    public static bool challenge14;
    public static bool challenge15;
    public static bool challenge16;
    public static bool challenge17;
    public static bool challenge18;
    public static bool challenge19;
    public static bool challenge20;

	public static void Set(int id)
	{
		if (id == 1)
			challenge1 = true;
		if (id == 2)
			challenge2 = true;
		if (id == 3)
			challenge3 = true;
		if (id == 4)
			challenge4 = true;
		if (id == 5)
			challenge5 = true;
		if (id == 6)
			challenge6 = true;
		if (id == 7)
			challenge7 = true;
		if (id == 8)
			challenge8 = true;
		if (id == 9)
			challenge9 = true;
		if (id == 10)
			challenge10 = true;
        if (id == 11)
            challenge11 = true;
        if (id == 12)
            challenge12 = true;
        if (id == 13)
            challenge13 = true;
        if (id == 14)
            challenge14 = true;
        if (id == 15)
            challenge15 = true;
        if (id == 16)
            challenge16 = true;
        if (id == 17)
            challenge17 = true;
        if (id == 18)
            challenge18 = true;
        if (id == 19)
            challenge19 = true;
        if (id == 20)
            challenge20 = true;
	}
}
