﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ActionsButtonsHandler : MonoBehaviour
{
    public GameObject activeTab;

    public GameObject tab1;
    public GameObject tab2;
    public GameObject tab3;
    public GameObject tab4;
    public GameObject tab5;

    public GameObject infoTab;

    public Button activeButton;
    public Button bOne, bTwo, bThree, bFour, bFive;

    public void SwitchTabs(GameObject tab, Button b)
    {
        if (activeTab != null)
        activeTab.SetActive(false);
        tab.SetActive(true);
        activeTab = tab;
        if (activeButton != null)
        activeButton.interactable = true;
        b.interactable = false;
        activeButton = b;
    }

    public void CloseActive()
    {
        activeTab.SetActive(false);
    }

    public void OpenGameObject()
    {
        if (PlayerInventory.hasStandardTelevision || PlayerInventory.hasFullHdTv)
        {
            activeTab = tab1;
        }
        else if (PlayerInventory.hasBooks)
        {
            activeTab = tab2;
        }
        else if (PlayerInventory.hasXboxOne)
        {
            activeTab = tab3;
        }
        else if (PlayerInventory.hasPS4)
        {
            activeTab = tab4;
        }
        else if (PlayerInventory.hasLaptop)
        {
            activeTab = tab5;
        }
        else
        {
            activeTab = infoTab;
        }
        StartCoroutine("Open");
    }

    IEnumerator Open()
    {
        yield return new WaitForSeconds(0.05f);
        activeTab.SetActive(true);
        StopCoroutine("Open");
    }
     
    public void Open1()
    {
        SwitchTabs(tab1, bOne);
    }

    public void Open2()
    {
        SwitchTabs(tab2, bTwo);
    }

    public void Open3()
    {
        SwitchTabs(tab3, bThree);
    }

    public void Open4()
    {
        SwitchTabs(tab4, bFour);
    }

    public void Open5()
    {
        SwitchTabs(tab5, bFive);
    }
}
