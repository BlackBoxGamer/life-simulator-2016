﻿using UnityEngine;
using System.Collections;

public class EnableDisableOnClick : MonoBehaviour
{
    public GameObject target;

    public void Trigger()
    {
        if (target.activeSelf)
        {
            target.SetActive(false);
        }
        else
        {
            target.SetActive(true);
        }
    }
	
}
