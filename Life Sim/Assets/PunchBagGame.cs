﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PunchBagGame : MonoBehaviour
{
    public GameObject punchToStart;
    public GameObject sessionStats;
    public GameObject shower;

    public int punches;
    public int lastPunches;
    public Text punchesText;

    float timer;
    public Text timerText;

    bool playing;

    public Text strengthText;
    public Text recordText;
    public Text admirersText;
    public Text energyText;

    int strength;
    int admirers;
    int energy;

    public Button punchBag;

    void Start()
    {
        punchesText.text = "Punches " + punches.ToString();

        strengthText.text = "Strength: --";
        recordText.text = "Record: " + PlayerStats.punchbagHighscore;
        admirersText.text = "Admirers: --";
        energyText.text = "Energy Used: --";
    }

    public void Punch()
    {
        if (punches == 0)
        {
            punchToStart.SetActive(false);
            StartGame();
        }

        punches++;
        punchesText.text = "Punches " + punches.ToString();
    }

    void Update()
    {
        if (playing)
        {
            if (timer > 0)
            {
                timer = timer - Time.deltaTime;
                timerText.text = "Time Left: " + timer.ToString("F3");
            }
            else
            {
                timerText.text = "Time's Up!";
                EndGame();
            }
        }
    }

    void StartGame()
    {
        strengthText.text = "Strength: --";
        recordText.text = "Record: " + PlayerStats.punchbagHighscore;
        admirersText.text = "Admirers: --";
        energyText.text = "Energy Used: --";
        punchesText.text = punches.ToString();

        timer = 10;
        playing = true;
    }

    void EndGame()
    {
        punchBag.interactable = false;
        playing = false;
        shower.SetActive(true);
        sessionStats.SetActive(true);
        lastPunches = punches;
        punchesText.text = "Punches: " + lastPunches.ToString();

        admirers = (punches / 10) - Random.Range(0, 5);
        energy = punches / 10;
        PlayerStats.energy -= energy;
        strength = (punches / 10) + Random.Range(0, 3);
        PlayerStats.strength += strength;

        AlertManager.Alert("+" + strength + " Strength", Color.green, AlertType.Middle, 2, 1);
        AlertManager.Alert("-" + energy + " energy", Color.red, AlertType.Middle, 2.5f, 1);

        admirersText.text = "Admirers: " + admirers.ToString();
        energyText.text = "Energy Used: " + energy.ToString();
        strengthText.text = "Strength: +" + strength;

        if (punches > PlayerStats.punchbagHighscore)
        {
            PlayerStats.punchbagHighscore = punches;
            recordText.text = PlayerStats.punchbagHighscore.ToString();
        }

        punches = 0;
    }


}
