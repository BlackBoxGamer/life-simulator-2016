﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FinanceManager : MonoBehaviour 
{
	public Text tuitionText;
	public GameObject tuitionButton;

	public Text rentText;
	public GameObject rentButton;

	public Text internetText;
	public GameObject internetButton;

	public bool hasNotifiedTuition;
	public bool hasNotifiedRent;
	public bool hasNotifiedInternet;
	void Update()
	{
		// Rent Text Settings
		rentText.text = "Rent (£" + PlayerStats.rent + ")"; 

		// Tuition notification/button Settings

		if (PlayerStats.hasTuition) {
			tuitionText.gameObject.SetActive (true);
			tuitionButton.SetActive (true);

			if (PlayerStats.days >= PlayerStats.tuitionPayDay - 2) 
			{
				if (!hasNotifiedTuition && !PlayerStats.autoPayBills) {
						StartCoroutine(NotificationManager.Notification("Tuition Fee due tomorrow!", Color.red));
						hasNotifiedTuition = true;
					}

					if (PlayerStats.money >= PlayerStats.tuitionFee) {
					if (!PlayerStats.autoPayBills) { 
						tuitionButton.GetComponent<Button> ().interactable = true;
						tuitionButton.GetComponentInChildren<Text> ().text = "Pay bill";
					} else {
						PayTuitionBill ();
						StartCoroutine(NotificationManager.Notification("Auto-Payed Tuition!", Color.green));
					}
					} else {
						tuitionButton.GetComponent<Button> ().interactable = false;
						tuitionButton.GetComponentInChildren<Text> ().text = "Can't afford!";
					}
				} else {
					tuitionButton.GetComponent<Button> ().interactable = false;
					tuitionButton.GetComponentInChildren<Text> ().text = "Not due";
				}
		} else {
			tuitionButton.SetActive (false);
			tuitionText.gameObject.SetActive (false);
		}


		// Rent notification/button Settings
		if (PlayerStats.hasHome) 
		{
			rentText.gameObject.SetActive (true);
			rentButton.SetActive (true);

			if (PlayerStats.days >= PlayerStats.rentPayDay - 2) 
			{
				if (!hasNotifiedRent) 
				{
					StartCoroutine(NotificationManager.Notification("Rent due tomorrow!", Color.red));
					hasNotifiedRent = true;
				}

				if (PlayerStats.money >= PlayerStats.rent) {
					if (!PlayerStats.autoPayBills) {
						rentButton.GetComponent<Button> ().interactable = true;
						rentButton.GetComponentInChildren<Text> ().text = "Pay bill";
					} else {
						PayRentBill ();
						StartCoroutine(NotificationManager.Notification("Auto-Payed Rent!", Color.green));
					}
				} else {
					rentButton.GetComponent<Button> ().interactable = false;
					rentButton.GetComponentInChildren<Text> ().text = "Can't afford!";
				}
			} else {
				rentButton.GetComponent<Button> ().interactable = false;
				rentButton.GetComponentInChildren<Text> ().text = "Not due";
			}
		}  else {
			rentButton.SetActive (false);
			rentText.gameObject.SetActive (false);
		}


		if (PlayerInventory.hasInternet) 
		{
			internetText.gameObject.SetActive (true);
			internetButton.SetActive (true);

			if (PlayerStats.days >= PlayerStats.internetPayDay - 2) 
			{
				if (!hasNotifiedInternet) 
				{
					StartCoroutine(NotificationManager.Notification("Internet bill due tomorrow!", Color.red));
					hasNotifiedInternet = true;
				}

				if (PlayerStats.money >= PlayerStats.internetFee) {
					if (!PlayerStats.autoPayBills) {
						internetButton.GetComponent<Button> ().interactable = true;
						internetButton.GetComponentInChildren<Text> ().text = "Pay bill";
					} else {
						PayInternetBill ();
						StartCoroutine(NotificationManager.Notification("Auto-Payed Internet Bill!", Color.green));
					}
				} else {
					internetButton.GetComponent<Button> ().interactable = false;
					internetButton.GetComponentInChildren<Text> ().text = "Can't afford!";
				}
			} else {
				internetButton.GetComponent<Button> ().interactable = false;
				internetButton.GetComponentInChildren<Text> ().text = "Not due";
			}
		}  else {
			internetButton.SetActive (false);
			internetText.gameObject.SetActive (false);
		}

		if (PlayerStats.days > PlayerStats.tuitionPayDay + 2 && PlayerStats.hasTuition) 
		{
			StartCoroutine(NotificationManager.Notification("Tuition Removed!", Color.red));
			PlayerStats.hasTuition = false;
			PlayerStats.money -= PlayerStats.tuitionFee;
            PlayerStats.tuitionFee = 0;
		}

		if (PlayerStats.days > PlayerStats.rentPayDay + 2 && PlayerInventory.hasHome) 
		{
			StartCoroutine(NotificationManager.Notification("Kicked out of home!", Color.red));
			PlayerInventory.hasHome = false;
			PlayerStats.money -= PlayerStats.rent;
            PlayerStats.rent = 0;
		}

		if (PlayerStats.days > PlayerStats.internetPayDay + 2 && PlayerInventory.hasInternet) 
		{
			StartCoroutine(NotificationManager.Notification("Internet disabled!", Color.red));
			PlayerInventory.hasInternet = false;
			PlayerStats.money -= PlayerStats.internetFee;
            PlayerStats.rent = 0;
		}


	}

	public void PayTuitionBill()
	{
		PlayerStats.money -= PlayerStats.tuitionFee;
        PlayerStats.moneySpent += PlayerStats.tuitionFee;
		PlayerStats.hasTuition = true;
		PlayerStats.billsPayed++;
		PlayerStats.tuitionPayDay += 7 + (PlayerStats.days - PlayerStats.tuitionPayDay);
		hasNotifiedTuition = false;
//		StartCoroutine(NotificationManager.Notification("-£" + PlayerStats.tuitionFee, Color.red));
		AlertManager.Alert ("-£" + PlayerStats.tuitionFee, Color.red, AlertType.Top, 1.5f, 1);
	}

	public void PayRentBill()
	{
		PlayerStats.money -= PlayerStats.rent;
        PlayerStats.moneySpent += PlayerStats.rent;
		PlayerStats.hasHome = true;
		PlayerStats.billsPayed++;
		PlayerStats.rentPayDay += 7 + (PlayerStats.days - PlayerStats.rentPayDay);
		hasNotifiedRent = false;
//		StartCoroutine(NotificationManager.Notification("-£" + PlayerStats.rent, Color.red));
		AlertManager.Alert ("-£" + PlayerStats.rent, Color.red, AlertType.Top, 1.5f, 1);
	}

	public void PayInternetBill()
	{
		PlayerStats.money -= PlayerStats.internetFee;
        PlayerStats.moneySpent += PlayerStats.internetFee;
		PlayerInventory.hasInternet = true;
		PlayerStats.billsPayed++;
		PlayerStats.internetPayDay += 7 + (PlayerStats.days - PlayerStats.internetPayDay);
		hasNotifiedInternet = false;
		//		StartCoroutine(NotificationManager.Notification("-£" + PlayerStats.rent, Color.red));
		AlertManager.Alert ("-£" + PlayerStats.internetFee, Color.red, AlertType.Top, 1.5f, 1);
	}

}
