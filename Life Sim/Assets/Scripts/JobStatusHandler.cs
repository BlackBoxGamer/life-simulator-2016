﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class JobStatusHandler : MonoBehaviour 
{
	public Color greatColour;
	public Color happyColour;
	public Color normalColour;
	public Color badColour;
	public Color awfulColour;

	public bool changedWage = false;

	void Update()
	{
		if (PlayerStats.bossHappiness == 10) {
			GetComponent<Text> ().text = "Your boss is over the moon with you!";
			GetComponent<Text> ().color = greatColour;
			if (!changedWage) 
			{
				PlayerStats.wage = PlayerStats.wage * 2;
				changedWage = true;
			}
		}
		if (PlayerStats.bossHappiness < 10 && PlayerStats.bossHappiness >= 7) {
			GetComponent<Text> ().text = "Your boss is happy with your work";
			GetComponent<Text> ().color = happyColour;
			if (changedWage) 
			{
				PlayerStats.wage = PlayerStats.wage / 2;
				changedWage = false;
			}
		}
		if (PlayerStats.bossHappiness < 7 && PlayerStats.bossHappiness >= 4) {
			GetComponent<Text> ().text = "You're just a typical worker...";
			GetComponent<Text> ().color = normalColour;
			changedWage = false;
		}
		if (PlayerStats.bossHappiness < 4 && PlayerStats.bossHappiness >= 2) {
			GetComponent<Text> ().text = "You need to step up a little";
			GetComponent<Text> ().color = badColour;
			if (changedWage) 
			{
				PlayerStats.wage = PlayerStats.wage * 2;
				changedWage = false;
			}
		}
		if (PlayerStats.bossHappiness < 2) {
			GetComponent<Text> ().text = "You are on the verge of being fired!";
			GetComponent<Text> ().color = awfulColour;
			if (!changedWage)
			{
				PlayerStats.wage = PlayerStats.wage / 2;
				changedWage = true;
			}
		}

		if (PlayerStats.bossHappiness < 1) 
		{
			FirePlayer ();
		}
	}
		
	void FirePlayer()
	{
		PlayerStats.wage = 0;
		PlayerStats.hasJob = false;
		PlayerStats.jobName = "";
		PlayerStats.lifeScore -= 50;
	}
}
