﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DrinkEnergy : MonoBehaviour
{
    void Update()
    {
        GetComponentInChildren<Text>().text = PlayerInventory.numEnergyDrinks.ToString();

        if (PlayerInventory.numEnergyDrinks <= 0)
        {
            GetComponent<Button>().interactable = false;
        }
        else
        {
            GetComponent<Button>().interactable = true;
        }
    }

	public void Drink()
	{
            PlayerStats.energy += 30;
            PlayerInventory.numEnergyDrinks--;
            AlertManager.Alert("+25 Energy", Color.white, AlertType.Middle, 1f, 1f);
	}
}
