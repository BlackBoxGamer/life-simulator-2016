﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class JobIndicatorHandler : MonoBehaviour
{
	public Sprite check;
	public Sprite cross;

	public Color checkGreen;
	public Color crossRed;

	JobInfo job;

	private bool canGet;

	public bool needsKnowledge;
	public bool needsMaths;
	public bool needsICT;
	public bool needsPhysics;
	public bool needsChemistry;
	public bool needsBiology;
	public bool needsEnglish;
	public bool needsGeo;
	public bool needsHistory;
	public bool needsBusiness;
	public bool needsProgramming;
	public bool needsComputing;
	public bool needsEconomics;
	public bool needsCalculus;
	public bool needsArt;
	public bool needsAnimation;
	public bool needsLaw;
	public bool needsSocial;
	public bool needsCreativity;
	public bool needsStrength;
	public bool needsExperience;

	public void Awake()
	{
		job = transform.parent.GetComponent<JobInfo> ();
	}


	public void FixedUpdate()
	{
		if (job.knowledgeNeeded != 0 && job.knowledgeNeeded > PlayerStats.knowledge) {
			needsKnowledge = true;
		} else {
			needsKnowledge = false;
		}

		if (job.mathsNeeded != 0 && job.mathsNeeded > PlayerStats.maths) {
			needsMaths = true;
		} else {
			needsMaths = false;
		} 

		if (job.ictNeeded != 0 && job.ictNeeded > PlayerStats.ict) {
			needsICT = true;
		} else {
			needsICT = false;
		}

		if (job.physicsNeeded != 0 && job.physicsNeeded > PlayerStats.physics) {
			needsPhysics = true;
		} else {
			needsPhysics = false;
		}		
		if (job.chemistryNeeded != 0 && job.chemistryNeeded > PlayerStats.chemistry) {
			needsChemistry = true;
		} else {
			needsChemistry = false;
		}
		if (job.biologyNeeded != 0 && job.biologyNeeded > PlayerStats.biology) {
			needsBiology = true;
		} else {
			needsBiology = false;
		}
		if (job.geographyNeeded != 0 && job.geographyNeeded > PlayerStats.geography) {
			needsGeo = true;
		} else {
			needsGeo = false;
		}
		if (job.historyNeeded != 0 && job.historyNeeded > PlayerStats.history) {
			needsHistory = true;
		} else {
			needsHistory = false;
		}
		if (job.businessNeeded != 0 && job.businessNeeded > PlayerStats.business) {
			needsBusiness = true;
		} else {
			needsBusiness = false;
		}
		if (job.programmingNeeded != 0 && job.programmingNeeded > PlayerStats.programming) {
			needsProgramming = true;
		} else {
			needsProgramming = false;
		}
		if (job.computingNeeded != 0 && job.computingNeeded > PlayerStats.computing) {
			needsComputing = true;
		} else {
			needsComputing = false;
		}
		if (job.economicsNeeded != 0 && job.economicsNeeded > PlayerStats.economics) {
			needsEconomics = true;
		} else {
			needsEconomics = false;
		}
		if (job.calculusNeeded != 0 && job.calculusNeeded > PlayerStats.calculus) {
			needsCalculus = true;
		} else {
			needsCalculus = false;
		}
		if (job.artNeeded != 0 && job.artNeeded > PlayerStats.art) {
			needsArt = true;
		} else {
			needsArt = false;
		}
		if (job.animationNeeded != 0 && job.animationNeeded > PlayerStats.animation) {
			needsAnimation = true;
		} else {
			needsAnimation = false;
		}
		if (job.lawNeeded != 0 && job.lawNeeded > PlayerStats.law) {
			needsLaw = true;
		} else {
			needsLaw = false;
		}
		if (job.englishNeeded != 0 && job.englishNeeded > PlayerStats.english) {
			needsEnglish = true;
		} else {
			needsEnglish = false;
		}
		if (job.experienceNeeded != 0 && job.experienceNeeded > PlayerStats.experience) {
			needsExperience = true;
		} else {
			needsExperience = false;
		}

		if (job.socialNeeded != 0 && job.socialNeeded > PlayerStats.social) {
			needsSocial = true;
		} else {
			needsSocial = false;
		} 
		if (job.creativityNeeded != 0 && job.creativityNeeded > PlayerStats.creativity) {
			needsCreativity = true;
		} else {
			needsCreativity = false;
		}
		if (job.strengthNeeded != 0 && job.strengthNeeded > PlayerStats.strength) {
			needsStrength = true;
		} else {
			needsStrength = false;
		}

		if (!needsEnglish &&
		    !needsExperience &&
		    !needsICT &&
		    !needsKnowledge &&
		    !needsMaths &&
		    !needsPhysics &&
		    !needsSocial &&
		    !needsStrength &&
			!needsChemistry &&
			!needsBiology && 
			!needsGeo && 
			!needsHistory &&
			!needsBusiness &&
			!needsProgramming &&
			!needsComputing && 
			!needsCalculus &&
			!needsArt &&
			!needsAnimation &&
			!needsLaw &&
			!needsEconomics
		) 
		{
			canGet = true;
		}

		if (canGet) {
			GetComponent<Image> ().sprite = check;
			GetComponent<Image> ().color = checkGreen;
		} else 
		{
			GetComponent<Image> ().sprite = cross;
			GetComponent<Image> ().color = crossRed;
		}
	}
}
