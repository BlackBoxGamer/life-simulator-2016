﻿using UnityEngine;
using UnityEngine.UI;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;
using UnityEngine.Advertisements;
using System.Collections;

public class ReviveHandler : MonoBehaviour, IRewardedVideoAdListener
{
	public bool isPay;

	void Update()
	{
		if (PlayerStats.money < PlayerStats.lifeScore * 6 && isPay) {
			GetComponent<Button> ().interactable = false;
		} else {
			GetComponent<Button> ().interactable = true;
		}
	}

	public void WatchAd()
	{
        Appodeal.show(Appodeal.REWARDED_VIDEO);
        Appodeal.setRewardedVideoCallbacks(this);
    }

    #region Rewarded Video callback handlers
    public void onRewardedVideoLoaded() { print("Video loaded"); }
    public void onRewardedVideoFailedToLoad() { print("Video failed"); }
    public void onRewardedVideoShown() { print("Video shown"); }
    public void onRewardedVideoClosed() { print("Video closed"); }
    public void onRewardedVideoFinished(int amount, string name) { Revive(); }
    #endregion

    public void Revive()
    {
        PlayerStats.health = 100;
        PlayerStats.hunger = 100;
        PlayerStats.mood = 100;
        PlayerStats.energy = 100;

        gameObject.SetActive(false);
    }

    public void PayFee()
	{
		PlayerStats.money -= PlayerStats.lifeScore *= 6;

		PlayerStats.health = 100;
		PlayerStats.hunger = 100;
		PlayerStats.mood = 100;
		PlayerStats.energy = 100;

		PlayerStats.lifeScore -= PlayerStats.lifeScore / 10;

		gameObject.SetActive (false);
	}
}


