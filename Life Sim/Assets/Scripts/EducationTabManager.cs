﻿using UnityEngine;
using System.Collections;

public class EducationTabManager : MonoBehaviour 
{
	public GameObject noTuition;
	public GameObject hasTuition;

	void OnEnable()
	{
		if (PlayerStats.hasTuition)
		{
			hasTuition.SetActive (true);
			noTuition.SetActive (false);
		}
		if (!PlayerStats.hasTuition) {
			noTuition.SetActive (true);
			hasTuition.SetActive (false);
		}
	}
}
