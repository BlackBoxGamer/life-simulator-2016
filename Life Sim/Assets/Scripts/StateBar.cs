﻿using UnityEngine;
using System.Collections;

public class StateBar : MonoBehaviour 
{
	public void Update()
	{
		if (PlayerStats.fatigued) 
		{
			transform.GetChild (0).gameObject.SetActive (true);
		}
		else
		{
			transform.GetChild (0).gameObject.SetActive (false);
		}
	}


}
