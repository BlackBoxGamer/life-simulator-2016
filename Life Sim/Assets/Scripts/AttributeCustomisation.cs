﻿using UnityEngine;
using System.Collections;

public class AttributeCustomisation : MonoBehaviour
{
	public bool isKnowledge;
	public bool isStrength;
	public bool isCreativity;
	public bool isSocial;

	public void Minus()
	{
		if (isKnowledge && PlayerStats.knowledge > 0)
		{
			PlayerStats.attributePoints++;
			PlayerStats.knowledge--;
			PlayerStats.lifeScore--;
		}
		if (isStrength && PlayerStats.strength > 0)
		{
			PlayerStats.attributePoints++;
			PlayerStats.strength--;
			PlayerStats.lifeScore--;
		}
		if (isSocial && PlayerStats.social > 0)
		{
			PlayerStats.attributePoints++;
			PlayerStats.social--;
			PlayerStats.lifeScore--;
		}
		if (isCreativity && PlayerStats.creativity > 0) 
		{
			PlayerStats.attributePoints++;
			PlayerStats.creativity--;
			PlayerStats.lifeScore--;
		}
	}

	public void Plus()
	{
		if (PlayerStats.attributePoints > 0)
		{
			PlayerStats.attributePoints--;

			if (isKnowledge) 
			{
				PlayerStats.knowledge++;
				PlayerStats.lifeScore++;
			}
			if (isStrength)
			{
				PlayerStats.strength++;
				PlayerStats.lifeScore++;
			}
			if (isSocial)
			{
				PlayerStats.social++;
				PlayerStats.lifeScore++;
			}
			if (isCreativity) 
			{
				PlayerStats.creativity++;
				PlayerStats.lifeScore++;
			}
		}
	}
}
