﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class JobPanelHandler : MonoBehaviour 
{
	public GameObject noJobStuff;

	public GameObject workStuff;
	public GameObject backButton;
	public Text text;

	void Update()
	{
		if (PlayerStats.hasJob) 
		{
			backButton.SetActive (true);
			text.text = "Other Jobs";
		}

		else if (!PlayerStats.hasJob)
		{
			noJobStuff.SetActive (true);
			workStuff.SetActive (false);
			backButton.SetActive (false);
			text.text = "You are unemployed!";
		}
	}
}
