﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SunMoonCycle : MonoBehaviour 
{
    public Sprite morning;
    public Sprite day;
    public Sprite evening;
    public Sprite night;


	void Update () 
	{
        if (PlayerStats.hours == 6)
            GetComponent<Image>().sprite = morning;
        if (PlayerStats.hours == 12)
            GetComponent<Image>().sprite = day;
        if (PlayerStats.hours == 18)
            GetComponent<Image>().sprite = evening;
        if (PlayerStats.hours == 23)
            GetComponent<Image>().sprite = night;
	}
}
