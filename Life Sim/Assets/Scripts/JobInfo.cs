﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using UnityEngine.Serialization;

public class JobInfo : MonoBehaviour
{
    [Header("Info")]
    [Space(10)]
    public int id;

	public Sprite icon;

	public string jobName;
	public string desc;

	public int wage;

	public int energyNeeded;

	public int startTime;
	public int endTime;

    public bool canDo;

    [Header("Application")]
    public double wait;
    public string[] questions;
    public string[] answers;
    public int[] correct;
    public int nextInterviewDay;
    public int applicationDelay;

    [Header("Requirements")]
    [Space(10)]
	public int strengthNeeded;
	public int knowledgeNeeded;
	public int mathsNeeded;
	public int ictNeeded;
	public int englishNeeded;
	public int physicsNeeded;
	public int chemistryNeeded;
	public int biologyNeeded;
	public int geographyNeeded;
	public int historyNeeded;
	public int businessNeeded;
	public int programmingNeeded;
	public int computingNeeded;
	public int economicsNeeded;
	public int calculusNeeded;
	public int artNeeded;
	public int animationNeeded;
	public int lawNeeded;
	public int socialNeeded;
	public int creativityNeeded;
	public int experienceNeeded;

	[Header("Qualifications")]
	public bool DrivingLicense;
	public bool BrickLaying;
	public bool GamesDesign;
	public bool Banking;
	public bool Finance;
	public bool Catering;
	public bool TrainLicense;
	public bool Archeology;
	public bool FirstAid;

	public GameObject reqPanel;
	public Text reqtext;
	public Text hasText;

    void Start()
    {
        UpdateList();
        GetComponentInChildren<Text>().text = jobName;
    }

    public void UpdateList()
    {
        if (PlayerStats.strength >= strengthNeeded &&
            PlayerStats.knowledge >= knowledgeNeeded &&
            PlayerStats.maths >= mathsNeeded &&
            PlayerStats.ict >= ictNeeded &&
            PlayerStats.english >= englishNeeded &&
            PlayerStats.physics >= physicsNeeded &&
            PlayerStats.chemistry >= chemistryNeeded &&
            PlayerStats.biology >= biologyNeeded &&
            PlayerStats.geography >= geographyNeeded &&
            PlayerStats.history >= historyNeeded &&
            PlayerStats.business >= businessNeeded &&
            PlayerStats.programming >= programmingNeeded &&
            PlayerStats.computing >= computingNeeded &&
            PlayerStats.economics >= economicsNeeded &&
            PlayerStats.calculus >= calculusNeeded &&
            PlayerStats.art >= artNeeded &&
            PlayerStats.animation >= animationNeeded &&
            PlayerStats.law >= lawNeeded &&
            PlayerStats.social >= socialNeeded &&
            PlayerStats.creativity >= creativityNeeded &&
            PlayerStats.experience >= experienceNeeded &&
			PlayerStats.drivingLicense == DrivingLicense &&
			PlayerStats.brickLaying == BrickLaying &&
			PlayerStats.gameDesign == GamesDesign &&
			PlayerStats.banking == Banking && 
			PlayerStats.finance == Finance &&
			PlayerStats.catering == Catering && 
			PlayerStats.trainLicense == TrainLicense && 
			PlayerStats.archeology == Archeology &&
			PlayerStats.firstAid == FirstAid)
        {
            canDo = true;
        }
        else
        {
            canDo = false;
        }
    }

	public void ShowRequirements(JobUI ui)
	{
		reqPanel = ui.reqPanel;
		reqtext = ui.reqText;
		hasText = ui.hasText;

		reqtext.text = FindRequirements ();
		hasText.text = FindNeeded ();

		reqPanel.SetActive (true);
	}

	string FindRequirements()
	{
		string s = "<size=25><color=green>Required</color></size>";

		if (strengthNeeded > 0) 
		{
			s = s + "\n Strength: " + strengthNeeded;
		}

		if (knowledgeNeeded > 0) 
		{
			s = s + "\n Knowledge: " + knowledgeNeeded;
		}

		if (mathsNeeded > 0) 
		{
			s = s + "\n Maths: " + mathsNeeded;
		}

		if (ictNeeded > 0) 
		{
			s = s  + "\n ICT: " + ictNeeded;
		}

		if (englishNeeded > 0) 
		{
			s = s + "\n English: " + englishNeeded;
		}

		if (physicsNeeded > 0) 
		{
			s = s + "\n Physics: " + physicsNeeded;
		}

		if (chemistryNeeded > 0) 
		{
			s = s + "\n Chemistry: " + chemistryNeeded;
		}

		if (biologyNeeded > 0) 
		{
			s = s + "\n Biology: " + biologyNeeded;
		}

		if (geographyNeeded > 0) 
		{
			s = s + "\n Geography: " + geographyNeeded;
		}

		if (historyNeeded > 0) 
		{
			s = s + "\n History: " + historyNeeded;
		}

		if (businessNeeded > 0) 
		{
			s = s + "\n Business: " + businessNeeded;
		}

		if (programmingNeeded > 0) 
		{
			s = s + "\n Programming: " + programmingNeeded;
		}

		if (computingNeeded > 0) 
		{
			s = s + "\n Computing: " + computingNeeded;
		}

		if (economicsNeeded > 0) 
		{
			s = s + "\n Economics: " + economicsNeeded;
		}

		if (calculusNeeded > 0) 
		{
			s = s + "\n Calculus: " + calculusNeeded;
		}

		if (artNeeded > 0) 
		{
			s = s + "\n Art: " + artNeeded;
		}

		if (animationNeeded > 0) 
		{
			s = s + "\n Animation: " + animationNeeded;
		}

		if (lawNeeded > 0) 
		{
			s = s + "\n Law: " + lawNeeded;
		}

		if (socialNeeded > 0) 
		{
			s = s + "\n Social: " + socialNeeded;
		}

		if (creativityNeeded > 0) 
		{
			s = s + "\n Creativity: " + creativityNeeded;
		}

		if (experienceNeeded > 0) 
		{
			s = s + "\n Experience: " + experienceNeeded;
		}

		if (DrivingLicense) 
		{
			s = s + "\n Driving License";
		}

		if (BrickLaying) 
		{
			s = s + "\n Bricklaying";
		}
		if (GamesDesign) 
		{
			s = s + "\n Games Design";
		}
		if (Banking) 
		{
			s = s + "\n Banking";
		}
		if (Finance) 
		{
			s = s + "\n Finance";
		}
		if (Catering) 
		{
			s = s + "\n Catering";
		}
		if (TrainLicense) 
		{
			s = s + "\n Train License";
		}
		if (Archeology) 
		{
			s = s + "\n Archeology";
		}
		if (FirstAid) 
		{
			s = s + "\n First Aid";
		}

		return s;
	}

	string FindNeeded()
	{
		string s = "<size=22><color=red>Needed</color></size>";

		if (strengthNeeded > 0 && PlayerStats.strength < strengthNeeded) 
		{
			s = s + "\n +" + (strengthNeeded - PlayerStats.strength).ToString ();
		}

		if (knowledgeNeeded > 0 && PlayerStats.knowledge < knowledgeNeeded) 
		{
			s = s + "\n +" + (knowledgeNeeded - PlayerStats.knowledge).ToString();
		}

		if (mathsNeeded > 0 && PlayerStats.maths < mathsNeeded) 
		{
			s = s + "\n +" + (mathsNeeded - PlayerStats.maths).ToString();
		}

		if (ictNeeded > 0 && PlayerStats.ict < ictNeeded) 
		{
			s = s + "\n +" + (ictNeeded - PlayerStats.ict).ToString();
		}

		if (englishNeeded > 0 && PlayerStats.english < englishNeeded) 
		{
			s = s + "\n +" + (englishNeeded - PlayerStats.english).ToString();
		}

		if (physicsNeeded > 0 && PlayerStats.physics < physicsNeeded) 
		{
			s = s + "\n +" + (physicsNeeded - PlayerStats.physics).ToString();
		}

		if (chemistryNeeded > 0 && PlayerStats.chemistry < chemistryNeeded) 
		{
			s = s + "\n +" + (chemistryNeeded - PlayerStats.chemistry).ToString();
		}

		if (biologyNeeded > 0 && PlayerStats.biology < biologyNeeded) 
		{
			s = s + "\n +" + (biologyNeeded - PlayerStats.biology).ToString();
		}

		if (geographyNeeded > 0 && PlayerStats.geography < geographyNeeded) 
		{
			s = s + "\n +" + (geographyNeeded - PlayerStats.geography).ToString();
		}

		if (historyNeeded > 0 && PlayerStats.history < historyNeeded) 
		{
			s = s + "\n +" + (historyNeeded - PlayerStats.history).ToString();
		}

		if (businessNeeded > 0 && PlayerStats.business < businessNeeded) 
		{
			s = s + "\n +" + (businessNeeded - PlayerStats.business).ToString();
		}

		if (programmingNeeded > 0 && PlayerStats.programming < programmingNeeded) 
		{
			s = s + "\n +" + (programmingNeeded - PlayerStats.programming).ToString();
		}

		if (computingNeeded > 0 && PlayerStats.computing < computingNeeded) 
		{
			s = s + "\n +" + (computingNeeded - PlayerStats.computing).ToString();
		}

		if (economicsNeeded > 0 && PlayerStats.economics < economicsNeeded) 
		{
			s = s + "\n +" + (economicsNeeded - PlayerStats.economics).ToString();
		}

		if (calculusNeeded > 0 && PlayerStats.calculus < calculusNeeded) 
		{
			s = s + "\n +" + (calculusNeeded - PlayerStats.calculus).ToString();
		}

		if (artNeeded > 0 && PlayerStats.art < artNeeded) 
		{
			s = s + "\n +" + (artNeeded - PlayerStats.art).ToString();
		}

		if (animationNeeded > 0 && PlayerStats.animation < animationNeeded) 
		{
			s = s + "\n +" + (animationNeeded - PlayerStats.animation).ToString();
		}

		if (lawNeeded > 0 && PlayerStats.law < lawNeeded) 
		{
			s = s + "\n +" + (lawNeeded - PlayerStats.law).ToString();
		}

		if (socialNeeded > 0 && PlayerStats.social < socialNeeded) 
		{
			s = s + "\n +" + (socialNeeded - PlayerStats.social).ToString();
		}

		if (creativityNeeded > 0 && PlayerStats.creativity < creativityNeeded) 
		{
			s = s + "\n +" + (creativityNeeded - PlayerStats.creativity).ToString();
		}

		if (experienceNeeded > 0 && PlayerStats.experience < experienceNeeded) 
		{
			s = s + "\n +" + (experienceNeeded - PlayerStats.experience).ToString ();
		}

		if (DrivingLicense && PlayerStats.drivingLicense == false) 
		{
			s = s + "\n Needed";
		}

		if (BrickLaying && PlayerStats.brickLaying == false) 
		{
			s = s + "\n Needed";
		}
		if (GamesDesign && PlayerStats.gameDesign == false) 
		{
			s = s + "\n Needed";
		}
		if (Banking && PlayerStats.banking == false) 
		{
			s = s + "\n Needed";
		}
		if (Finance && PlayerStats.finance == false) 
		{
			s = s + "\n Needed";
		}
		if (Catering && PlayerStats.catering == false) 
		{
			s = s + "\n Needed";
		}
		if (TrainLicense && PlayerStats.trainLicense == false) 
		{
			s = s + "\n Needed";
		}
		if (Archeology && PlayerStats.archeology == false) 
		{
			s = s + "\n Needed";
		}
		if (FirstAid && PlayerStats.firstAid == false) 
		{
			s = s + "\n Needed";
		}

		return s;
	}
}
