﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

[System.Serializable]
public class PlayerStats
{
    public static string version = "0.8a";

    public static bool resetting = false;

	public static string FBName = null;

	public static int timesPlayed;

	public static int hours = 6;
    public static int minutes = 0;
	public static int days = 1;
	public static int weeks = 0;
    public static int months = 0;
	public static int lastWorkDay;

	public static int classesTaken;

	public static int health = 100;
	public static int hunger = 100;
	public static int energy = 100;
	public static int mood = 100;
	public static int experience = 0;

    public static float conversionRate = 1f;
    public static float maxMoney;
	public static float money = 50;
    public static float bankMoney;
    public static float interestRate;

    public static string currency = "£";

	public static float wage;

	public static float weeklyCosts;
	public static float rent;
	public static float tuitionFee;
	public static float internetFee;
    public static float gymMembership;
    public static int gymRenewalDay;

	public static string name = "Brandon";
	public static string jobName = "Unemployed";
    public static string jobDesc = "You don't have a job, that's not good. If you have a job you can earn more money every day. I suggest you get one.";
    public static int jobId = 16;
    public static string lifeAspiration;
    public static string location = "Europe";

    // Interview Stuff
    public static int jobApplyDay;

    public static string interviewJobName;
    public static string interviewJobDesc;
    public static float interviewJobMinimumWage;
    public static int interviewJobStartTime;
    public static int interviewJobEndTime;
    public static int interviewJobDelay;
    public static int interviewJobID;
    public static bool waitingForInterview;

    public static DateTime interviewTime;
    public static string[] interviewQuestions;
    public static string[] interviewAnswers;
    public static int[] interviewCorrect;

	public static bool hasJob = false;
	public static bool hasTuition = false;
	public static bool hasHome = false;

	// Pay Days
	public static int tuitionPayDay;
	public static int rentPayDay;
	public static int internetPayDay;

	// States
	public static bool fatigued;
	public static bool ill;
	public static bool happy;
	public static bool sad;
	public static bool bored;

	// Attributes
	public static int attributePoints = 10;

	public static int strength;
	public static int physique;
	public static int coolness;
	public static int social;
	public static int creativity;

	public static int knowledge;

	// Courses
	public static bool onCourse;
	public static int currentCourse;
	public static int currentCourseRemaining;

	// Classes
	public static int maths;
	public static int english;
	public static int physics;
	public static int chemistry;
	public static int biology;
	public static int ict;
	public static int geography;
	public static int history;
	public static int business;

	public static int programming;
	public static int computing;
	public static int economics;
	public static int calculus;
	public static int art;
	public static int animation;
	public static int law;

	// Qualifications
	public static bool drivingLicense;
	public static bool brickLaying;
	public static bool gameDesign;
	public static bool banking;
	public static bool finance;
	public static bool catering;
	public static bool trainLicense;
	public static bool archeology;
	public static bool firstAid;

	// Random
	public static int attractiveness;

    // World stuff
    public static bool playMusic = true;
    public static bool playSounds = true;
	public static int lifeScore = 50;
	public static bool autoPayBills = true;
	public static int billsPayed;
	public static bool infoButtons = true;
    public static bool autoLoadGame = true;
    public static bool disableOffensiveJokes = false;
    public static bool enableMinigames = true;
    public static int gambles;

	// Work stuff
	public static int bossHappiness = 5;
	public static int workStartTime;
	public static int workEndTime;
	public static int workEnergyRequired;

    // House stuff
    public static int entertainmentItems;
    public static int houseRoom;
    public static float plantHealth = 10f;

    //Misc
    public static DateTime rewardTime;
    public static int daysReturned = 1;
    public static int timesGuitarPlayed;
    public static bool tutorials = true;

    public static float moneySpent;
    public static int daysWorked;
    public static int numJobs;
    public static int classesAttended;
    public static int promotions;
    public static int punchbagHighscore;

    // Bank interest stuff

    public static DateTime lastPlayed;
    public static float interestGained;

    // Lottery
    public static int num1;
    public static int num2;
    public static int num3;
    public static int num4;
    public static int num5;

    // Twitter

    public static bool tweetedUs;
    public static bool followedBrandon;
    public static bool followedAlfie;
    public static bool followedKlaus;
    public static bool followedAaron;
    public static bool followedFVG;

    // Promo Codes
    public static bool code1;
    public static bool code2;
    public static bool code3;
    public static bool code4;
    public static bool code5;
}
