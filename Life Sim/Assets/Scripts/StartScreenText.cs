﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StartScreenText : MonoBehaviour 
{
	public GameObject[] textObjects;

	private int textActive = -1;

	public void Next()
	{
		if (textActive + 1 < textObjects.Length) {
			textActive++;
			textObjects [textActive].SetActive (true);
		} 
		else 
		{
			gameObject.SetActive (false);
		}
	}


}
