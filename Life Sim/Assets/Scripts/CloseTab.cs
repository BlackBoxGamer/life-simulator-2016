﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UberAudio;

public class CloseTab : MonoBehaviour
{
    public LevelManager LevelManager;

	public GameObject open;

    public bool mainPanel;

	public bool hasExtraParent;

    public Image transitionImage;

    void Awake()
    {
        LevelManager = GameObject.FindGameObjectWithTag("LevelManager").GetComponent<LevelManager>();
    }

	public void Close()
	{
		if (hasExtraParent) {
			transform.parent.parent.gameObject.SetActive (false);
		} else {
			transform.parent.gameObject.SetActive (false);
		}
	}

    public void Open()
    {
        if (mainPanel)
        {
            StartCoroutine("MoveLocation");
        }
        else
        {
            open.SetActive(true);
        }
    }

    IEnumerator MoveLocation()
    {
        float curAlpha = transitionImage.color.a;

        transitionImage.CrossFadeAlpha(1, 0.75f, false);
        yield return new WaitForSeconds(0.75f);
        transitionImage.CrossFadeAlpha(0, 0.75f, false);

        open.SetActive(true);

        if (mainPanel)
        {
            if (LevelManager.activePanel != open)
            {
                LevelManager.activePanel.SetActive(false);
                LevelManager.activePanel = open;

                if (LevelManager.activePanel == LevelManager.homePanel)
                {
                    if (PlayerStats.hasHome && !PlayerInventory.hasLevel1Home)
                    {
                        if (PlayerInventory.hasLevel2Home)
                            LevelManager.homePanel.BroadcastMessage("UpdateMyItems");
                    }

                    if (LevelManager.lamp.on)
                    {
                        LevelManager.lamp.LightOn();
                        LevelManager.lamp.LightOn();
                    }
                }
            }
            else
            {
                LevelManager.activePanel = open;
            }
        }
    }
}
