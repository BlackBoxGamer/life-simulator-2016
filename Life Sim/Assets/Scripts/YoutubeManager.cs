﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class YoutubeManager
{
    public static string channelName;
    public static int subscribers;
    public static int totalViews;
    public static int totalRevenue;
    public static List<string> videos;

    public static string latestVideo;
    public static int latestVideoViews;
    public static int latestVideoRevenue;

    public static bool hasChannel;

    public static int maxQuality = 1;
    public static bool releasedToday;
}
