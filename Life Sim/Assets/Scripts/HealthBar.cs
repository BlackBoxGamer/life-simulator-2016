﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HealthBar : MonoBehaviour 
{
	public bool isEnergy;
	public bool isHealth;
	public bool isMood;

	void Update()
	{
		if (isEnergy)
			GetComponent<Image> ().fillAmount = (float)PlayerStats.energy / 100;
		if (isHealth)
			GetComponent<Image> ().fillAmount = (float)PlayerStats.health / 100;
		if (isMood)
			GetComponent<Image> ().fillAmount = (float)PlayerStats.mood / 100;
	}
}
