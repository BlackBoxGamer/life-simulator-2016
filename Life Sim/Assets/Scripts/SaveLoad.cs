﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

[System.Serializable]
public class SaveLoad : MonoBehaviour
{
    public static SaveLoad saveLoad;

    void Awake()
    {
        if (saveLoad == null)
        {
            DontDestroyOnLoad(gameObject);
            saveLoad = this;
        }
        else if (saveLoad != this)
        {
            Destroy(gameObject);
        }
    }

    public void Save()
    {
        BinaryFormatter bf = new BinaryFormatter ();

		FileStream file = File.Create (Application.persistentDataPath + "/lifeSim.Life");

		Stats player = new Stats ();

        player.timesPlayed = PlayerStats.timesPlayed;

        player.FBName = PlayerStats.FBName;
		player.days = PlayerStats.days;
		player.hours = PlayerStats.hours;
        player.minutes = PlayerStats.minutes;
		player.weeks = PlayerStats.weeks;
        player.months = PlayerStats.months;
		player.lastWorkDay = PlayerStats.lastWorkDay;
        player.jobDesc = PlayerStats.jobDesc;

		player.classesTaken = PlayerStats.classesTaken;

		player.health = PlayerStats.health;
		player.hunger = PlayerStats.hunger;
		player.energy = PlayerStats.energy;
		player.mood = PlayerStats.mood;
		player.experience = PlayerStats.experience;

        player.jobApplyDay = PlayerStats.jobApplyDay;

        player.interviewJobEndTime = PlayerStats.interviewJobEndTime;
        player.interviewJobStartTime = PlayerStats.interviewJobStartTime;
        player.interviewJobName = PlayerStats.interviewJobName;
        player.interviewJobDesc = PlayerStats.interviewJobDesc;
        player.interviewJobMinimumWage = PlayerStats.interviewJobMinimumWage;
        player.interviewJobDelay = PlayerStats.interviewJobDelay;
        player.interviewJobID = PlayerStats.interviewJobID;
        player.waitingForInterview = PlayerStats.waitingForInterview;

        player.interviewTime = PlayerStats.interviewTime;
        player.interviewQuestions = PlayerStats.interviewQuestions;
        player.interviewAnswers = PlayerStats.interviewAnswers;
        player.interviewCorrect = PlayerStats.interviewCorrect;

		player.money = PlayerStats.money;
        player.maxMoney = PlayerStats.maxMoney;
        player.bankMoney = PlayerStats.bankMoney;
        player.interestRate = PlayerStats.interestRate;
		player.wage = PlayerStats.wage;
		player.weeklyCosts = PlayerStats.weeklyCosts;
        player.conversionRate = PlayerStats.conversionRate;
        player.currency = PlayerStats.currency;

		player.rent = PlayerStats.rent;
		player.tuitionFee = PlayerStats.tuitionFee;
		player.internetFee = PlayerStats.internetFee;

		player.name = PlayerStats.name;
		player.jobName = PlayerStats.jobName;
        player.jobId = PlayerStats.jobId;
        player.lifeAspiration = PlayerStats.lifeAspiration;
        player.location = PlayerStats.location;

		player.hasJob = PlayerStats.hasJob;
		player.hasTuition = PlayerStats.hasTuition;
		player.hasHome = PlayerStats.hasHome;

		player.tuitionPayDay = PlayerStats.tuitionPayDay;
		player.rentPayDay = PlayerStats.rentPayDay;
		player.internetPayDay = PlayerStats.internetPayDay;
        player.gymMembership = PlayerStats.gymMembership;
        player.gymRenewalDay = PlayerStats.gymRenewalDay;

		player.onCourse = PlayerStats.onCourse;
		player.currentCourse = PlayerStats.currentCourse;
		player.currentCourseRemaining = PlayerStats.currentCourseRemaining;

		player.knowledge = PlayerStats.knowledge;
		player.strength = PlayerStats.strength;
		player.physique = PlayerStats.physique;
		player.coolness = PlayerStats.coolness;
		player.social = PlayerStats.social;
		player.creativity = PlayerStats.creativity;
		player.numJobs = PlayerStats.numJobs;
		player.maths = PlayerStats.maths;
		player.english = PlayerStats.english;
		player.physics = PlayerStats.physics;
        player.chemistry = PlayerStats.chemistry;
        player.biology = PlayerStats.biology;
		player.ict = PlayerStats.ict;
        player.geography = PlayerStats.geography;
        player.history = PlayerStats.history;
        player.business = PlayerStats.business;

		player.drivingLicense = PlayerStats.drivingLicense;
		player.brickLaying = PlayerStats.brickLaying;
		player.gameDesign = PlayerStats.gameDesign;
		player.banking = PlayerStats.banking;
		player.finance = PlayerStats.finance;
		player.catering = PlayerStats.catering;
		player.trainLicense = PlayerStats.trainLicense;
		player.archeology = PlayerStats.archeology;
		player.firstAid = PlayerStats.firstAid;

        player.programming = PlayerStats.programming;
        player.computing = PlayerStats.computing;
        player.economics = PlayerStats.economics;
        player.calculus = PlayerStats.calculus;
        player.art = PlayerStats.art;
        player.animation = PlayerStats.animation;
        player.law = PlayerStats.law;

		player.attractiveness = PlayerStats.attractiveness;

        player.playMusic = PlayerStats.playMusic;
        player.playSounds = PlayerStats.playSounds;
		player.lifeScore = PlayerStats.lifeScore;
		player.autoPayBills = PlayerStats.autoPayBills;
		player.billsPayed = PlayerStats.billsPayed;
		player.infoButtons = PlayerStats.infoButtons;
        player.autoLoadGame = PlayerStats.autoLoadGame;
        player.disableOffensiveJokes = PlayerStats.disableOffensiveJokes;
        player.enableMinigames = PlayerStats.enableMinigames;
        player.gambles = PlayerStats.gambles;
        player.punchbagHighscore = PlayerStats.punchbagHighscore;

		player.bossHappiness = PlayerStats.bossHappiness;
		player.workStartTime = PlayerStats.workStartTime;
		player.workEndTime = PlayerStats.workEndTime;
		player.workEnergyRequired = PlayerStats.workEnergyRequired;

        player.entertainmentItems = PlayerStats.entertainmentItems;
        player.houseRoom = PlayerStats.houseRoom;
        player.plantHealth = PlayerStats.plantHealth;

        player.rewardTime = PlayerStats.rewardTime;
        player.daysReturned = PlayerStats.daysReturned;
		player.timesGuitarPlayed = PlayerStats.timesGuitarPlayed;
        player.tutorials = PlayerStats.tutorials;

        player.moneySpent = PlayerStats.moneySpent;
        player.daysWorked = PlayerStats.daysWorked;
        player.numJobs = PlayerStats.numJobs;
        player.classesAttended = PlayerStats.classesAttended;

		player.lastPlayed = PlayerStats.lastPlayed;
        player.interestGained = PlayerStats.interestGained;

        player.num1 = PlayerStats.num1;
        player.num2 = PlayerStats.num2;
        player.num3 = PlayerStats.num3;
        player.num4 = PlayerStats.num4;
        player.num5 = PlayerStats.num5;

        player.tweetedUs = PlayerStats.tweetedUs;
        player.followedAaron = PlayerStats.followedAaron;
        player.followedAfie = PlayerStats.followedAlfie;
        player.followedBrandon = PlayerStats.followedBrandon;
        player.followedKlaus = PlayerStats.followedKlaus;
        player.followedFVG = PlayerStats.followedFVG;

        player.code1 = PlayerStats.code1;
        player.code2 = PlayerStats.code2;
        player.code3 = PlayerStats.code3;
        player.code4 = PlayerStats.code4;
        player.code5 = PlayerStats.code5;

        player.version = PlayerStats.version;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
		bf.Serialize (file, player);

		file.Close ();

		BinaryFormatter bf2 = new BinaryFormatter ();

        FileStream file2 = File.Create (Application.persistentDataPath + "/inventory.Life");
		Inventory inv = new Inventory ();

		inv.numFood = PlayerInventory.numFood;
		inv.numEnergyDrinks = PlayerInventory.numEnergyDrinks;
		inv.numHealthPacks = PlayerInventory.numHealthPacks;

        inv.gymMembership = PlayerInventory.hasGymMembership;

		inv.hasHome = PlayerInventory.hasHome;
        inv.hasHostelHome = PlayerInventory.hasHostelHome;
		inv.hasHouse1 = PlayerInventory.hasLevel1Home;
		inv.hasHouse2 = PlayerInventory.hasLevel2Home;
		inv.hasHouse3 = PlayerInventory.hasLevel3Home;
		inv.hasHouse4 = PlayerInventory.hasLevel4Home;
		inv.hasHouse5 = PlayerInventory.hasLevel5Home;
		inv.hasHouse6 = PlayerInventory.hasLevel6Home;
		inv.hasHouse7 = PlayerInventory.hasLevel7Home;

        inv.hasPlant = PlayerInventory.hasPlant;

		inv.hasStandardTelevision = PlayerInventory.hasStandardTelevision;
		inv.hasMusicPlayer = PlayerInventory.hasMusicPlayer;
		inv.hasLaptop = PlayerInventory.hasLaptop;
		inv.hasInternet = PlayerInventory.hasInternet;
		inv.hasFullHdTv = PlayerInventory.hasFullHdTv;
		inv.hasGamingPC = PlayerInventory.hasGamingPC;
		inv.hasVirtualReality = PlayerInventory.hasVirtualReality;
		inv.hasTimeMachine = PlayerInventory.hasTimeMachine;
        inv.hasXboxOne = PlayerInventory.hasXboxOne;
        inv.hasPS4 = PlayerInventory.hasPS4;
        inv.hasBooks = PlayerInventory.hasBooks;

        inv.hasMicrowave = PlayerInventory.hasMicrowave;
        inv.hasLamp = PlayerInventory.hasLamp;

        inv.bookWorgensTale = PlayerInventory.bookWorgensTale;
        inv.bookUnforgiven = PlayerInventory.bookUnforgiven;
        inv.bookLoopOfLife = PlayerInventory.bookLoopOfLife;

        inv.gameSnake = PlayerInventory.gameSnake;

		bf2.Serialize (file2, inv);
		file2.Close ();

		BinaryFormatter bf3 = new BinaryFormatter ();
        FileStream file3 = File.Create (Application.persistentDataPath + "/challenges.Life");
		Challenges c = new Challenges ();

		c.c1 = ChallengeManager.challenge1;
		c.c2 = ChallengeManager.challenge2;
		c.c3 = ChallengeManager.challenge3;
		c.c4 = ChallengeManager.challenge4;
		c.c5 = ChallengeManager.challenge5;
		c.c6 = ChallengeManager.challenge6;
		c.c7 = ChallengeManager.challenge7;
		c.c8 = ChallengeManager.challenge8;
		c.c9 = ChallengeManager.challenge9;
		c.c10 = ChallengeManager.challenge10;
        c.c11 = ChallengeManager.challenge11;
        c.c12 = ChallengeManager.challenge12;
        c.c13 = ChallengeManager.challenge13;
        c.c14 = ChallengeManager.challenge14;
        c.c15 = ChallengeManager.challenge15;
        c.c16 = ChallengeManager.challenge16;
        c.c17 = ChallengeManager.challenge17;
        c.c18 = ChallengeManager.challenge18;
        c.c19 = ChallengeManager.challenge19;
        c.c20 = ChallengeManager.challenge20;

		bf3.Serialize (file3, c);
		file3.Close ();

        if (PlayerStats.lifeAspiration == "Youtuber")
        {
            BinaryFormatter bf4 = new BinaryFormatter();
            FileStream file4 = File.Create(Application.persistentDataPath + "/youtube.Life");
            Youtube yt = new Youtube();

            yt.channelName = YoutubeManager.channelName;
            yt.subscribers = YoutubeManager.subscribers;
            yt.totalViews = YoutubeManager.totalViews;
            yt.totalRevenue = YoutubeManager.totalRevenue;
            yt.videos = YoutubeManager.videos;

            yt.latestVideo = YoutubeManager.latestVideo;
            yt.latestVideoViews = YoutubeManager.latestVideoViews;
            yt.latestVideoRevenue = YoutubeManager.latestVideoRevenue;

            yt.hasChannel = YoutubeManager.hasChannel;
            yt.maxQuality = YoutubeManager.maxQuality;

            bf4.Serialize(file4, yt);
            file4.Close();
        }
    }

    public void Load()
    {
        if (File.Exists (Application.persistentDataPath + "/lifeSim.Life")) 
		{
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (Application.persistentDataPath + "/lifeSim.Life", FileMode.Open);
			Stats player = (Stats)bf.Deserialize (file);
			file.Close ();

            PlayerStats.timesPlayed = player.timesPlayed;

			PlayerStats.FBName = player.FBName;

			PlayerStats.days = player.days;
			PlayerStats.hours = player.hours;
            PlayerStats.minutes = player.minutes;
			PlayerStats.weeks = player.weeks;
            PlayerStats.months = player.months;
			PlayerStats.lastWorkDay = player.lastWorkDay;
            PlayerStats.jobDesc = player.jobDesc;

			PlayerStats.classesTaken = player.classesTaken;

			PlayerStats.health = player.health;
			PlayerStats.hunger = player.hunger;
			PlayerStats.energy = player.energy;
			PlayerStats.mood = player.mood;
			PlayerStats.experience = player.experience;

            PlayerStats.jobApplyDay = player.jobApplyDay;

            PlayerStats.interviewJobStartTime = player.interviewJobStartTime;
            PlayerStats.interviewJobEndTime = player.interviewJobEndTime;
            PlayerStats.interviewJobDelay = player.interviewJobDelay;

            PlayerStats.interviewJobName = player.jobName;
            PlayerStats.interviewJobDesc = player.interviewJobDesc;
            PlayerStats.interviewJobMinimumWage = player.interviewJobMinimumWage;
            PlayerStats.interviewJobID = player.interviewJobID;
            PlayerStats.waitingForInterview = player.waitingForInterview;

            PlayerStats.interviewTime = player.interviewTime;
            PlayerStats.interviewQuestions = player.interviewQuestions;
            PlayerStats.interviewAnswers = player.interviewAnswers;
            PlayerStats.interviewCorrect = player.interviewCorrect;

			PlayerStats.money = player.money;
            PlayerStats.maxMoney = player.maxMoney;
            PlayerStats.bankMoney = player.bankMoney;
            PlayerStats.interestRate = player.interestRate;
			PlayerStats.wage = player.wage;
			PlayerStats.weeklyCosts = player.weeklyCosts;
            PlayerStats.conversionRate = player.conversionRate;
            PlayerStats.currency = player.currency;

			PlayerStats.rent = player.rent;
			PlayerStats.tuitionFee = player.tuitionFee;
			PlayerStats.internetFee = player.internetFee;
            PlayerStats.gymMembership = player.gymMembership;
            PlayerStats.gymRenewalDay = player.gymRenewalDay;

			PlayerStats.name = player.name;
			PlayerStats.jobName = player.jobName;
            PlayerStats.jobId = player.jobId;

			PlayerStats.hasJob = player.hasJob;
			PlayerStats.hasTuition = player.hasTuition;
			PlayerStats.hasHome = player.hasHome;
            PlayerStats.lifeAspiration = player.lifeAspiration;
            PlayerStats.location = player.location;

			PlayerStats.tuitionPayDay = player.tuitionPayDay;
			PlayerStats.rentPayDay = player.rentPayDay;
			PlayerStats.internetPayDay = player.internetPayDay;

			PlayerStats.knowledge = player.knowledge;
			PlayerStats.strength = player.strength;
			PlayerStats.physique = player.physique;
			PlayerStats.coolness = player.coolness;
			PlayerStats.social = player.social;
			PlayerStats.creativity = player.creativity;

			PlayerStats.maths = player.maths;
			PlayerStats.english = player.english;
			PlayerStats.physics = player.physics;
            PlayerStats.biology = player.biology;
            PlayerStats.chemistry = player.chemistry;
			PlayerStats.ict = player.ict;
            PlayerStats.geography = player.geography;
            PlayerStats.history = player.history;
            PlayerStats.business = player.business;

            PlayerStats.programming = player.programming;
            PlayerStats.computing = player.computing;
            PlayerStats.economics = player.economics;
            PlayerStats.calculus = player.calculus;
            PlayerStats.art = player.art;
            PlayerStats.animation = player.animation;
            PlayerStats.law = player.law;

			PlayerStats.attractiveness = player.attractiveness;

            PlayerStats.playMusic = player.playMusic;
            PlayerStats.playSounds = player.playSounds;
			PlayerStats.lifeScore = player.lifeScore;
			PlayerStats.autoPayBills = player.autoPayBills;
			PlayerStats.numJobs = player.numJobs;
			PlayerStats.billsPayed = player.billsPayed;
			PlayerStats.infoButtons = player.infoButtons;
            PlayerStats.autoLoadGame = player.autoLoadGame;
            PlayerStats.disableOffensiveJokes = player.disableOffensiveJokes;
            PlayerStats.enableMinigames = player.enableMinigames;
            PlayerStats.gambles = player.gambles;
            PlayerStats.punchbagHighscore = player.punchbagHighscore;

			PlayerStats.bossHappiness = player.bossHappiness;
			PlayerStats.workStartTime = player.workStartTime;
			PlayerStats.workEndTime = player.workEndTime;
			PlayerStats.workEnergyRequired = player.workEnergyRequired;

            PlayerStats.entertainmentItems = player.entertainmentItems;
            PlayerStats.houseRoom = player.houseRoom;
            PlayerStats.plantHealth = player.plantHealth;

            PlayerStats.rewardTime = player.rewardTime;
            PlayerStats.daysReturned = player.daysReturned;
			PlayerStats.timesGuitarPlayed = player.timesGuitarPlayed;
            PlayerStats.tutorials = player.tutorials;

            PlayerStats.moneySpent = player.moneySpent;
            PlayerStats.daysWorked = player.daysWorked;
            PlayerStats.numJobs = player.numJobs;
            PlayerStats.classesAttended = player.classesAttended;

            PlayerStats.lastPlayed = player.lastPlayed;
            PlayerStats.interestGained = player.interestGained;

            PlayerStats.num1 = player.num1;
            PlayerStats.num2 = player.num2;
            PlayerStats.num3 = player.num3;
            PlayerStats.num4 = player.num4;
            PlayerStats.num5 = player.num5;

            PlayerStats.tweetedUs = player.tweetedUs;
            PlayerStats.followedAaron = player.followedAaron;
            PlayerStats.followedAlfie = player.followedAfie;
            PlayerStats.followedBrandon = player.followedBrandon;
            PlayerStats.followedKlaus = player.followedKlaus;
            PlayerStats.followedFVG = player.followedFVG;

			PlayerStats.code1 = player.code1;
			PlayerStats.code2 = player.code2;
			PlayerStats.code3 = player.code3;
			PlayerStats.code4 = player.code4;
			PlayerStats.code5 = player.code5;

			if (player.version == PlayerStats.version)
			{
				PlayerStats.onCourse = player.onCourse;
				PlayerStats.currentCourse = player.currentCourse;
				PlayerStats.currentCourseRemaining = player.currentCourseRemaining;

				PlayerStats.drivingLicense = player.drivingLicense;
				PlayerStats.brickLaying = player.brickLaying;
				PlayerStats.gameDesign = player.gameDesign;
				PlayerStats.banking = player.banking;
				PlayerStats.finance = player.finance;
				PlayerStats.catering = player.catering;
				PlayerStats.trainLicense = player.trainLicense;
				PlayerStats.archeology = player.archeology;
				PlayerStats.firstAid = player.firstAid;
			}
		}

		if (File.Exists (Application.persistentDataPath + "/inventory.Life")) 
		{
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file2 = File.Open (Application.persistentDataPath + "/inventory.Life", FileMode.Open);
			Inventory inv = (Inventory)bf.Deserialize (file2);
			file2.Close ();

			PlayerInventory.numFood = inv.numFood;
			PlayerInventory.numEnergyDrinks = inv.numEnergyDrinks;
			PlayerInventory.numHealthPacks = inv.numHealthPacks;

            PlayerInventory.hasGymMembership = inv.gymMembership;

			PlayerInventory.hasHome = inv.hasHome;
            PlayerInventory.hasHostelHome = inv.hasHostelHome;
			PlayerInventory.hasLevel1Home = inv.hasHouse1;
			PlayerInventory.hasLevel2Home = inv.hasHouse2;
			PlayerInventory.hasLevel3Home = inv.hasHouse3;
			PlayerInventory.hasLevel4Home = inv.hasHouse4;
			PlayerInventory.hasLevel5Home = inv.hasHouse5;
			PlayerInventory.hasLevel6Home = inv.hasHouse6;
			PlayerInventory.hasLevel7Home = inv.hasHouse7;

            PlayerInventory.hasPlant = inv.hasPlant;

			PlayerInventory.hasStandardTelevision = inv.hasStandardTelevision;
			PlayerInventory.hasMusicPlayer = inv.hasMusicPlayer;
			PlayerInventory.hasLaptop = inv.hasLaptop;
			PlayerInventory.hasInternet = inv.hasInternet;
			PlayerInventory.hasFullHdTv = inv.hasFullHdTv;
			PlayerInventory.hasGamingPC = inv.hasGamingPC;
			PlayerInventory.hasVirtualReality = inv.hasVirtualReality;
			PlayerInventory.hasTimeMachine = inv.hasTimeMachine;
            PlayerInventory.hasXboxOne = inv.hasXboxOne;
            PlayerInventory.hasPS4 = inv.hasPS4;
            PlayerInventory.hasBooks = inv.hasBooks;

            PlayerInventory.youtubeQualityLevel = inv.youtubeQualityLevel;
            PlayerInventory.hasQProcessor99 = inv.hasQProcessor99;
            PlayerInventory.hasLowKeyMicrophone = inv.hasLowKeyMicrophone;
            PlayerInventory.hasVMEditing = inv.hasVMEditing;
            PlayerInventory.hasYOAnimator = inv.hasYOAnimator;
            PlayerInventory.hasYintelZ780 = inv.hasYintelZ780;
            PlayerInventory.hasPonyVegas13 = inv.hasPonyVegas13;
            PlayerInventory.hasHumanWareK69 = inv.hasHumanWareK69;
            PlayerInventory.hasLiveSpeakerMic = inv.hasLiveSpeakerMic;

            PlayerInventory.hasMicrowave = inv.hasMicrowave;
            PlayerInventory.hasLamp = inv.hasLamp;

            PlayerInventory.bookWorgensTale = inv.bookWorgensTale;
            PlayerInventory.bookUnforgiven = inv.bookUnforgiven;
            PlayerInventory.bookLoopOfLife = inv.bookLoopOfLife;

            PlayerInventory.gameSnake = inv.gameSnake;

		}


		if (File.Exists (Application.persistentDataPath + "/challenges.Life")) 
		{
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file3 = File.Open (Application.persistentDataPath + "/challenges.Life", FileMode.Open);
			Challenges c = (Challenges)bf.Deserialize (file3);
			file3.Close ();

			ChallengeManager.challenge1 = c.c1;
			ChallengeManager.challenge2 = c.c2;
			ChallengeManager.challenge3 = c.c3;
			ChallengeManager.challenge4 = c.c4;
			ChallengeManager.challenge5 = c.c5;
			ChallengeManager.challenge6 = c.c6;
			ChallengeManager.challenge7 = c.c7;
			ChallengeManager.challenge8 = c.c8;
			ChallengeManager.challenge9 = c.c9;
			ChallengeManager.challenge10 = c.c10;
            ChallengeManager.challenge11 = c.c11;
            ChallengeManager.challenge12 = c.c12;
            ChallengeManager.challenge13 = c.c13;
            ChallengeManager.challenge14 = c.c14;
            ChallengeManager.challenge15 = c.c15;
            ChallengeManager.challenge16 = c.c16;
            ChallengeManager.challenge17 = c.c17;
            ChallengeManager.challenge18 = c.c18;
            ChallengeManager.challenge19 = c.c19;
            ChallengeManager.challenge20 = c.c20;
		}

        if (File.Exists(Application.persistentDataPath + "/youtube.Life"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream yTfile = File.Open(Application.persistentDataPath + "/youtube.Life", FileMode.Open);
            Youtube yt = (Youtube)bf.Deserialize(yTfile);
            yTfile.Close();

            // Load Stats
            YoutubeManager.channelName = yt.channelName;
            YoutubeManager.subscribers = yt.subscribers;
            YoutubeManager.totalViews = yt.totalViews;
            YoutubeManager.totalRevenue = yt.totalRevenue;
            YoutubeManager.videos = yt.videos;

            YoutubeManager.latestVideo = yt.latestVideo;
            YoutubeManager.latestVideoViews = yt.latestVideoViews;
            YoutubeManager.latestVideoRevenue = yt.latestVideoRevenue;
            YoutubeManager.hasChannel = yt.hasChannel;
            YoutubeManager.maxQuality = yt.maxQuality;      
        }
    }
}  

    [System.Serializable]
class Stats
{
    public string version;

    public int timesPlayed;

    public bool FBLoggedIn;
	public string FBName;

	public int days;
	public int hours;
    public int minutes;
	public int weeks;
    public int months;
	public int lastWorkDay;

    public string jobDesc;

	public int classesTaken;

	public int test;
	public int health;
	public int hunger;
	public int energy;
	public int mood;
	public int experience;

    public int jobApplyDay;

    public int interviewJobStartTime;
    public int interviewJobEndTime;
    public string interviewJobName;
    public string interviewJobDesc;
    public float interviewJobMinimumWage;
    public int interviewJobDelay;
    public int interviewJobID;
    public bool waitingForInterview;

    public DateTime interviewTime;
    public string[] interviewQuestions;
    public string[] interviewAnswers;
    public int[] interviewCorrect;

    public float money;
    public float bankMoney;
    public float interestRate;
    public float maxMoney;
    public float conversionRate;

    public string currency;

	public float wage;
	public float weeklyCosts;
	public float rent;
	public float tuitionFee;
	public float internetFee;
    public float gymMembership;
    public int gymRenewalDay;

	public string name;
	public string jobName;
    public int jobId;
    public string lifeAspiration;
    public string location;

	public bool hasJob;
	public bool hasTuition;
	public bool hasHome;

	// Pay Days
	public int tuitionPayDay;
	public int rentPayDay;
	public int internetPayDay;

		// States

	public bool fatigued;
	public bool ill;
	public bool happy;
	public bool sad;
	public bool bored;

		// Attributes
	public int attributePoints = 10;

	public int strength;
	public int physique;
	public int coolness;
	public int social;
	public int creativity;

    public int knowledge;

	// Courses
	public bool onCourse;
	public int currentCourse;
	public int currentCourseRemaining;

    // Classes
    public int maths;
    public int english;
    public int physics;
    public int chemistry;
    public int biology;
    public int ict;
    public int geography;
    public int history;
    public int business;
  
	public int programming;
    public int computing;
    public int economics;
    public int calculus;
    public int art;
    public int animation;
    public int law;

	// Qualifications
	public bool drivingLicense;
	public bool brickLaying;
	public bool gameDesign;
	public bool banking;
	public bool finance;
	public bool catering;
	public bool trainLicense;
	public bool archeology;
	public bool firstAid;

	// Random
    public int attractiveness;

    // World stuff
    public bool playMusic;
    public bool playSounds;    
	public int lifeScore;
	public bool autoPayBills;
	public int billsPayed;
	public bool infoButtons;
    public bool autoLoadGame;
    public bool disableOffensiveJokes;
    public bool enableMinigames;
    public int gambles;

	// Work stuff

	public int bossHappiness;
	public int workStartTime;
	public int workEndTime;
	public int workEnergyRequired;

    // House Stuff
    public int entertainmentItems;
    public int houseRoom;
    public float plantHealth;

    //Misc
    public DateTime rewardTime;
    public int daysReturned;
	public int timesGuitarPlayed;
    public bool tutorials;

    public float moneySpent;
    public int daysWorked;
    public int numJobs;
    public int classesAttended;
    public int punchbagHighscore;
    public int check;
    // Bank interest stuff

    public DateTime lastPlayed;
    public float interestGained;

    // Lottery
    public int num1;
    public int num2;
    public int num3;
    public int num4;
    public int num5;

    // Twitter
    public bool tweetedUs;
    public bool followedAaron;
    public bool followedAfie;
    public bool followedBrandon;
    public bool followedKlaus;
    public bool followedFVG;

    // Promo Codes
    public bool code1;
    public bool code2;
    public bool code3;
    public bool code4;
    public bool code5;

}

[System.Serializable]
class Inventory
{
	public int numFood;
	public int numEnergyDrinks;
	public int numHealthPacks;

    // Membership renewals
    public bool gymMembership;


	// Housing
	public bool hasHome;

    public bool hasHostelHome;
	public bool hasHouse1;
	public bool hasHouse2;
	public bool hasHouse3;
	public bool hasHouse4;
	public bool hasHouse5;
	public bool hasHouse6;
	public bool hasHouse7;

    // Decor
    public bool hasPlant; 

	// Entertainment
	public bool hasStandardTelevision;
	public bool hasMusicPlayer;
	public bool hasLaptop;
	public bool hasInternet;
	public bool hasFullHdTv;
	public bool hasGamingPC;
	public bool hasVirtualReality;
	public bool hasTimeMachine;
    public bool hasXboxOne;
    public bool hasPS4;
    public bool hasBooks;

    public int youtubeQualityLevel;
    public bool hasQProcessor99;
    public bool hasLowKeyMicrophone;
    public bool hasVMEditing;
    public bool hasYOAnimator;
    public bool hasYintelZ780;
    public bool hasPonyVegas13;
    public bool hasHumanWareK69;
    public bool hasLiveSpeakerMic;

    public bool hasMicrowave;
    public bool hasLamp;

    // Books
    public bool bookWorgensTale;
    public bool bookUnforgiven;
    public bool bookLoopOfLife;

    // Games
    public bool gameSnake;
}

[System.Serializable]
class Challenges
{
	public bool c1;
	public bool c2;
	public bool c3;
	public bool c4;
	public bool c5;
	public bool c6;
	public bool c7;
	public bool c8;
	public bool c9;
	public bool c10;
    public bool c11;
    public bool c12;
    public bool c13;
    public bool c14;
    public bool c15;
    public bool c16;
    public bool c17;
    public bool c18;
    public bool c19;
    public bool c20;
}

// Youtube
[System.Serializable]
class Youtube
{
    public string channelName;
    public int subscribers;
    public int totalViews;
    public int totalRevenue;
    public List<string> videos;

    public string latestVideo;
    public int latestVideoViews;
    public int latestVideoRevenue;

    public bool hasChannel;

    public int maxQuality;
    public bool releasedToday;
}
