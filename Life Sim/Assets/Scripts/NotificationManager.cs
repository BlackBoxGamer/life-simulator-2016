﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NotificationManager : MonoBehaviour 
{
	public static NotificationManager notif;

	public Text text;

	public string startText;

	public GameObject tab1;
	public GameObject tab2;
	public GameObject tab3;
	public GameObject tab4; 
	public GameObject tab5;

	void Awake()
	{
		notif = this.GetComponent<NotificationManager> ();
		text = GetComponentInChildren<Text> ();
	}

	void Update()
	{
		if (tab1.activeSelf)
			startText = "Home";
		if (tab2.activeSelf)
			startText = "Job";
		if (tab3.activeSelf)
			startText = "Player Statistics";
		if (tab4.activeSelf)
			startText = "Social";
		if (tab5.activeSelf)
			startText = "Settngs";
	}


	public static IEnumerator Notification(string message, Color colour)
	{
		Animator anim = notif.GetComponentInChildren<Animator> ();

		Debug.Log ("Playing animation");

		anim.SetBool ("hasNotification", true);
		yield return new WaitForSeconds (0.25f);
		notif.text.text = message;
		notif.text.color = colour;

		yield return new WaitForSeconds (2.6f);

		notif.text.text = notif.startText;
		notif.text.color = Color.white;
		anim.SetBool ("hasNotification", false);

	}


}
