﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShopBuy : MonoBehaviour
{
    bool canClick = true;

    public int price;
    public int lifeScoreGain;

    public string itemName;
    public string itemDesc;
    public Sprite itemIcon;

    public bool isOwned;
    public bool oneTimeOnly;

    public bool isFood;
    public bool isEnergy;
    public bool isHealth;

    public bool isItem;
    public bool isEntertainmentItem;
    public bool isHouse;

    public string call;

    public Color boughtColour = new Color(83, 83, 83, 66);

    public GameObject itemInfoPanel;
    public Text itemNameText;
    public Text itemDescText;
    public Image itemIconImage;
    public Image itemIconImage2;
    public Image itemIconImage3;
    public Button purchaseButton;
    public Text priceText;

    public Color canBuyCol;
    public Color cannotBuyCol;

    int clicks = 0;

    public bool isSelected;

    void Start()
    {
        itemNameText = itemInfoPanel.transform.GetChild(0).GetComponent<Text>();
        itemDescText = itemInfoPanel.transform.GetChild(1).GetComponent<Text>();
        itemIconImage = itemInfoPanel.transform.GetChild(2).GetChild(0).GetComponent<Image>();
        itemIconImage2 = itemInfoPanel.transform.GetChild(2).GetChild(1).GetComponent<Image>();
        itemIconImage3 = itemInfoPanel.transform.GetChild(2).GetChild(2).GetComponent<Image>();
        purchaseButton = itemInfoPanel.transform.GetChild(3).GetComponent<Button>();
        priceText = itemInfoPanel.transform.GetChild(3).GetComponentInChildren<Text>();
    }

    public void ShowItem()
    {
        if (canClick)
        {
            transform.parent.parent.gameObject.BroadcastMessage("ResetPanel");
            clicks++;

            if (itemIconImage2.gameObject.activeSelf)
                itemIconImage2.gameObject.SetActive(false);

            if (itemIconImage3.gameObject.activeSelf)
                itemIconImage3.gameObject.SetActive(false);

            itemNameText.text = itemName;
            itemDescText.text = itemDesc;
            itemIconImage.sprite = itemIcon;
            priceText.text = "£" + price.ToString();

            isSelected = true;

            if (PlayerStats.money >= price)
            {
                purchaseButton.interactable = true;
                purchaseButton.image.color = canBuyCol;
            }
            else
            {
                purchaseButton.interactable = false;
                purchaseButton.image.color = cannotBuyCol;
            }

            itemInfoPanel.SetActive(true);
            itemInfoPanel.GetComponent<EasyTween>().OpenCloseObjectAnimation();
            StartCoroutine("ClickDelay");
            canClick = false;
            Debug.Log("Opened Panel");
        }
    }

    IEnumerator ClickDelay()
    {
        yield return new WaitForSeconds(0.3f);
        canClick = true;
    }

    public void BuyItem()
    {
        if (isSelected)
        {
            PlayerStats.money -= price;
            PlayerStats.moneySpent += price;
            AlertManager.Alert("-£" + price.ToString(), Color.red, AlertType.Bottom, 1f);

            if (oneTimeOnly)
                isOwned = true;
            if (isFood)
                PlayerInventory.numFood++;
            if (isEnergy)
                PlayerInventory.numEnergyDrinks++;
            if (isHealth)
                PlayerInventory.numHealthPacks++;
            if (isEntertainmentItem)
                PlayerStats.entertainmentItems++;

            gameObject.SendMessage(call);

            EnableBuyState();

            isSelected = false;
        }
    }

    public void EnableBuyState()
    {
        transform.parent.GetComponent<Image>().color = boughtColour;
        GetComponent<Button>().interactable = false;
        isOwned = true;
        GetComponent<ShopBuy>().enabled = false;
        isSelected = false;

        if (itemInfoPanel.activeSelf)
        {
            itemInfoPanel.SetActive(false);
        }
    }

    public void ResetPanel()
    {
        if (itemInfoPanel.activeSelf)
        {
            itemInfoPanel.SetActive(false);
        }

        isSelected = false;
    }

    void ClearHouses()
    {
        PlayerInventory.hasHostelHome = false;
        PlayerInventory.hasLevel1Home = false;
        PlayerInventory.hasLevel2Home = false;
        PlayerInventory.hasLevel3Home = false;
        PlayerInventory.hasLevel4Home = false;
        PlayerInventory.hasLevel5Home = false;
        PlayerInventory.hasLevel6Home = false;
        PlayerInventory.hasLevel7Home = false;
    }

    public void BuyHostelRoom()
    {
        ClearHouses();
        PlayerInventory.hasHostelHome = true;
        PlayerInventory.hasHome = true;
        PlayerStats.hasHome = true;
    }

	public void BuyHouse1()
	{
        ClearHouses();
        PlayerInventory.hasLevel1Home = true;
		PlayerInventory.hasHome = true;
		PlayerStats.hasHome = true;
	}

	public void BuyHouse2()
	{
        ClearHouses();
        PlayerInventory.hasLevel2Home = true;
		PlayerInventory.hasHome = true;
		PlayerStats.hasHome = true;
    }

    public void BuyHouse3()
	{
        ClearHouses();
        PlayerInventory.hasLevel3Home = true;
		PlayerInventory.hasHome = true;
		PlayerStats.hasHome = true;
    }

    public void BuyHouse4()
	{
        ClearHouses();
        PlayerInventory.hasLevel4Home = true;
		PlayerInventory.hasHome = true;
		PlayerStats.hasHome = true;
    }

    public void BuyHouse5()
	{
        ClearHouses();
        PlayerInventory.hasLevel5Home = true;
		PlayerInventory.hasHome = true;
		PlayerStats.hasHome = true;
    }

    public void BuyHouse6()
	{
        ClearHouses();
        PlayerInventory.hasLevel6Home = true;
        PlayerInventory.hasHome = true;
		PlayerStats.hasHome = true;
    }
    public void BuyHouse7()
	{
        ClearHouses();
        PlayerInventory.hasLevel7Home = true;
		PlayerInventory.hasHome = true;
		PlayerStats.hasHome = true;
    }

    public void StandardTelevision()
	{
		PlayerInventory.hasStandardTelevision = true;
	}

	public void MusicPlayer()
	{
		PlayerInventory.hasMusicPlayer = true;
	}

    public void Lamp()
    {
        PlayerInventory.hasLamp = true;
    }

	public void BuyLaptop()
	{
		PlayerInventory.hasLaptop = true;
        Debug.Log("Bought Laptop");
    }

	public void BuyInternet()
	{
		PlayerInventory.hasInternet = true;
	}

	public void FullHDTV()
	{
		PlayerInventory.hasFullHdTv = true;
        Debug.Log("Bought Full HDTV");
    }

    public void XboxOne()
    {
        PlayerInventory.hasXboxOne = true;
        Debug.Log("Bought Xbox One");
    }

    public void PS4()
    {
        PlayerInventory.hasPS4 = true;
        Debug.Log("Bought PS4");
    }

    public void Books()
    {
        PlayerInventory.hasBooks = true;
        Debug.Log("Bought Books");
    }

	public void GamingPC()
	{
		PlayerInventory.hasGamingPC = true;
	}

	public void VirtualReality()
	{
		PlayerInventory.hasVirtualReality = true;
	}

	public void TimeMachine()
	{
		PlayerInventory.hasTimeMachine = true;
	}

    public void qProcessor99()
    {
        PlayerInventory.hasQProcessor99 = true;
        PlayerInventory.youtubeQualityLevel = 2;
    }

    public void LowKeyMicrophone()
    {
        PlayerInventory.hasLowKeyMicrophone = true;
        PlayerInventory.youtubeQualityLevel = 3;
    }

    public void VMEditing()
    {
        PlayerInventory.hasVMEditing = true;
        PlayerInventory.youtubeQualityLevel = 4;
    }

    public void YOAnimator()
    {
        PlayerInventory.hasYOAnimator = true;
        PlayerInventory.youtubeQualityLevel = 6;
    }

    public void YintelZ780()
    {
        PlayerInventory.hasYintelZ780 = true;
        PlayerInventory.youtubeQualityLevel = 7;
    }

    public void PonyVegas13()
    {
        PlayerInventory.hasPonyVegas13 = true;
        PlayerInventory.youtubeQualityLevel = 7;
    }

    public void HumanWareK69()
    {
        PlayerInventory.hasHumanWareK69 = true;
        PlayerInventory.youtubeQualityLevel = 10;
    }

    public void LiveSpeakerMic()
    {
        PlayerInventory.hasLiveSpeakerMic = true;
        PlayerInventory.youtubeQualityLevel = 10;
    }
}
