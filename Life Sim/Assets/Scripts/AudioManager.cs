﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AudioManager : MonoBehaviour
{
    public AudioSource music;

    public bool isSound;
    public bool isMusic;
    public bool isBills;
    public bool isInfoButton;
    public bool isOffensiveJokes;
    public bool isMinigames;

    void Awake()
    {
        if (isMusic)
            music = GameObject.FindGameObjectWithTag("Carried").GetComponent<AudioSource>();

        if (isMusic)
            GetComponent<Toggle>().isOn = PlayerStats.playMusic;
        if (isSound)
            GetComponent<Toggle>().isOn = PlayerStats.playSounds;
        if (isBills)
            GetComponent<Toggle>().isOn = PlayerStats.autoPayBills;
        if (isInfoButton)
            GetComponent<Toggle>().isOn = PlayerStats.infoButtons;
        if (isOffensiveJokes)
            GetComponent<Toggle>().isOn = PlayerStats.disableOffensiveJokes;
        if (isMinigames)
            GetComponent<Toggle>().isOn = PlayerStats.enableMinigames;
    }


    void Start()
    {
        if (isMusic)
        {
            if (!PlayerStats.playMusic)
            {
                music.mute = true;
                Debug.Log("Muting music");
            }
        }
    }

    public void Switch()
    {
        PlayerStats.playMusic = !PlayerStats.playMusic;

        if (PlayerStats.playMusic)
        {
            music.mute = false;
            Debug.Log("UnMuting Music");
        } else
        {
            music.mute = true;
            Debug.Log("Muting Music");
        }
    }

    public void ToggleSounds()
    {
        PlayerStats.playSounds = !PlayerStats.playSounds;
    }

	public void ToggleBills()
	{
		PlayerStats.autoPayBills = !PlayerStats.autoPayBills;
	}

	public void ToggleInfoButton()
	{
		PlayerStats.infoButtons = !PlayerStats.infoButtons;
	}

    public void ToggleOffensiveJokes()
    {
        PlayerStats.disableOffensiveJokes = !PlayerStats.disableOffensiveJokes;
    }

    public void ToggleMinigames()
    {
        PlayerStats.enableMinigames = !PlayerStats.enableMinigames;
    }






}
