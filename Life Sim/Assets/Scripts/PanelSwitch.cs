﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PanelSwitch : MonoBehaviour 
{
	public GameObject activePanel;

	public void ChangePanel(GameObject toOpen)
	{
		activePanel.SetActive (false);
		activePanel = toOpen;
		activePanel.SetActive (true);
	}
}
