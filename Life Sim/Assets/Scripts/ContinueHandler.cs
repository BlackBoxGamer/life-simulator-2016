﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;

public class ContinueHandler : MonoBehaviour 
{
	public void Start()
	{
		if (File.Exists (Application.persistentDataPath + "/lifeSim.Life")) 
		{
			GetComponent<Button> ().interactable = true;
		}
	}

	public void Load()
	{
		LoadScreenManager.LoadScene (4);
		SaveLoad.saveLoad.Load ();
	}

}
