﻿using UnityEngine;
using System.Collections;

public class ResetPosition : MonoBehaviour 
{
	bool hasChanged;

	void Update()
	{
		// Reset the position

		if (!hasChanged && gameObject.activeSelf) {
			GetComponent<RectTransform> ().offsetMin = new Vector2 (GetComponent<RectTransform> ().offsetMin.x, 0);
			GetComponent<RectTransform> ().offsetMax = new Vector2 (GetComponent<RectTransform> ().offsetMax.x, 0);
			hasChanged = true;
		} else if (!gameObject.activeSelf) 
		{
			hasChanged = false;
		}
	}

}
