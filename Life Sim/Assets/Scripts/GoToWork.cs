﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GoToWork : MonoBehaviour
{
    public playerInfoHandler LM;

    public JobUI job;

    public GameObject sendMessage;

    void Start()
    {
        LM = GameObject.FindGameObjectWithTag("LevelManager").GetComponent<playerInfoHandler>();
    }

	public void Work()
	{
		PlayerStats.energy -= PlayerStats.workEnergyRequired;
		PlayerStats.money += PlayerStats.wage;
		PlayerStats.lastWorkDay = PlayerStats.days;
		PlayerStats.experience++;
        PlayerStats.daysWorked++;
        AlertManager.Alert("-" + PlayerStats.workEnergyRequired + " energy", Color.red, AlertType.Middle, 1.2f);
		AlertManager.Alert ("+ £" + PlayerStats.wage, Color.green, AlertType.Middle, 1.5f);

		PlayerStats.hours = PlayerStats.workEndTime;
        PlayerStats.minutes = 0;

        sendMessage.BroadcastMessage("UpdateList");
        job.UpdateText();
        LM.UpdateToD();
	}

	public void Quit()
	{
        PlayerStats.jobId = 16;
        PlayerStats.hasJob = false;

        sendMessage.BroadcastMessage("UpdateList");
        job.UpdateText();
    }
}
