﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EatFood : MonoBehaviour 
{
	void Update()
	{
        GetComponentInChildren<Text>().text = PlayerInventory.numFood.ToString();

        if (PlayerInventory.numFood <= 0)
        {
            GetComponent<Button>().interactable = false;
        }
        else
        {
            GetComponent<Button>().interactable = true;
        }
	}

    public void Eat()
    {
        PlayerStats.mood += 20;
        PlayerInventory.numFood--;
        AlertManager.Alert("+20 Mood", Color.white, AlertType.Middle, 1f, 1f);
    }
}
