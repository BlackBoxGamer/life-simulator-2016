﻿using UnityEngine;
using UnityEngine.UI;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;
using System;
using System.Collections;
using UberAudio;

[System.Serializable]
public class playerInfoHandler : MonoBehaviour 
{
    public GameObject deathPanel;
    public GameObject noMoneyPanel;

    public GameObject settingsPanel;

    public Transform jobsList;

    public JobInfo playerJob;

    public PlantGrowthHandler plant;
    public ShopDealsHandler shopDeals;
	public CourseHandler college;

    public Image overlay;
    public Image sky;

    public Text timeText;

    public LevelManager LM;
    public Text sleepingText;

    public Color morningColor;
    public Color morningSkyColor;
    public Color afternoonColor;
    public Color afternoonSkyColor;
    public Color eveningColor;
    public Color eveningSkyColor;
    public Color nightColor;
    public Color nightSkyColor;

    bool morning;
    bool afternoon;
    bool evening;
    bool night;
    bool home;

    public bool lightOn;

    public bool canChangeTime = true;
    public bool canChangeMinute = true;

    void Awake()
    {
        LM = GetComponent<LevelManager>();
        overlay.gameObject.SetActive(true);

        overlay.CrossFadeAlpha(0, 2f, false);
    }

	void Start()
    {
        PlayerStats.timesPlayed++;
        InvokeRepeating("ChangeStats", 30f, 15f);
        InvokeRepeating("ChangeMinute", 5f, 1f);
        InvokeRepeating("AutoSave", 10f, 5f);
        InvokeRepeating("ShowAd", 600f, 400f);

        StartCoroutine("PlaySound");

        UpdateToD();
		#if UNITY_ANDROID
        String appKey = "7b7fc7c878b3d84f8c6daf3701f80242e88be7fc43809306";
		#endif

		#if UNITY_IOS
		String appKey = "2875d4d56c80092e18cacdf8dd86f4924500945bc1a7fb08";
		#endif
        Appodeal.disableLocationPermissionCheck();
        Appodeal.confirm(Appodeal.SKIPPABLE_VIDEO | Appodeal.REWARDED_VIDEO);
        Appodeal.initialize(appKey, Appodeal.SKIPPABLE_VIDEO | Appodeal.REWARDED_VIDEO);
    }

	void Update()
	{
        if (PlayerStats.money > PlayerStats.maxMoney)
            PlayerStats.maxMoney = PlayerStats.money;

		if (PlayerStats.health <= 0) {
			deathPanel.SetActive (true);
		} else {
			deathPanel.SetActive (false);
		}

        if (PlayerStats.minutes >= 60)
        {
            PlayerStats.minutes = PlayerStats.minutes - 60;
            ChangeHour();
        }

        if (PlayerStats.hours >= 24)
        {
            PlayerStats.hours = PlayerStats.hours - 24;
            NewDay();
        }

#if UNITY_EDITOR
        if (Input.GetKeyDown (KeyCode.Space)) 
		{
			PlayerStats.health = 0;
		}

        if (Input.GetKeyDown(KeyCode.L))
        {
            NewWeek();
        }

        if (Input.GetKeyDown(KeyCode.O))
        {
            PlayerStats.lifeScore += 9999999;
            PlayerStats.money += 10000;
        }

        if (Input.GetKey(KeyCode.P))
        {
            ChangeHour();
        }
#endif
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GetComponent<CloseTab>().Open();
        }

		if (PlayerStats.energy > 100)
			PlayerStats.energy = 100;
		if (PlayerStats.energy <= 0) 
			PlayerStats.energy = 0;
		if (PlayerStats.mood > 100)
			PlayerStats.mood = 100;
		if (PlayerStats.mood < 0)
			PlayerStats.mood = 0;
		if (PlayerStats.lifeScore < 0)
			PlayerStats.lifeScore = 0;
	}

    IEnumerator PlaySound()
    {
        yield return new WaitForSeconds(1f);

        UberAudio.AudioManager.Instance.Play("StreetAmbience");
    }

    public void ChangeMinute()
    {
        if (canChangeMinute)
        PlayerStats.minutes++;
    }

    public void ChangeHour()
    {
        if (PlayerStats.hours + 1 > 23)
        {
            PlayerStats.hours = 0;
            NewDay();
        }
        else
        {
            PlayerStats.hours++;

            gameObject.SendMessage("OnTimeChanged");

            // Overlay control

            if (canChangeTime)
            {
                if (PlayerStats.hours >= 5 && PlayerStats.hours < 10 && morning == false)
                {
                    if (LM.isHome && !lightOn)
                        overlay.CrossFadeAlpha(morningColor.a, 12.5f, false);
                    sky.CrossFadeColor(morningSkyColor, 12.5f, false, false);
                    morning = true;
                    night = false;
                    home = true;
                    LM.currentColour = morningColor;
                    Debug.Log("Changing to morning");
                }
                if (PlayerStats.hours >= 10 && PlayerStats.hours < 18 && afternoon == false)
                {
                    if (LM.isHome && !lightOn)
                        overlay.CrossFadeAlpha(afternoonColor.a, 6.25f, false);
                    sky.CrossFadeColor(afternoonSkyColor, 6.25f, false, false);
                    afternoon = true;
                    morning = false;
                    home = true;
                    LM.currentColour = afternoonColor;
                    Debug.Log("Changing to afternoon");
                }
                if (PlayerStats.hours >= 18 && PlayerStats.hours < 20 && evening == false)
                {
                    if (LM.isHome && !lightOn)
                        overlay.CrossFadeAlpha(eveningColor.a, 6.25f, false);
                    sky.CrossFadeColor(eveningSkyColor, 6.25f, false, false);
                    evening = true;
                    afternoon = false;
                    home = true;
                    LM.currentColour = eveningColor;
                    Debug.Log("Changing to evening");
                }
                if (PlayerStats.hours >= 20 && night == false || PlayerStats.hours < 5 && night == false)
                {
                    if (LM.isHome && !lightOn)
                        overlay.CrossFadeAlpha(nightColor.a, 8f, false);
                    sky.CrossFadeColor(nightSkyColor, 8f, false, false);
                    night = true;
                    evening = false;
                    home = true;
                    LM.currentColour = nightColor;
                    Debug.Log("Changing to night");
                }
            }
            if (PlayerInventory.hasPlant)
                plant.DecreaseHealth();
        }
    }

    public void UpdateToD()
    {
        if (PlayerStats.hours >= 5 && PlayerStats.hours < 10 && morning == false)
        {
            if (LM.isHome && !lightOn)
                overlay.CrossFadeAlpha(morningColor.a, 12.5f, false);
            sky.CrossFadeColor(morningSkyColor, 12.5f, false, false);
            morning = true;
            night = false;
            home = true;
            LM.currentColour = morningColor;
            Debug.Log("Changing to morning");
        }
        if (PlayerStats.hours >= 10 && PlayerStats.hours < 18 && afternoon == false)
        {
            if (LM.isHome && !lightOn)
                overlay.CrossFadeAlpha(afternoonColor.a, 6.25f, false);
            sky.CrossFadeColor(afternoonSkyColor, 6.25f, false, false);
            afternoon = true;
            morning = false;
            home = true;
            LM.currentColour = afternoonColor;
            Debug.Log("Changing to afternoon");
        }
        if (PlayerStats.hours >= 18 && PlayerStats.hours < 20 && evening == false)
        {
            if (LM.isHome && !lightOn)
                overlay.CrossFadeAlpha(eveningColor.a, 6.25f, false);
            sky.CrossFadeColor(eveningSkyColor, 6.25f, false, false);
            evening = true;
            afternoon = false;
            home = true;
            LM.currentColour = eveningColor;
            Debug.Log("Changing to evening");
        }
        if (PlayerStats.hours >= 20 && night == false || PlayerStats.hours < 5 && night == false)
        {
            if (LM.isHome && !lightOn)
                overlay.CrossFadeAlpha(nightColor.a, 8f, false);
            sky.CrossFadeColor(nightSkyColor, 8f, false, false);
            night = true;
            evening = false;
            home = true;
            LM.currentColour = nightColor;
            Debug.Log("Changing to night");
        }
    }

	void ChangeStats()
	{
		PlayerStats.energy--;
		PlayerStats.mood -= 2;
		if (PlayerStats.energy < 5 || PlayerStats.mood < 2) 
		{
			if (PlayerStats.energy < 2 && PlayerStats.mood < 2) {
				PlayerStats.health -= 10;
			} else {
				PlayerStats.health -= 5;
			}
		}
	}

	void NewDay()
	{
		PlayerStats.days++;
		PlayerStats.classesTaken = 0;

		if (PlayerStats.onCourse)
			college.ResetPanel ();

		if (PlayerStats.days % 8 == 0) 
		{
			NewWeek ();
		}

        if (PlayerStats.days % 31 == 0)
        {
            NewMonth();
        }

		if (!PlayerStats.hasJob) 
		{
			PlayerStats.money += 10;
		}

		if (PlayerStats.lastWorkDay < PlayerStats.days - 2 && PlayerStats.hasJob) 
		{
			PlayerStats.bossHappiness--;
		}
    }

    public void NewWeek()
	{
		PlayerStats.weeks++;
        shopDeals.NewOffer();
	}

    public void NewMonth()
    {
        PlayerStats.months++;
        GetComponent<Calender>().calenderGO.BroadcastMessage("NewMonth");
    }

    void AutoSave()
    {
        PlayerStats.lastPlayed = DateTime.Now;
        SaveLoad.saveLoad.Save();
        Debug.Log("Saving");
    }

	public void ShowAd()
	{
        Appodeal.show(Appodeal.SKIPPABLE_VIDEO);
        if (Appodeal.isLoaded(Appodeal.SKIPPABLE_VIDEO))
        {
            Debug.Log("Ad Loaded");
        }
	}

    void DisableOptions()
    {
        settingsPanel.SetActive(false);
    }
}