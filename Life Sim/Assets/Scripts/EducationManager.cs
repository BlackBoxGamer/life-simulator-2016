﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EducationManager : MonoBehaviour 
{
	public int cost;
	public int energyCost;
	public int timeCost;

	public GameObject educationTab;

    public int maxClasses = 5;

    void Update()
    {
        if (PlayerStats.money >= cost)
        {
            if (PlayerStats.energy >= energyCost)
            {
                if (PlayerStats.classesTaken < maxClasses)
                {
                    GetComponent<Button>().interactable = true;
                }
                else { GetComponent<Button>().interactable = false; }
            } else { GetComponent<Button>().interactable = false; }
        } else { GetComponent<Button>().interactable = false; }
    }

	public void Tuition()
	{
		if (PlayerStats.money >= cost) 
		{
			PlayerStats.money -= cost;
			PlayerStats.weeklyCosts += cost;
			PlayerStats.tuitionFee = 40;
            PlayerStats.moneySpent += 40;
			PlayerStats.tuitionPayDay = PlayerStats.days + 7;

			PlayerStats.hasTuition = true;
			educationTab.SetActive (true);
			transform.parent.gameObject.SetActive (false);
		}
		else {
			AlertManager.Alert ("Not enough money!", Color.red, AlertType.Middle, 2.5f);
		}
	}

	public void Maths()
	{
		if (PlayerStats.money >= cost && PlayerStats.classesTaken < maxClasses && PlayerStats.energy >= energyCost) 
		{
			PlayerStats.money -= cost;
			PlayerStats.energy -= energyCost;
			PlayerStats.hours += timeCost;
			PlayerStats.maths++;
			PlayerStats.knowledge++;
			PlayerStats.classesTaken++;
            PlayerStats.classesAttended++;
			PlayerStats.lifeScore++;
			AlertManager.Alert ("+1 Maths", Color.white, AlertType.Middle);
		}
	}

	public void English()
	{
		if (PlayerStats.money >= cost && PlayerStats.classesTaken < maxClasses && PlayerStats.energy >= energyCost) 
		{
			PlayerStats.money -= cost;
			PlayerStats.energy -= energyCost;
			PlayerStats.hours += timeCost;
			PlayerStats.english++;
			PlayerStats.knowledge++;
			PlayerStats.classesTaken++;
            PlayerStats.classesAttended++;
            PlayerStats.lifeScore++;
			AlertManager.Alert ("+1 English", Color.white, AlertType.Middle, 1f);
		}
	}

	public void Physics()
	{
		if (PlayerStats.money >= cost && PlayerStats.classesTaken < maxClasses && PlayerStats.energy >= energyCost) 
		{
			PlayerStats.money -= cost;
			PlayerStats.energy -= energyCost;
			PlayerStats.hours += timeCost;
			PlayerStats.physics++;
			PlayerStats.knowledge++;
			PlayerStats.classesTaken++;
            PlayerStats.classesAttended++;
            PlayerStats.lifeScore++;
			AlertManager.Alert ("+1 Physics", Color.white, AlertType.Middle, 1f);
		}
	}

	public void ICT()
	{
		if (PlayerStats.money >= cost && PlayerStats.classesTaken < maxClasses && PlayerStats.energy >= energyCost) 
		{
			PlayerStats.money -= cost;
			PlayerStats.energy -= energyCost;
			PlayerStats.hours += timeCost;
			PlayerStats.ict++;
			PlayerStats.knowledge++;
			PlayerStats.classesTaken++;
            PlayerStats.classesAttended++;
            PlayerStats.lifeScore++;
			AlertManager.Alert ("+1 ICT", Color.white, AlertType.Middle, 1f);
		}
	}

	public void Chemistry()
	{
		if (PlayerStats.money >= cost && PlayerStats.classesTaken < maxClasses && PlayerStats.energy >= energyCost) 
		{
			PlayerStats.money -= cost;
			PlayerStats.energy -= energyCost;
			PlayerStats.hours += timeCost;
			PlayerStats.chemistry++;
			PlayerStats.knowledge++;
			PlayerStats.classesTaken++;
            PlayerStats.classesAttended++;
            PlayerStats.lifeScore++;
			AlertManager.Alert ("+1 Chemistry", Color.white, AlertType.Middle, 1f);
		}
	}

	public void Biology()
	{
		if (PlayerStats.money >= cost && PlayerStats.classesTaken < maxClasses && PlayerStats.energy >= energyCost) 
		{
			PlayerStats.money -= cost;
			PlayerStats.energy -= energyCost;
			PlayerStats.hours += timeCost;
			PlayerStats.biology++;
			PlayerStats.knowledge++;
			PlayerStats.classesTaken++;
            PlayerStats.classesAttended++;
            PlayerStats.lifeScore++;
			AlertManager.Alert ("+1 Biology", Color.white, AlertType.Middle, 1f);
		}
	}

	public void Geography()
	{
		if (PlayerStats.money >= cost && PlayerStats.classesTaken < maxClasses && PlayerStats.energy >= energyCost) 
		{
			PlayerStats.money -= cost;
			PlayerStats.energy -= energyCost;
			PlayerStats.hours += timeCost;
			PlayerStats.geography++;
			PlayerStats.knowledge++;
			PlayerStats.classesTaken++;
            PlayerStats.classesAttended++;
            PlayerStats.lifeScore++;
			AlertManager.Alert ("+1 Geography", Color.white, AlertType.Middle, 1f);
		}
	}
	public void History()
	{
		if (PlayerStats.money >= cost && PlayerStats.classesTaken < maxClasses && PlayerStats.energy >= energyCost) 
		{
			PlayerStats.money -= cost;
			PlayerStats.energy -= energyCost;
			PlayerStats.hours += timeCost;
			PlayerStats.history++;
			PlayerStats.knowledge++;
			PlayerStats.classesTaken++;
            PlayerStats.classesAttended++;
            PlayerStats.lifeScore++;
			AlertManager.Alert ("+1 History", Color.white, AlertType.Middle, 1f);
		}
	}
	public void Business()
	{
		if (PlayerStats.money >= cost && PlayerStats.classesTaken < maxClasses && PlayerStats.energy >= energyCost) 
		{
			PlayerStats.money -= cost;
			PlayerStats.energy -= energyCost;
			PlayerStats.hours += timeCost;
			PlayerStats.business++;
			PlayerStats.knowledge++;
			PlayerStats.classesTaken++;
            PlayerStats.classesAttended++;
            PlayerStats.lifeScore++;
			AlertManager.Alert ("+1 Business", Color.white, AlertType.Middle, 1f);
		}
	}
	public void Programming()
	{
		if (PlayerStats.money >= cost && PlayerStats.classesTaken < maxClasses && PlayerStats.energy >= energyCost) 
		{
			PlayerStats.money -= cost;
			PlayerStats.energy -= energyCost;
			PlayerStats.hours += timeCost;
			PlayerStats.programming++;
			PlayerStats.knowledge++;
			PlayerStats.classesTaken++;
            PlayerStats.classesAttended++;
            PlayerStats.lifeScore++;
			AlertManager.Alert ("+1 Programming", Color.white, AlertType.Middle, 1f);
		}
	}
	public void Computing()
	{
		if (PlayerStats.money >= cost && PlayerStats.classesTaken < maxClasses && PlayerStats.energy >= energyCost) 
		{
			PlayerStats.money -= cost;
			PlayerStats.energy -= energyCost;	
			PlayerStats.hours += timeCost;
			PlayerStats.computing++;
			PlayerStats.knowledge++;
			PlayerStats.classesTaken++;
            PlayerStats.classesAttended++;
            PlayerStats.lifeScore++;
			AlertManager.Alert ("+1 Computing", Color.white, AlertType.Middle, 1f);
		}
	}
	public void Economics()
	{
		if (PlayerStats.money >= cost && PlayerStats.classesTaken < maxClasses && PlayerStats.energy >= energyCost) 
		{
			PlayerStats.money -= cost;
			PlayerStats.energy -= energyCost;
			PlayerStats.hours += timeCost;

			PlayerStats.economics++;
			PlayerStats.knowledge++;
			PlayerStats.classesTaken++;
            PlayerStats.classesAttended++;
            PlayerStats.lifeScore++;
			AlertManager.Alert ("+1 Economic", Color.white, AlertType.Middle, 1f);
		}
	}
	public void Calculus()
	{
		if (PlayerStats.money >= cost && PlayerStats.classesTaken < maxClasses && PlayerStats.energy >= energyCost) 
		{
			PlayerStats.money -= cost;
			PlayerStats.energy -= energyCost;
			PlayerStats.hours += timeCost;

			PlayerStats.calculus++;
			PlayerStats.knowledge++;
			PlayerStats.classesTaken++;
            PlayerStats.classesAttended++;
            PlayerStats.lifeScore++;
			AlertManager.Alert ("+1 Calculus", Color.white, AlertType.Middle, 1f);
		}
	}
	public void Art()
	{
		if (PlayerStats.money >= cost && PlayerStats.classesTaken < maxClasses && PlayerStats.energy >= energyCost) 
		{
			PlayerStats.money -= cost;
			PlayerStats.energy -= energyCost;
			PlayerStats.hours += timeCost;

			PlayerStats.art++;
			PlayerStats.knowledge++;
			PlayerStats.classesTaken++;
            PlayerStats.classesAttended++;
            PlayerStats.lifeScore++;
			AlertManager.Alert ("+1 Art", Color.white, AlertType.Middle, 1f);
		}
	}
	public void Animation()
	{
		if (PlayerStats.money >= cost && PlayerStats.classesTaken < maxClasses && PlayerStats.energy >= energyCost) 
		{
			PlayerStats.money -= cost;
			PlayerStats.energy -= energyCost;
			PlayerStats.hours += timeCost;

			PlayerStats.animation++;
			PlayerStats.knowledge++;
			PlayerStats.classesTaken++;
            PlayerStats.classesAttended++;
            PlayerStats.lifeScore++;
			AlertManager.Alert ("+1 Animation", Color.white, AlertType.Middle, 1f);
		}
	}
	public void Law()
	{
		if (PlayerStats.money >= cost && PlayerStats.classesTaken < maxClasses && PlayerStats.energy >= energyCost) 
		{
			PlayerStats.money -= cost;
			PlayerStats.energy -= energyCost;
			PlayerStats.hours += timeCost;

			PlayerStats.law++;
			PlayerStats.knowledge++;
			PlayerStats.classesTaken++;
            PlayerStats.classesAttended++;
            PlayerStats.lifeScore++;
			AlertManager.Alert ("+1 Law", Color.white, AlertType.Middle, 1f);
		}
	}
}
