﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SleepFunction : MonoBehaviour
{
    public playerInfoHandler LM;

    public Text energyGainedText;
    public Slider hoursSlider;

    public int hours;
    public int energyGained;

    public GameObject sleepPanel;
    public GameObject sleepMessage;

    public Text sleepText;

    public Image fade;

    public int timer;

    void Awake()
    {
        LM = GetComponent<playerInfoHandler>();
    }

    void Start()
    {
        hours = (int)hoursSlider.value;
        CalculateEnergyGained();
    }

    public void OpenPanel()
    {
        sleepPanel.SetActive(true);
    }

    public void OnHoursChanged()
    {
        hours = (int)hoursSlider.value;
        CalculateEnergyGained();
    }

    void CalculateEnergyGained()
    {
        if (hours < 10)
        {
            energyGained = hours * 10;
            energyGainedText.text = "Restores " + energyGained + " Energy";
        }
        else
        {
            energyGained = 100;
            energyGainedText.text = "Restores " + energyGained + " Energy";
        }
    }

    public void Sleep()
    {
        timer = hours;
        LM.canChangeTime = false;
        LM.canChangeMinute = false;
        sleepPanel.SetActive(false);
        fade.CrossFadeAlpha(1, 0.75f, false);
        InvokeRepeating("CountHours", 1, 1);
    }

    void CountHours()
    {
        if (timer > 0)
        {
            timer--;
            LM.ChangeHour();

            sleepMessage.SetActive(true);
        }
        else
        {
            PlayerStats.energy += energyGained;
            PlayerStats.health += energyGained / 2;
            PlayerStats.mood += energyGained / 3;
            LM.canChangeTime = true;
            LM.canChangeMinute = true;

            sleepMessage.SetActive(false);
            fade.CrossFadeAlpha(LM.LM.currentColour.a, 0.75f, false);
            LM.UpdateToD();

            CancelInvoke("CountHours");
        }
    }

    public void Cancel()
    {
        Debug.Log("Cancelling Sleep");

        PlayerStats.energy += energyGained;
        PlayerStats.health += energyGained / 2;
        PlayerStats.mood += energyGained / 3;
        LM.canChangeTime = true;
        LM.canChangeMinute = true;

        sleepMessage.SetActive(false);
        fade.CrossFadeAlpha(LM.LM.currentColour.a, 0.75f, false);
        LM.UpdateToD();

        CancelInvoke("CountHours");
    }

    public void Skip()
    {
        Debug.Log("Skipping Sleep");

        CancelInvoke("CountHours");

        LM.canChangeTime = false;
        LM.canChangeMinute = false;

        InvokeRepeating("SkipHours", 0.05f, 0.2f);
    }

    void SkipHours()
    {
        if (timer > 0)
        {
            timer--;
            LM.ChangeHour();

            sleepMessage.SetActive(true);
        }
        else
        {
            PlayerStats.energy += energyGained;
            PlayerStats.health += energyGained / 2;
            PlayerStats.mood += energyGained / 3;
            LM.canChangeTime = true;
            LM.canChangeMinute = true;

            sleepMessage.SetActive(false);
            fade.CrossFadeAlpha(LM.LM.currentColour.a, 0.75f, false);
            LM.UpdateToD();

            CancelInvoke("SkipHours");
        }
    }
}
