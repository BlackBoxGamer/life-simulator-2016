﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextHandler : MonoBehaviour
{
    public bool isFBName;

	public bool isJobName;
	public bool isWage;
	public bool isName;
	public bool isMoney;
	public bool isEnergy;
	public bool isHunger;
	public bool isDay;
	public bool isEnergyBar;
	public bool isHungerBar;
	public bool isMoodBar;

	public bool isStrength;
	public bool isKnowledge;
	public bool isCreativity;
	public bool isSocial;

	public bool isAttributePoints;

	public bool isMaths;
	public bool isPhysics;
    public bool isChemistry;
    public bool isBiology;
	public bool isEnglish;
	public bool isIct;
    public bool isGeography;
    public bool isHistory;
    public bool isBusiness;
    public bool isProgramming;
    public bool isComputing;
    public bool isEconomics;
    public bool isCalculus;
    public bool isArt;
    public bool isAnimation;
    public bool isLaw;

	public bool isClassesTaken;

	public bool isTime;

	public bool isLifeScore;

	public bool isExperience;

	public bool isPayRevive;

	public bool isWorkTimes;
	public bool isEnergyRequired;

    public bool isHouseRoom;

    public bool isMoneySpent;
    public bool isDaysWorked;
    public bool isNumJobs;
    public bool isClassesAttended;
    public bool isPromotions;

    public bool isRawKnowledge;
    public bool isRawCreativity;
    public bool isRawStrength;
    public bool isRawSocial;

	void Update()
	{
        if (isFBName)
            GetComponent<Text>().text = "Hello there, " + PlayerStats.FBName + "!";
		if (isJobName)
			GetComponent<Text> ().text = PlayerStats.jobName;
		if (isWage)
			GetComponent<Text> ().text = "Wage: " + PlayerStats.currency + PlayerStats.wage.ToString();
		if (isName)
			GetComponent<Text> ().text = PlayerStats.name;
		if (isMoney)
			GetComponent<Text> ().text = PlayerStats.money.ToString("F2");
		if (isEnergy)
			GetComponent<Text> ().text = "Energy: " + PlayerStats.energy.ToString ();
		if (isHunger)
			GetComponent<Text> ().text = "Hunger: " + PlayerStats.hunger.ToString ();
		if (isDay)
			GetComponent<Text> ().text = "Day: " + PlayerStats.days.ToString ();
		if(isEnergyBar)
			GetComponent<Text>().text = PlayerStats.energy.ToString();
		if (isHungerBar)
			GetComponent<Text> ().text = PlayerStats.health.ToString();
		if (isMoodBar)
			GetComponent<Text> ().text = PlayerStats.mood.ToString(); 
		if (isCreativity)
			GetComponent<Text> ().text = "Creativity: " + PlayerStats.creativity;
		if (isStrength) 
			GetComponent<Text> ().text = "Strength: " + PlayerStats.strength.ToString ();
		if (isKnowledge)
			GetComponent<Text> ().text = "Knowledge: " + PlayerStats.knowledge.ToString ();
		if (isSocial)
			GetComponent<Text> ().text = "Social: " + PlayerStats.social.ToString ();
		if (isAttributePoints)
			GetComponent<Text> ().text = "Attribute Points: " + PlayerStats.attributePoints.ToString ();
		if (isMaths)
			GetComponent<Text> ().text = "Maths: " + PlayerStats.maths.ToString ();
		if (isPhysics)
			GetComponent<Text> ().text = "Physics: " + PlayerStats.physics.ToString ();
        if (isChemistry)
            GetComponent<Text>().text = "Chemistry: " + PlayerStats.chemistry.ToString();
        if (isBiology)
            GetComponent<Text>().text = "Biology: " + PlayerStats.biology.ToString();
        if (isEnglish)
			GetComponent<Text> ().text = "English: " + PlayerStats.english.ToString ();
		if (isIct)
			GetComponent<Text> ().text = "ICT: " + PlayerStats.ict.ToString();
        if (isGeography)
            GetComponent<Text>().text = "Geography: " + PlayerStats.geography.ToString();
        if (isHistory)
            GetComponent<Text>().text = "History: " + PlayerStats.history.ToString();
        if (isBusiness)
            GetComponent<Text>().text = "Business: " + PlayerStats.history.ToString();
        if (isProgramming)
            GetComponent<Text>().text = "Programming: " + PlayerStats.programming.ToString();
        if (isComputing)
            GetComponent<Text>().text = "Computing: " + PlayerStats.computing.ToString();
        if (isEconomics)
            GetComponent<Text>().text = "Economics: " + PlayerStats.economics.ToString();
        if (isCalculus)
            GetComponent<Text>().text = "Calculus: " + PlayerStats.calculus.ToString();
        if (isArt)
            GetComponent<Text>().text = "Art: " + PlayerStats.art.ToString();
        if (isAnimation)
            GetComponent<Text>().text = "Animation: " + PlayerStats.animation.ToString();
        if (isLaw)
            GetComponent<Text>().text = "Law: " + PlayerStats.law.ToString();
		if (isLifeScore)
			GetComponent<Text> ().text = "Life Score: " + PlayerStats.lifeScore.ToString ();
		if (isExperience)
			GetComponent<Text>().text = "Experience: " + PlayerStats.experience;
        if (isMoneySpent)
            GetComponent<Text>().text = "Money Spent: " + PlayerStats.currency + PlayerStats.moneySpent;
        if (isDaysWorked)
            GetComponent<Text>().text = "Days Worked: " + PlayerStats.daysWorked;
        if (isNumJobs)
            GetComponent<Text>().text = "Jobs: " + PlayerStats.numJobs;
        if (isClassesAttended)
            GetComponent<Text>().text = "Classes Attended: " + PlayerStats.classesAttended;
		if (isClassesTaken)
			GetComponent<Text> ().text = "Classes today: " + PlayerStats.classesTaken + "/5";
		if (isPayRevive)
			GetComponent<Text> ().text = "Pay " + PlayerStats.currency + PlayerStats.lifeScore * 6;
		if (isEnergyRequired)
			GetComponent<Text> ().text = "Energy Required: " + GetComponentInParent<JobInfo> ().energyNeeded;
        if (isHouseRoom)
            GetComponent<Text>().text = "Current Item Space Used: " + PlayerStats.entertainmentItems + "/" + PlayerStats.houseRoom;
        if (isPromotions)
            GetComponent<Text>().text = "Promotions: " + PlayerStats.promotions;
        if (isRawCreativity)
            GetComponent<Text>().text = PlayerStats.creativity.ToString();
        if (isRawSocial)
            GetComponent<Text>().text = PlayerStats.social.ToString();
        if (isRawStrength)
            GetComponent<Text>().text = PlayerStats.strength.ToString();
        if (isRawKnowledge)
            GetComponent<Text>().text = PlayerStats.knowledge.ToString();
		if (isTime) 
		{
            if (PlayerStats.hours < 10)
            {
                if (PlayerStats.minutes < 10)
                {
                    GetComponent<Text>().text = "0" + PlayerStats.hours + ":0" + PlayerStats.minutes;
                }
                else
                {
                    GetComponent<Text>().text = "0" + PlayerStats.hours + ":" + PlayerStats.minutes;
                }
            }
            else if (PlayerStats.minutes < 10)
            {
                GetComponent<Text>().text = PlayerStats.hours + ":0" + PlayerStats.minutes;
            }
            else
            {
                GetComponent<Text>().text = PlayerStats.hours.ToString() +":" + PlayerStats.minutes.ToString();
            }

		}
		if (isWorkTimes) 
		{
			if (PlayerStats.workStartTime < 10) {
				GetComponent<Text> ().text = "0" + PlayerStats.workStartTime + ":00-" + PlayerStats.workEndTime + ":00";
			} else 
			{
				GetComponent<Text> ().text = PlayerStats.workStartTime + ":00-" + PlayerStats.workEndTime + ":00";
			}
		}
	}
}