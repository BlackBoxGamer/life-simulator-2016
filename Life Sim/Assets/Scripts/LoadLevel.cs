﻿using UnityEngine;
using System.Collections;

public class LoadLevel : MonoBehaviour 
{
	public int levelId;
	public string levelName;

	public void ShowLoadScreen()
	{
		LoadScreenManager.LoadScene (levelId);
	}

	public void ChangeScene()
	{
		Application.LoadLevel (levelName);
	}
}
