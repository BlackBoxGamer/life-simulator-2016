﻿using UnityEngine;
using System.Collections;
using System;
#if UNITY_ANDROID
using System.Runtime.Serialization.Formatters.Binary;
#endif
using System.IO;

[System.Serializable]
public class PlayerInventory
{
	// Consumable shop options

	public static int numFood = 5;
	public static int numEnergyDrinks = 5;
	public static int numHealthPacks = 2;

    // Memberships/Renewals
    public static bool hasGymMembership;

	//Housing

	public static bool hasHome;

    public static bool hasHostelHome;
	public static bool hasLevel1Home;
	public static bool hasLevel2Home;
	public static bool hasLevel3Home;
	public static bool hasLevel4Home;
	public static bool hasLevel5Home;
	public static bool hasLevel6Home;
	public static bool hasLevel7Home;

    // Decor
    public static bool hasPlant;


	// Entertainment

	public static bool hasStandardTelevision;
	public static bool hasMusicPlayer;
	public static bool hasLaptop;
	public static bool hasInternet;
	public static bool hasFullHdTv;
	public static bool hasGamingPC;
	public static bool hasVirtualReality;
	public static bool hasTimeMachine;
    public static bool hasXboxOne;
    public static bool hasPS4;
    public static bool hasBooks;

    // Youtube
    public static int youtubeQualityLevel = 1;
    public static bool hasQProcessor99;
    public static bool hasLowKeyMicrophone;
    public static bool hasVMEditing;
    public static bool hasYOAnimator;
    public static bool hasYintelZ780;
    public static bool hasPonyVegas13;
    public static bool hasHumanWareK69;
    public static bool hasLiveSpeakerMic;

    // Furniture
    public static bool hasMicrowave;
    public static bool hasLamp;

    // Books
    public static bool bookWorgensTale;
    public static bool bookUnforgiven;
    public static bool bookLoopOfLife;

    // Games
    public static bool gameSnake;


}
