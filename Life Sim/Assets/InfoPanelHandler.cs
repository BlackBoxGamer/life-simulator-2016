﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class InfoPanelHandler : MonoBehaviour, IPointerClickHandler
{
    public bool tvShows;
    public bool games;
    public bool information;

    public int numNames;
    public string[] names;
    public string[] descriptions;
    public string[] writtenBy;

    public string infoTitleText;
	public string infoDescText;
    public string infoPositivesText;
    public string infoNegativesText;
    public string writtenByText;

	public Transform infoPanel;
    public Text infoTitle;
    public Text infoDesc;
    public Text infoWritten;
    public Text infoPos;
    public Text infoNeg;

    public int selected;
    public int lastDaySeen;

    public GameObject closeInfo;

    void Start()
    {
        infoTitle = infoPanel.GetChild(0).GetComponent<Text>();
        infoDesc = infoPanel.GetChild(1).GetComponent<Text>();
        infoWritten = infoPanel.GetChild(2).GetComponent<Text>();
        infoPos = infoPanel.GetChild(3).GetComponent<Text>();
        infoNeg = infoPanel.GetChild(4).GetComponent<Text>();
    }

    public void ShowInformation()
	{
        closeInfo.SetActive(true);

        if (tvShows)
        {
            infoPos.gameObject.SetActive(false);
            infoNeg.gameObject.SetActive(false);
            infoWritten.gameObject.SetActive(true);

            if (PlayerStats.days != lastDaySeen)
            {
                int num = Random.Range(0, numNames);
                selected = num;
                lastDaySeen = PlayerStats.days;
                infoTitleText = "Watch: " + names[selected];
                infoDescText = descriptions[selected];
                writtenByText = writtenBy[selected];
            }
            else
            {
                infoTitleText = "Watch: " + names[selected];
                infoDescText = descriptions[selected];
                writtenByText = writtenBy[selected];
            }

            infoWritten.text = "Written By: " + writtenByText;
        }

        if (games)
        {
            infoPos.gameObject.SetActive(false);
            infoNeg.gameObject.SetActive(false);
            infoWritten.gameObject.SetActive(true);

            infoWritten.text = "Produced By: " + writtenByText;
        }

        if (information)
        {
            infoPos.gameObject.SetActive(true);
            infoNeg.gameObject.SetActive(true);
            infoWritten.gameObject.SetActive(false);

            infoPositivesText = infoPositivesText.Replace("BREAK ", "\n");
            infoNegativesText = infoNegativesText.Replace("BREAK ", "\n");

            infoPos.text = infoPositivesText;
            infoNeg.text = infoNegativesText;
        }

        infoTitle.text = infoTitleText;
        infoDesc.text = infoDescText;

        infoPanel.gameObject.SetActive(true);
	}

    public void HideInformation()
    {
        infoPanel.gameObject.SetActive(false);
        closeInfo.SetActive(false);
    }

    public void OnPointerClick(PointerEventData data)
    {
        if (information)
        ShowInformation();
    }
}
