﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CourseHandler : MonoBehaviour 
{
	public CollegeCourse[] courses;

	public CollegeCourse currentCourse;
	public int currentCourseID;
	public string currentCourseName;
	public int currentCourseLength;
	public int currentCourseRemaining;
	public string[] currentCourseLearnedText;
	public string[] currentCourseRandomText;

	public GameObject coursePanel;

	public PanelSwitch panels;

	[Header("UI Objects")]
	public GameObject noCoursePanel;
	public GameObject courseReadyPanel;
	public GameObject courseDonePanel;
	public GameObject courseCompletePanel;

	public Text courseReadyTitleText;
	public Text courseDoneTitleText;

	public Text learnedTodayText;	
	public Text randomTodayText;

	public GameObject completeCourseButton;
	public Text finishedText;

	public GameObject reqPanel;
	public Text reqText;
	public Text nedText;

	void Start()
	{
		if (PlayerStats.onCourse) 
		{
			currentCourseRemaining = PlayerStats.currentCourseRemaining;
			SetCourse (PlayerStats.currentCourse);
		}
	}

	public void SetCourse(int courseID)
	{
		// This sets the current course to a course from the courses array, defined with the course ID.
		PlayerStats.currentCourse = courseID;

		currentCourseID = courseID;
		currentCourse = courses [courseID];
		currentCourseLearnedText = currentCourse.learned;
		currentCourseRandomText = currentCourse.randomEvents;
		currentCourseName = currentCourse.courseName;

		if (PlayerStats.onCourse) 
		{
			currentCourseRemaining = PlayerStats.currentCourseRemaining;
		} 
		else 
		{
			PlayerStats.currentCourseRemaining = currentCourse.courseLength;
		}

		PlayerStats.onCourse = true;

		noCoursePanel.SetActive (false);
		courseReadyPanel.SetActive (true);
		courseDonePanel.SetActive (false);
		courseCompletePanel.SetActive (false);
		completeCourseButton.SetActive (false);

		courseReadyTitleText.text = currentCourseName;
		courseDoneTitleText.text = currentCourseName;

		panels.ChangePanel (coursePanel);
	}

	public void AttendCourse()
	{
		// This is the action that will occur when the player attends the course, show a text message saying what the player learned that day and things that happened.
		noCoursePanel.SetActive(false);
		courseReadyPanel.SetActive (false);
		courseDonePanel.SetActive (true);
		courseCompletePanel.SetActive (false);

		learnedTodayText.text = currentCourseLearnedText [currentCourse.courseLength - PlayerStats.currentCourseRemaining];
		randomTodayText.text = currentCourseRandomText [Random.Range (0, currentCourseRandomText.Length)];
		PlayerStats.knowledge ++;

		PlayerStats.currentCourseRemaining--;

		if (PlayerStats.currentCourseRemaining == 0) 
		{
			completeCourseButton.SetActive (true);
		} 
		else 
		{
			completeCourseButton.SetActive (false);
		}
	}

	public void ResetPanel()
	{
		if (PlayerStats.currentCourseRemaining > 0) 
		{
			noCoursePanel.SetActive (false);
			courseReadyPanel.SetActive (true);
			courseDonePanel.SetActive (false);
			courseCompletePanel.SetActive (false);
		}
		else 
		{
			noCoursePanel.SetActive (false);
			courseReadyPanel.SetActive (false);
			courseDonePanel.SetActive (false);
			courseCompletePanel.SetActive (true);
		}
	}

	public void GiveQualification()
	{
		currentCourse.GetQualification ();

		finishedText.text = "You have finished your " + currentCourseName + " course!";
		courseCompletePanel.SetActive (true);
	}

	public void UpdateList()
	{
		foreach (CollegeCourse c in courses) 
		{
			c.Check ();
		}
	}

}
