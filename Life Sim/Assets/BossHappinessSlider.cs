﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BossHappinessSlider : MonoBehaviour
{
	public Slider button;

	public Color angry;
	public Color unHappy;
	public Color neutral;
	public Color satisfied;
	public Color happy;

	public GameObject fillBar;

	Image fill;

	public Text bossText;

	void Start()
	{
		button = GetComponent<Slider> ();
		fill = fillBar.GetComponent<Image> ();
	}

	void Update()
	{
		button.value = PlayerStats.bossHappiness;

		if (PlayerStats.bossHappiness < 2) 
		{
			fill.color = angry;
			bossText.text = "Angry";
			bossText.color = angry;
		}

		if (PlayerStats.bossHappiness >= 2 && PlayerStats.bossHappiness < 5) 
		{
			fill.color = unHappy;
			bossText.text = "Unhappy";
			bossText.color = unHappy;
		}

		if (PlayerStats.bossHappiness == 5) 
		{
			fill.color = neutral;
			bossText.text = "Neutral";
			bossText.color = neutral;
		}

		if (PlayerStats.bossHappiness > 5 && PlayerStats.bossHappiness < 9) 
		{
			fill.color = satisfied;
			bossText.text = "Satisfied";
			bossText.color = satisfied;
		}

		if (PlayerStats.bossHappiness > 8 && PlayerStats.bossHappiness >= 10) 
		{
			fill.color = happy;
			bossText.text = "Happy";
			bossText.color = happy;
		}



	}

}
