﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;
using System;

public class JobUI : MonoBehaviour, IRewardedVideoAdListener
{
    public JobList jobs;

    public Text descBox;

    public Button applyButton;
    public Button interviewButton;
    public Button workButton;
    public Button quitButton;
    public Button skipButton;

    JobInfo selectedJob;

    public DateTime interviewTime;

    public Text interviewTimeText;

    public bool waitingForInterview;

    public Text jobName;
    public Text jobDesc;
    public Text jobWage;
    public Text jobHours;
    public Image jobIcon;

    public Text jobInfoText;
    public Text betterStatsText;

	public GameObject reqButton;
	public GameObject reqPanel;
	public Text reqText;
	public Text hasText;

    void Start()
    {
        if (PlayerStats.waitingForInterview == true)
        {
            interviewButton.gameObject.SetActive(true);
        }
    }

    public void SelectJob(JobInfo job)
    {
        selectedJob = job;

        job.UpdateList();

        descBox.gameObject.SetActive(true);
        descBox.text = job.desc + "\n \nWork Hours: " + job.startTime + ":00 - " + job.endTime + ":00" + "\n \nMinimum Pay: £" + job.wage;

        if (PlayerStats.waitingForInterview == false)
        {
            if (PlayerStats.jobName != job.jobName)
            {
                if (job.nextInterviewDay <= PlayerStats.days)
                {
                    if (job.canDo)
                    {
                        applyButton.interactable = true;
                        applyButton.gameObject.SetActive(true);
                        if (jobInfoText.gameObject.activeSelf)
                            jobInfoText.gameObject.SetActive(false);
                        if (betterStatsText.gameObject.activeSelf)
                            betterStatsText.gameObject.SetActive(false);
						if (reqButton.activeSelf)
							reqButton.SetActive (false);
                    }
                    else
                    {
                        applyButton.interactable = false;
                        applyButton.gameObject.SetActive(true);
                        if (jobInfoText.gameObject.activeSelf)
                            jobInfoText.gameObject.SetActive(false);
                        if (!betterStatsText.gameObject.activeSelf)
                            betterStatsText.gameObject.SetActive(true);
						if (!reqButton.activeSelf)
							reqButton.SetActive (true);
                    }
                }
                else
                {
                    applyButton.interactable = false;
                    applyButton.gameObject.SetActive(true);
                    jobInfoText.text = "You must wait: " + (job.nextInterviewDay - PlayerStats.days) + " days before applying again.";
                    jobInfoText.gameObject.SetActive(true);
                    if (betterStatsText.gameObject.activeSelf)
                        betterStatsText.gameObject.SetActive(false);
					if (!reqButton.activeSelf)
						reqButton.SetActive (true);
                }
            }
        }
    }

    public void ApplyForJob()
    {
        interviewTime = DateTime.Now.AddMinutes(selectedJob.wait);
        PlayerStats.interviewTime = interviewTime;

        PlayerStats.waitingForInterview = true;

        interviewButton.gameObject.SetActive(true);
        applyButton.gameObject.SetActive(false);
        interviewButton.interactable = false;
        interviewTimeText.gameObject.SetActive(true);

        PlayerStats.interviewJobID = selectedJob.id;
    }

    public void UpdateText()
    {
        JobInfo curJob = jobs.jobs[PlayerStats.jobId];

        PlayerStats.jobName = curJob.jobName;
        PlayerStats.jobDesc = curJob.desc;
        PlayerStats.wage = curJob.wage;
        PlayerStats.workStartTime = curJob.startTime;
        PlayerStats.workEndTime = curJob.endTime;

        jobName.text = PlayerStats.jobName;
        jobDesc.text = PlayerStats.jobDesc;
        jobWage.text = "Minumum Pay: £" + PlayerStats.wage;
        jobHours.text = "Work Hours: " + PlayerStats.workStartTime + ":00 - " + PlayerStats.workEndTime + ":00";

        jobIcon.sprite = curJob.icon;
    }

	public void ShowRequirements()
	{
		// This opens the requirement panel and updates the text based on your requirements and what you have compared to them.
		selectedJob.ShowRequirements(GetComponent<JobUI>());
	}

    void Update()
    {
        if (PlayerStats.hasJob)
        {
            workButton.gameObject.SetActive(true);
            quitButton.gameObject.SetActive(true);

            if (PlayerStats.lastWorkDay < PlayerStats.days)
            {
                if (PlayerStats.hours >= (PlayerStats.workStartTime - 1) && PlayerStats.hours < PlayerStats.workEndTime)
                {
                    workButton.interactable = true;
                }
                else { workButton.interactable = false; }
            }
            else { workButton.interactable = false; }
        }
        else
        {
            workButton.gameObject.SetActive(false);
            quitButton.gameObject.SetActive(false);
        }

        if (PlayerStats.waitingForInterview)
        {
            TimeSpan ts = PlayerStats.interviewTime.Subtract(DateTime.Now);

            interviewButton.gameObject.SetActive(true);

            if (DateTime.Now >= PlayerStats.interviewTime)
            {
                interviewTimeText.text = "00:00";
                interviewButton.interactable = true;
            }
            else
            {

                if (PlayerStats.interviewTime >= DateTime.Now.AddMinutes(10))
                {
                    skipButton.gameObject.SetActive(true);
                }
                else { skipButton.gameObject.SetActive(false); }
                 
                interviewButton.interactable = false;

                string hourPrefix;
                string minPrefix;
                string secPrefix;

                if (ts.Hours < 10)
                {
                    hourPrefix = "0";
                } else { hourPrefix = ""; }

                if (ts.Minutes < 10)
                {
                    minPrefix = "0";
                } else { minPrefix = ""; }

                if (ts.Seconds < 10)
                {
                    secPrefix = "0";
                } else { secPrefix = ""; }

                if (ts.Hours > 0)
                {
                    interviewTimeText.text = hourPrefix + ts.Hours + ":" + minPrefix + ts.Minutes + ":" + secPrefix + ts.Seconds;
                }
                else
                {
                    interviewTimeText.text = minPrefix + ts.Minutes + ":" + secPrefix + ts.Seconds;
                }
            }
        }
    }

    public void WatchAd()
    {
        Appodeal.show(Appodeal.REWARDED_VIDEO);
        Appodeal.setRewardedVideoCallbacks(this);
    }

    #region Rewarded Video callback handlers
    public void onRewardedVideoLoaded() { print("Video loaded"); }
    public void onRewardedVideoFailedToLoad() { print("Video failed"); }
    public void onRewardedVideoShown() { print("Video shown"); }
    public void onRewardedVideoClosed() { print("Video closed"); }
    public void onRewardedVideoFinished(int amount, string name) { SkipWait(); }
    #endregion


    void SkipWait()
    {
        PlayerStats.interviewTime = System.DateTime.Now;
    }

}
