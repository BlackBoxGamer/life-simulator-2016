﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ReadBook : MonoBehaviour
{
    public string story;
    public string bookName;
    public string author;

    public string[] pages;

    public int curPage;
    int nextPage;
    int lastPage;

    public GameObject bookPanel;
    public Text textBox;
    public Text titleText;
    public Text writtenByText;

    bool isLoaded;

    public GameObject previousButton;
    public GameObject nextButton;

    public int numberOfPages;


    public void LoadBook()
    {
        char delimiter = '/';
        pages = story.Split(delimiter);
        numberOfPages = pages.Length - 1;

        curPage = 0;
        nextPage = 1;
        lastPage = -1;

        titleText.text = bookName;
        writtenByText.text = "Written By: " + author;
        textBox.text = pages[curPage];
        bookPanel.SetActive(true);

        isLoaded = true;

        previousButton.SetActive(false);
        nextButton.SetActive(true);

        Time.timeScale = 0;
    }

    public void NextPage()
    {
        if (isLoaded)
        {
            curPage++;
            nextPage++;
            lastPage++;

            textBox.text = pages[curPage];

            if (!previousButton.activeSelf)
            previousButton.SetActive(true);

            if (nextPage > numberOfPages)
            {
                nextButton.SetActive(false);
            }
        }
    }

    public void LastPage()
    {
        if (isLoaded)
        {
            curPage--;
            nextPage--;
            lastPage--;

            textBox.text = pages[curPage];

            if (!nextButton.activeSelf)
            nextButton.SetActive(true);

            if (lastPage < 0)
                previousButton.SetActive(false);
        }
    }

    public void CloseBook()
    {
        if (isLoaded)
        {
            bookPanel.SetActive(false);
            Time.timeScale = 1f;
            isLoaded = false;
        }
    }
}
