﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LocationSettings : MonoBehaviour
{
    public float poundRate = 1f;
    public float dollarRate;
    public float yenRate;
    public float euroRate;
    public float otherRate;

    public void SetLocation()
    {
        int value;
        value = GetComponent<Dropdown>().value;

        if (value == 0)
            PlayerStats.location = "United Kingdom";
        if (value == 1)
            PlayerStats.location = "Europe";
        if (value == 2)
            PlayerStats.location = "United States";
        if (value == 3)
            PlayerStats.location = "China";
        if (value == 4)
            PlayerStats.location = "India";
        if (value == 5)
            PlayerStats.location = "Japan";
        if (value == 6)
            PlayerStats.location = "South Korea";
        if (value == 7)
            PlayerStats.location = "North Korea";
        if (value == 8)
            PlayerStats.location = "Africa";
        if (value == 9)
            PlayerStats.location = "Rest of World";

        SetCurrency();
    }

    void SetCurrency()
    {
        if (PlayerStats.location == "United Kingdom")
        {
            PlayerStats.currency = "£";
            PlayerStats.conversionRate = 1f;
        }

        if (PlayerStats.location == "Europe")
        {
            PlayerStats.currency = "€";
            PlayerStats.conversionRate = 1.31f;
        }
        if (PlayerStats.location == "United States" || PlayerStats.location == "Africa" || PlayerStats.location == "Rest of World" || PlayerStats.location == "China")
        {
            PlayerStats.currency = "$";
            PlayerStats.conversionRate = 1.57f;
        }

        if (PlayerStats.location == "India")
        {
            PlayerStats.currency = "₹";
            PlayerStats.conversionRate = 98.1f;
        }

        if (PlayerStats.location == "Japan")
        {
            PlayerStats.currency = "¥";
            PlayerStats.conversionRate = 160.79f;
        }

        if (PlayerStats.location == "South Korea" || PlayerStats.location == "North Korea")
        {
            PlayerStats.currency = "₩";
            PlayerStats.conversionRate = 1729.1f;
        }

        PlayerStats.money = 200;
        PlayerStats.money = PlayerStats.money *= (int)PlayerStats.conversionRate;

    }
}
