﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CasinoHandler : MonoBehaviour
{
    public AudioClip winSound;
    public AudioClip loseSound;

    AudioSource audio;

    public int energyCost;

    public int increment;

    public int currentBet;
    public int winnings;

    public int oldNumber;
    public int curNumber;
    public int maxNumber;
    public int minNumber;

    public bool easy;
    public bool medium;
    public bool hard;

    public GameObject betText;
    public GameObject numberText;
    public GameObject winText;

    public bool higher;
    public bool lower;
    public bool canPlay;

    public GameObject increaseButton;
    public GameObject decreaseButton;
    public GameObject higherButton;
    public GameObject lowerButton;

    public Color winColour;
    public Color loseColour;

    bool switch1;
    bool switch2;
    bool switch3;

    void Start()
    {
        audio = GetComponent<AudioSource>();

        oldNumber = 5;
        curNumber = 5;
        numberText.GetComponent<Text>().text = curNumber.ToString();

        winText.SetActive(false);

        currentBet = 10;
        betText.GetComponent<Text>().text = "Bet: "+ PlayerStats.currency + currentBet.ToString();

    }

    void Update()
    {
        numberText.GetComponent<Text>().text = curNumber.ToString();

        if (currentBet >= 10000)
        {
            increment = 1000;
        }
        else

        if (currentBet >= 1000 && currentBet < 10000)
        {
            increment = 100;
        }
        else
        {
            increment = 10;
        }


        if (currentBet + increment <= PlayerStats.money)
        {
            increaseButton.GetComponent<Button>().interactable = true;
        }
        else
        {
            increaseButton.GetComponent<Button>().interactable = false;
        }

        if (currentBet <= 10)
        {
            decreaseButton.GetComponent<Button>().interactable = false;
        }
        else
        {
            decreaseButton.GetComponent<Button>().interactable = true;
        }
        if (PlayerStats.energy >= energyCost)
        {
            if (currentBet > PlayerStats.money)
            {
                higherButton.GetComponent<Button>().interactable = false;
                lowerButton.GetComponent<Button>().interactable = false;
            }
            else
            {
                higherButton.GetComponent<Button>().interactable = true;
                lowerButton.GetComponent<Button>().interactable = true;
            }
        }
        else
        {
            higherButton.GetComponent<Button>().interactable = false;
            lowerButton.GetComponent<Button>().interactable = false;
        }

    }

    public void SetHigher()
    {
        higher = true;
        lower = false;
    }

    public void SetLower()
    {
        higher = false;
        lower = true;
    }

    public void IncreaseBet()
    {
        currentBet += increment;
        betText.GetComponent<Text>().text = "Bet: " + PlayerStats.currency + currentBet.ToString();  
    }

    public void DecreaseBet()
    {
        currentBet -= increment;
        betText.GetComponent<Text>().text = "Bet: "+ PlayerStats.currency + currentBet.ToString();
    }

    public void PlayGame()
    {
        PlayerStats.energy -= energyCost;

        higherButton.GetComponent<Button>().interactable = false;
        lowerButton.GetComponent<Button>().interactable = false;

        StartCoroutine("ChangeNumber");
    }

    void CheckWin()
    {
        winText.SetActive(true);

        if (oldNumber < curNumber && higher)
        {
            Win();

        }
        else if (oldNumber > curNumber && lower)
        {
            Win();
        }
        else if (oldNumber == curNumber)
        {
            Lose();
        }
        else
        {
            Lose();
        }

        oldNumber = curNumber;

        PlayerStats.gambles++;

        higherButton.GetComponent<Button>().interactable = true;
        lowerButton.GetComponent<Button>().interactable = true;
    }

    void Win()
    {
        PlayerStats.money += currentBet;

        winText.GetComponent<Text>().text = "Win!";
        winText.GetComponent<Text>().color = winColour;

        audio.PlayOneShot(winSound);
    }

    void Lose()
    {
        PlayerStats.money -= currentBet;

        winText.GetComponent<Text>().text = "Loss!";
        winText.GetComponent<Text>().color = loseColour;

        audio.PlayOneShot(loseSound);
    }

    IEnumerator ChangeNumber()
    {
        Debug.Log("Changing Number");

        float t = 60f;

        while (t > 0f)
        {
            yield return new WaitForSeconds(0.01f);
            curNumber = Random.Range(minNumber, maxNumber);
            t -= 1f;
            Debug.Log("Step 1" + t.ToString());
        }


        if (t <= 0f)
        {
            if (curNumber == oldNumber)
            {
                StartCoroutine("ChangeNumber");
                yield break;
            }

            CheckWin();
            StopCoroutine("ChangeNumber");
        }
    }


}
