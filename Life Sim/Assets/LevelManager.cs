﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class LevelManager : MonoBehaviour
{
    public GameObject homePanel;

    public GameObject activePanel;
    public GameObject activeButton;

    public GameObject gymPanel, noMembershipPanel, shopClosedPanel;
    public GameObject schoolPanel, bankPanel, goAwayPanel, shopPanel, locationsPanel, casinoPanel;

    public GameObject quickHomeButton, workButton, settingsButton, playerButton;
    public GameObject actionPanelButton;

    public Color currentColour;
    public Color notHomeColour;

    public GameObject overlayImage;
    public LampHandler lamp;

    public HomeHandler homes;

    public GameObject kickOutPanel;

	public GameObject HUD;

    public bool isHome;

    void Awake()
    {
        homes = GetComponent<HomeHandler>();
    }

    public void Save()
    {
        SaveLoad.saveLoad.Save();
        Debug.Log("Saved using the save button");
    }

    public void Load()
    {
        SaveLoad.saveLoad.Load();
        Debug.Log("Loaded the save file");
    }

    void Start()
    {
        if (PlayerStats.hasHome)
        {
            if (PlayerInventory.hasLevel1Home)
            {
                homePanel = homes.level1Home;
            }
            else if (PlayerInventory.hasLevel2Home)
            {
                homePanel = homes.level2Home;
            }
            else if (PlayerInventory.hasLevel3Home)
            {
                homePanel = homes.level3Home;
            }
        }
        else
        {
            homePanel = homes.homelessHome;
        }

        homePanel.SetActive(true);
        activePanel = homePanel;
        GetComponent<CloseTab>().open = homePanel;
    }

    void Update()
    {
        if (activePanel == homePanel)
        {
            isHome = true;
        }
        else
        {
            isHome = false;
        }
        if (PlayerStats.hasHome)
        {
            if (activePanel == gymPanel && !PlayerInventory.hasGymMembership)
            {
                noMembershipPanel.SetActive(true);
            }
            else { noMembershipPanel.SetActive(false); }

            if (PlayerInventory.hasLevel1Home)
            {
                if (activePanel == shopPanel)
                {
                    shopClosedPanel.SetActive(true);
                }
            }
        }
        else
        {
            if (activePanel == gymPanel || activePanel == bankPanel || activePanel == shopPanel || activePanel == casinoPanel)
            {
                if (activePanel == shopPanel)
                {
                    shopClosedPanel.SetActive(true);
                } else { goAwayPanel.SetActive(true); }
            }
            else
            {
                goAwayPanel.SetActive(false);
                shopClosedPanel.SetActive(false);
            }
        }

		if (activePanel == schoolPanel || activePanel == locationsPanel) 
		{
			HUD.SetActive (false);
		} 
		else 
		{
			HUD.SetActive (true);
		}

    }

    void OnApplicationQuit()
    {
        Debug.Log("Quitting Application, saving time.");
        
        if (!PlayerStats.resetting)
        {
            PlayerStats.lastPlayed = DateTime.Now;
            SaveLoad.saveLoad.Save();
        }
    }

}
