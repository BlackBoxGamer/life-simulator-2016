﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GetNews : MonoBehaviour
{
    public string url;
    public Text textBox;

    public string[] waste;

    public void UpdateNews()
    {
        StartCoroutine(Check());
    }

    private IEnumerator Check()
    {
        WWW w = new WWW(url);
        yield return w;

        if (w.error != null)
        {
            Debug.Log("Error .. " + w.error);
        }
        else
        {
            char delimiter = '~';
            waste = w.text.Split(delimiter);

            textBox.text = waste[0];
            Debug.Log(w.text);
        }
    }
}


