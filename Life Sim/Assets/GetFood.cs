﻿using UnityEngine;
using UnityEngine.UI;
using UberAudio;
using System.Collections;

public class GetFood : MonoBehaviour
{
    int lastDayUsed;

    int used;

    public bool homeless = true;
    public bool shop;

    void Update()
    {
        if (shop)
        {
            if (PlayerStats.money >= 2)
            {
                GetComponent<Button>().interactable = true;
            } else { GetComponent<Button>().interactable = false; }
        }
    }

    public void CollectFood()
    {
        if (used >= 3)
        {
            if (PlayerStats.days > lastDayUsed)
            {
                used = 0;
                Collect();
            }
            else
            {
                AlertManager.Alert("There's nothing left...", Color.red, AlertType.Middle, 2f, 1f);
            }
        }
        else
        {
            Collect();
        }
    }

    public void Buy(bool food)
    {
        PlayerStats.money -= 5;
        
        if (food)
            PlayerInventory.numFood += 3;
        if (!food)
            PlayerInventory.numEnergyDrinks += 3;
    }

    void Collect()
    {
        int num = Random.Range(0, 2);
        int numFound = Random.Range(1, 4);

        if (num == 0)
        {
            PlayerInventory.numFood += numFound;
            AlertManager.Alert("Found +" + numFound + " food", Color.white, AlertType.Middle, 1.5f, 1f); 
        }

        if (num == 1)
        {
            PlayerInventory.numEnergyDrinks += numFound;
            AlertManager.Alert("Found +" + numFound + " drinks", Color.white, AlertType.Middle, 1.5f, 1f);
        }

        used++;
        if (used >= 3)
            lastDayUsed = PlayerStats.days;
    }
}
