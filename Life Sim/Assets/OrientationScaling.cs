﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OrientationScaling : MonoBehaviour
{
    public Vector3 portraitSize;
    public Vector3 landscapeSize;

    public Vector3 portraitPos;
    public Vector3 landscapePos;

    public RectTransform size;

    public bool isPortrait;
    public bool isLandscape;

    void Start()
    {
        Debug.Log("SizeDelta: " + size.sizeDelta.ToString());
        Debug.Log("Anchored Pos: " + size.anchoredPosition.ToString());
        Debug.Log("Local Pos: " + size.localPosition.ToString());
    }

    void Update()
    {

        if (Input.deviceOrientation == DeviceOrientation.LandscapeLeft ||
            Input.deviceOrientation == DeviceOrientation.LandscapeRight)
        {
            size.sizeDelta = landscapeSize;
        }
        else if (Input.deviceOrientation == DeviceOrientation.Portrait)
        {
            size.sizeDelta = portraitSize;
        }

        if (Screen.width > Screen.height)
        {
                size.sizeDelta = landscapeSize;
    //            size.anchoredPosition = landscapePos;
                isPortrait = false;
                isLandscape = true;
        }
        else
        {
                size.sizeDelta = portraitSize;
     //           size.anchoredPosition = portraitPos;
                isLandscape = false;
                isPortrait = true;
        }
    }


}
