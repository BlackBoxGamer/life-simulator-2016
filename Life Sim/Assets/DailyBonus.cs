﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;

public class DailyBonus : MonoBehaviour
{
    public DateTime lastTime;
    public DateTime nextTime;

    public int cashReward;

    public int dayMultiplier;

    public Button b;
    public Text t;

    private TimeSpan ts;

    public GameObject notif;

    void Awake()
    {
        dayMultiplier = PlayerStats.daysReturned;

        if (DateTime.Now > nextTime.AddHours(22))
        {
            PlayerStats.daysReturned = 1;
        }
    }

    void Start()
    {
        cashReward = cashReward *= (int)PlayerStats.conversionRate;
    }

    void Update()
    {
        ts = PlayerStats.rewardTime.Subtract(DateTime.Now);

        if (DateTime.Now >= PlayerStats.rewardTime)
        {
            b.interactable = true;
            t.text = "Daily Reward (" + PlayerStats.currency + cashReward * dayMultiplier + ")" ;
        }
        else
        {
            b.interactable = false;
            t.text = ts.Hours + ":" + ts.Minutes + ":" + ts.Seconds;
        }
    }

    public void GetReward()
    {
        lastTime = System.DateTime.Now;
        nextTime = System.DateTime.Now.AddHours(22);
        PlayerStats.rewardTime = nextTime;

        PlayerStats.money += cashReward * PlayerStats.daysReturned;

        PlayerStats.daysReturned++;
    }




	
}
