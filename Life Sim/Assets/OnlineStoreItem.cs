﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OnlineStoreItem : MonoBehaviour
{
    public float price;

    public Sprite canBuyImage;
    public Sprite cantBuyImage;

    bool isOwned;

    void Start()
    {
        GetComponentInChildren<Text>().text = "Buy £" + price.ToString();
    }

    void Update()
    {
        if (!isOwned)
        {
            if (PlayerStats.money >= price)
            {
                GetComponent<Button>().interactable = true;
                GetComponent<Image>().sprite = canBuyImage;
            }
            else
            {
                GetComponent<Button>().interactable = false;
                GetComponent<Image>().sprite = cantBuyImage;
            }
        }
        else
        {
            GetComponent<Button>().interactable = false;
            GetComponent<Image>().sprite = cantBuyImage;
            GetComponentInChildren<Text>().text = "Owned";
        }
    }

    public void Buy()
    {
        PlayerStats.money -= price;
        isOwned = true;
    }

    public void BuyWorgensTale()
    {
        PlayerInventory.bookWorgensTale = true;
    }

    public void BuyUnforgiven()
    {
        PlayerInventory.bookUnforgiven = true;
    }

    public void BuyLoopLife()
    {
        PlayerInventory.bookLoopOfLife = true;
    }
}
