﻿using UnityEngine;
using System.Collections;

public class PlantGrowthHandler : MonoBehaviour
{
    public float maxPlantHealth = 10f;

    public int daysWithoutWater;

    public GameObject healthy;
    public GameObject withered;
    public GameObject dying;
    public GameObject dead;

    public int counter;

    public GameObject notification;

    public float health;

    void Start()
    {
        PlayerInventory.hasPlant = true;
    }

    void Update()
    {
        health = PlayerStats.plantHealth;

        if (counter <= 0)
        {
            if (notification.activeSelf)
            {
                // Do nothing
            }
            else
            {
                notification.SetActive(true);
            }
        }
        else
        {
            if (notification.activeSelf)
            {
                notification.SetActive(false);
            }
            else
            {
                // Do nothing
            }
        }

        if (PlayerStats.plantHealth > maxPlantHealth)
        {
            PlayerStats.plantHealth = maxPlantHealth;
        }
    }

    public void DecreaseHealth()
    {
        if ((PlayerStats.plantHealth -= 0.045f) >= 0)
        PlayerStats.plantHealth -= 0.045f;

        counter--;

        if (PlayerStats.plantHealth > 8)
        {
            withered.SetActive(false);
            healthy.SetActive(true);
        }
        else if (PlayerStats.plantHealth > 5 && PlayerStats.plantHealth <= 8)
        {
            healthy.SetActive(false);
            withered.SetActive(true);
        }
        else if (PlayerStats.plantHealth > 2 && PlayerStats.plantHealth <= 5)
        {
            withered.SetActive(false);
            dying.SetActive(true);
        }
        else
        {
            dying.SetActive(false);
            dead.SetActive(true);
        }
    }

    public void WaterPlant()
    {
        if (counter <= 0)
        {

                PlayerStats.plantHealth += 1f;
                counter = 12;

                if (PlayerStats.plantHealth > 8)
                {
                    withered.SetActive(false);
                    healthy.SetActive(true);
                }
                else if (PlayerStats.plantHealth > 5 && PlayerStats.plantHealth <= 8)
                {
                    withered.SetActive(true);
                    dying.SetActive(false);
                }
                else if (PlayerStats.plantHealth > 2 && PlayerStats.plantHealth <= 5)
                {
                    dead.SetActive(false);
                    dying.SetActive(true);
                }
                else
                {
                    dead.SetActive(true);
                }
        }
    }
}
