﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SlotsHandler : MonoBehaviour
{
    // This is the script that handles the slots mini game.

    public GameObject[] values;

    public SlotsSlot slot1;
    public SlotsSlot slot2;
    public SlotsSlot slot3;

    bool lock1;
    bool lock2;
    bool lock3;

    public Button lock1Button;
    public Button lock2Button;
    public Button lock3Button;

    float timer = 10f;

    public Button startButton;


    void Start()
    {
        lock1Button.interactable = false;
        lock2Button.interactable = false;
        lock3Button.interactable = false;

        lock1 = false;
        lock2 = false;
        lock3 = false;

        timer = 10;  
    }

    void Update()
    {
        if (PlayerStats.money >= 1)
        {
            startButton.interactable = true;
        }
        else
        {
            startButton.interactable = false;
        }
    }

    public void PlaySlots()
    {
        PlayerStats.money -= 1;

        InvokeRepeating("ChangeSlots", 0f, 0.05f);
        startButton.interactable = false;
        lock1Button.interactable = false;
        lock2Button.interactable = false;
        lock3Button.interactable = false;
    }

    public void LockSlot(int slot)
    {
        if (slot == 1)
        {
            lock1 = true;
            lock1Button.interactable = false;
        }

        if (slot == 2)
        {
            lock2 = true;
            lock2Button.interactable = false;
        }

        if (slot == 3)
        {
            lock3 = true;
            lock3Button.interactable = false;
        }
    }

    void ChangeSlots()
    {
        timer -= 0.5f;

        if (timer > 0 && !lock1)
        {
            SlotsSlot slot = RandomSlot();
            slot1.img.sprite = slot.img.sprite;
            slot1.value = slot.value;        
        }

         if (timer > 0 && !lock2)
        {
            SlotsSlot slot = RandomSlot();
            slot2.img.sprite = slot.img.sprite;
            slot2.value = slot.value;
        }

         if (timer > 0 && !lock3)
        {
            SlotsSlot slot = RandomSlot();
            slot3.img.sprite = slot.img.sprite;
            slot3.value = slot.value;
        }
        
         if (timer <= 0)
        {
            CancelInvoke();
            Finish();
        }
    }

    SlotsSlot RandomSlot()
    {
        return values[Random.Range(0, values.Length)].GetComponent<SlotsSlot>();
    }

    void Finish()
    {
        if (slot1.value == slot2.value && slot1.value == slot3.value)
        {
            if (slot1.value == 1)
            {
                PlayerStats.money += 5;
            }
            else if (slot1.value == 2)
            {
                PlayerStats.money += 10;
            }
            else if (slot1.value == 3)
            {
                PlayerStats.money += 15;
            }
            else if (slot1.value == 4)
            {
                PlayerStats.money += 25;
            }
            else if (slot1.value == 5)
            {
                PlayerStats.money += 50;
            }
            else if (slot1.value == 6)
            {
                PlayerStats.money += 100;
            }
            else if (slot1.value == 7)
            {
                PlayerStats.money += 150;
            }
        }
        else
        {
            if (lock1)
            {
                lock1 = false;
                lock1Button.interactable = false;
            }
            else { lock1Button.interactable = true; lock1 = false; }

            if (lock2)
            {
                lock2 = false;
                lock2Button.interactable = false;
            }
            else { lock2Button.interactable = true; lock2 = false; }

            if (lock3)
            {
                lock3 = false;
                lock3Button.interactable = false;
            }
            else { lock3Button.interactable = true; lock3 = false; }
        }

        startButton.interactable = true;
        timer = 10f;

        if (lock3)
        {
            lock3 = false;
            lock3Button.interactable = false;
        }

        if (lock2)
        {
            lock2 = false;
            lock2Button.interactable = false;
        }

        if (lock1)
        {
            lock1 = false;
            lock1Button.interactable = false;
        }
    }


}
