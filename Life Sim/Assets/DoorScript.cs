﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DoorScript : MonoBehaviour
{
    public GameObject button1;
    public GameObject button2;
    public GameObject button3;
    public GameObject button4;
    public GameObject button5;
    public GameObject button6;
    public GameObject button7;
    public GameObject button8;
    public GameObject button9;

    EasyTween b1Anim;
    EasyTween b2Anim;
    EasyTween b3Anim;
    EasyTween b4Anim;
    EasyTween b5Anim;
    EasyTween b6Anim;
    EasyTween b7Anim;
    EasyTween b8Anim;
    EasyTween b9Anim;

    bool open;

    void Start()
    {
        b1Anim = button1.GetComponent<EasyTween>();
        b2Anim = button2.GetComponent<EasyTween>();
        if (button3 != null)
            b3Anim = button3.GetComponent<EasyTween>();
        if (button4 != null)
            b4Anim = button4.GetComponent<EasyTween>();
        if (button5 != null)
            b5Anim = button5.GetComponent<EasyTween>();
        if (button6 != null)
            b6Anim = button6.GetComponent<EasyTween>();
        if (button7 != null)
            b7Anim = button7.GetComponent<EasyTween>();
        if (button8 != null)
            b8Anim = button8.GetComponent<EasyTween>();
        if (button9 != null)
            b9Anim = button9.GetComponent<EasyTween>();

    }


    public void EnableButtons()
    {
        if (!open)
        {
            StartCoroutine(ShowButtonAnimations());
        }
        else
        {
            StartCoroutine(HideButtonAnimations());
        }
    }

    IEnumerator ShowButtonAnimations()
    {
        GetComponent<Button>().interactable = false;

        open = true;

        button1.SetActive(true);
        b1Anim.OpenCloseObjectAnimation();

        yield return new WaitForSeconds(0.25f);

        button2.SetActive(true);
        b2Anim.OpenCloseObjectAnimation();

        yield return new WaitForSeconds(0.25f);

        if (button3 != null)
        {
            button3.SetActive(true);
            b3Anim.OpenCloseObjectAnimation();

            if (button4 != null)
            yield return new WaitForSeconds(0.25f);
        }

        if (button4 != null)
        {
            button4.SetActive(true);
            b4Anim.OpenCloseObjectAnimation();

            if (button5 != null)
                yield return new WaitForSeconds(0.25f);
        }

        if (button5 != null)
        {
            button5.SetActive(true);
            b5Anim.OpenCloseObjectAnimation();

            if (button6 != null)
                yield return new WaitForSeconds(0.25f);
        }

        if (button6 != null)
        {
            button6.SetActive(true);
            b6Anim.OpenCloseObjectAnimation();

            if (button7 != null)
                yield return new WaitForSeconds(0.25f);
        }

        if (button7 != null)
        {
            button7.SetActive(true);
            b7Anim.OpenCloseObjectAnimation();

            if (button8 != null)
                yield return new WaitForSeconds(0.25f);
        }

        if (button8 != null)
        {
            button8.SetActive(true);
            b8Anim.OpenCloseObjectAnimation();

            if (button9 != null)
                yield return new WaitForSeconds(0.25f);
        }

        if (button9 != null)
        {
            button9.SetActive(true);
            b9Anim.OpenCloseObjectAnimation();
        }

        GetComponent<Button>().interactable = true;
    }


    IEnumerator HideButtonAnimations()
    {
        GetComponent<Button>().interactable = false;

        open = false;

        if (button9 != null)
        {
            b9Anim.OpenCloseObjectAnimation();

            yield return new WaitForSeconds(0.25f);

            button9.SetActive(false);
        }

        if (button8 != null)
        {
            b8Anim.OpenCloseObjectAnimation();

            yield return new WaitForSeconds(0.25f);

            button8.SetActive(false);
        }

        if (button7 != null)
        {
            b7Anim.OpenCloseObjectAnimation();

            yield return new WaitForSeconds(0.25f);

            button7.SetActive(false);
        }

        if (button6 != null)
        {
            b6Anim.OpenCloseObjectAnimation();

            yield return new WaitForSeconds(0.25f);

            button6.SetActive(false);
        }

        if (button5 != null)
        {
            b5Anim.OpenCloseObjectAnimation();

            yield return new WaitForSeconds(0.25f);

            button5.SetActive(false);
        }

        if (button4 != null)
        {
            b4Anim.OpenCloseObjectAnimation();

            yield return new WaitForSeconds(0.25f);

            button4.SetActive(false);
        }

        if (button3 != null)
        {
            b3Anim.OpenCloseObjectAnimation();

            yield return new WaitForSeconds(0.25f);
            button3.SetActive(false);
        }

        b2Anim.OpenCloseObjectAnimation();

        yield return new WaitForSeconds(0.25f);
        button2.SetActive(false);

        b1Anim.OpenCloseObjectAnimation();

        yield return new WaitForSeconds(0.25f);

        button1.SetActive(false);

        GetComponent<Button>().interactable = true;
    }

}
