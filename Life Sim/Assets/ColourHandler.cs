﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ColourHandler : MonoBehaviour
{
    public Camera bg;
    public GameObject stats;
    public GameObject settingsPanel;
    public GameObject optionsBar;

    public GameObject home1;
    public GameObject home2;
    public GameObject home3;

    public Color background;
    public Color statBar;
    public Color panels;

    [Header("Purple")]
    public Color purpleBG;
    public Color purpleStat;
    public Color purplePanel;
    public Transform purpleButton;

    [Header("Grey")]
    public Color greyBG;
    public Color greyStat;
    public Color greyPanel;
    public Transform greyButton;

    [Header("Maroon")]
    public Color maroonBG;
    public Color maroonStat;
    public Color maroonPanel;
    public Transform maroonButton;

    [Header("Brown")]
    public Color brownBG;
    public Color brownStat;
    public Color brownPanel;
    public Transform brownButton;

    [Header("Green")]
    public Color greenBG;
    public Color greenStat;
    public Color greenPanel;
    public Transform greenButton;

    [Header("Black")]
    public Color blackBG;
    public Color blackStat;
    public Color blackPanel;
    public Transform blackButton;

    [Header("Blue")]
    public Color blueBG;
    public Color blueStat;
    public Color bluePanel;
    public Transform blueButton;

    [Header("Purple 2")]
    public Color purpleBG2;
    public Color purpleStat2;
    public Color purplePanel2;
    public Transform purpleButton2;

    void Start()
    {
        bg.backgroundColor = statBar;
        stats.GetComponent<Image>().color = statBar;
        settingsPanel.GetComponent<Image>().color = background;
        optionsBar.GetComponent<Image>().color = statBar;
    }

    public void UpdateColours(Transform tick)
    {
        bg.backgroundColor = statBar;
        stats.GetComponent<Image>().color = statBar;
        settingsPanel.GetComponent<Image>().color = background;
        optionsBar.GetComponent<Image>().color = statBar;
        home1.GetComponent<Image>().color = panels;
        home2.GetComponent<Image>().color = panels;
        home3.GetComponent<Image>().color = panels;

        purpleButton.GetChild(0).gameObject.SetActive(false);
        greyButton.GetChild(0).gameObject.SetActive(false);
        maroonButton.GetChild(0).gameObject.SetActive(false);
        brownButton.GetChild(0).gameObject.SetActive(false);
        blueButton.GetChild(0).gameObject.SetActive(false);
        blackButton.GetChild(0).gameObject.SetActive(false);
        purpleButton2.GetChild(0).gameObject.SetActive(false);
        greenButton.GetChild(0).gameObject.SetActive(false);

        tick.GetChild(0).gameObject.SetActive(true);
    }

    public void SetGrey()
    {
        bg.backgroundColor = greyStat;
        stats.GetComponent<Image>().color = greyStat;

        background = greyBG;
        statBar = greyStat;
        panels = greyPanel;

        UpdateColours(greyButton);
    }

    public void SetPurple()
    {
        bg.backgroundColor = purpleStat;
        stats.GetComponent<Image>().color = purpleStat;

        background = purpleBG;
        statBar = purpleStat;
        panels = purplePanel;

        UpdateColours(purpleButton);
    }

    public void SetMaroon()
    {
        bg.backgroundColor = maroonStat;
        stats.GetComponent<Image>().color = maroonStat;

        background = maroonBG;
        statBar = maroonStat;
        panels = maroonPanel;

        UpdateColours(maroonButton);
    }

    public void SetBrown()
    {
        bg.backgroundColor = brownStat;
        stats.GetComponent<Image>().color = brownStat;

        background = brownBG;
        statBar = brownStat;
        panels = brownPanel;

        UpdateColours(brownButton);
    }

    public void SetGreen()
    {
        bg.backgroundColor = greenStat;
        stats.GetComponent<Image>().color = greenStat;

        background = greenBG;
        statBar = greenStat;
        panels = greenPanel;

        UpdateColours(greenButton);
    }

    public void SetBlack()
    {
        bg.backgroundColor = blackStat;
        stats.GetComponent<Image>().color = blackStat;

        background = blackBG;
        statBar = blackStat;
        panels = blackPanel;

        UpdateColours(blackButton);
    }

    public void SetBlue()
    {
        bg.backgroundColor = blueStat;
        stats.GetComponent<Image>().color = blueStat;

        background = blueBG;
        statBar = blueStat;
        panels = bluePanel;

        UpdateColours(blueButton);
    }

    public void SetPurple2()
    {
        bg.backgroundColor = purpleStat2;
        stats.GetComponent<Image>().color = purpleStat2;

        background = purpleBG2;
        statBar = purpleStat2;
        panels = purplePanel2;

        UpdateColours(purpleButton2);
    }

}
