﻿using UnityEngine;
using System.Collections;

public class GameCheck : MonoBehaviour
{
    // This script checks to see if a book is owned.

    public GameObject snakeGame;
    public GameObject buyOnline;

    void Start()
    {
        CheckGames();
    }

    public void CheckGames()
    {
        if (PlayerInventory.hasLaptop)
            buyOnline.SetActive(true);
        if (PlayerInventory.gameSnake)
            snakeGame.SetActive(true);
    }
}
