﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LampHandler : MonoBehaviour
{
    public Image overlayImage;

    public playerInfoHandler LM;

    public bool on;

    public void LightOn()
    {
        if (!LM.lightOn)
        {
            overlayImage.CrossFadeAlpha(LM.afternoonColor.a, 0.5f, false);
            LM.lightOn = true;
            on = true;
        }
        else
        {
            LM.lightOn = false;
            overlayImage.CrossFadeAlpha(LM.LM.currentColour.a, 0.5f, false);
            on = false;
        }
    }
}
