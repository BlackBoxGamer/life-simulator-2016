﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class BankHandler : MonoBehaviour
{
    public Text amountText;
    public Text inBankText;
    public Text inHandText;
    public Text interestGainedText;

    public float interestRateOffline;
    public float interestRateOnline;

    public float increment;
    public float amount;

    public InputField amountField;
    public Button depositButton;
    public Button withdrawButton;

    void Start()
    {
        amountField.text = increment.ToString();

        if (PlayerStats.bankMoney - (int)PlayerStats.bankMoney != 0)
        {
            inBankText.text = "£" + PlayerStats.bankMoney.ToString("F2");
        }
        else
        {
            inBankText.text = "£" + PlayerStats.bankMoney.ToString();
        }

        if (PlayerStats.money - (int)PlayerStats.money != 0)
        {
            inHandText.text = "£" + PlayerStats.money.ToString("F2");
        }
        else
        {
            inHandText.text = "£" + PlayerStats.money.ToString();
        }
        if (PlayerStats.interestGained - (int)PlayerStats.interestGained != 0)
        {
            interestGainedText.text = "Interest Gained: £" + PlayerStats.interestGained.ToString("F2");
        }
        else
        {
            interestGainedText.text = "Interest Gained: £" + PlayerStats.interestGained.ToString();
        }

        OfflineInterestGain();
    }

    void OfflineInterestGain()
    {
        Debug.Log("Adding interest");

        TimeSpan ts = DateTime.Now.Subtract(PlayerStats.lastPlayed);
        Debug.Log(ts);
        Debug.Log(ts.Hours);
        int hours = ts.Hours;

        interestRateOffline = hours * 0.005f;

        PlayerStats.interestGained = PlayerStats.interestGained + (PlayerStats.bankMoney * interestRateOffline);

        Debug.Log(PlayerStats.interestGained.ToString());

        if (PlayerStats.interestGained - (int)PlayerStats.interestGained != 0)
        {
            interestGainedText.text = "Interest Gained: £" + PlayerStats.interestGained.ToString("F2");
        }
        else
        {
            interestGainedText.text = "Interest Gained: £" + PlayerStats.interestGained.ToString();
        }
    }

    void Update()
    {
        if (amount >= 0)
        {
            if (PlayerStats.money < amount)
            {
                depositButton.interactable = false;
            }
            else if (PlayerStats.money - amount >= 0)
            {
                depositButton.interactable = true;
            }

            if (PlayerStats.bankMoney < amount)
            {
                withdrawButton.interactable = false;
            }
            else if (PlayerStats.bankMoney - amount >= 0)
            {
                withdrawButton.interactable = true;
            }
        }
        else
        {
            amount = 0;
            amountField.text = "£" + amount.ToString();
        }
    }

    public void Withdraw()
    {
        PlayerStats.bankMoney -= amount;
        PlayerStats.money += amount;
        if (PlayerStats.bankMoney - (int)PlayerStats.bankMoney != 0)
        {
            inBankText.text = "£" + PlayerStats.bankMoney.ToString("F2");
        }
        else
        {
            inBankText.text = "£" + PlayerStats.bankMoney.ToString("F2");
        }

        if (PlayerStats.money - (int)PlayerStats.money != 0)
        {
            inHandText.text = "£" + PlayerStats.money.ToString("F2");
        }
        else
        {
            inHandText.text = "£" + PlayerStats.money.ToString("F2");
        }
    }

    public void Deposit()
    {
        PlayerStats.bankMoney += amount;
        PlayerStats.money -= amount;

        if (PlayerStats.bankMoney - (int)PlayerStats.bankMoney != 0)
        {
            inBankText.text = "£" + PlayerStats.bankMoney.ToString("F2");
        }
        else
        {
            inBankText.text = "£" + PlayerStats.bankMoney.ToString("F2");
        }

        if (PlayerStats.money - (int)PlayerStats.money != 0)
        {
            inHandText.text = "£" + PlayerStats.money.ToString("F2");
        }
        else
        {
            inHandText.text = "£" + PlayerStats.money.ToString("F2");
        }
    }

    public void Collect()
    {
        PlayerStats.bankMoney += PlayerStats.interestGained;
        PlayerStats.interestGained = 0;

        if (PlayerStats.interestGained - (int)PlayerStats.interestGained != 0)
        {
            interestGainedText.text = "Interest Gained: £" + PlayerStats.interestGained.ToString("F2");
        }
        else
        {
            interestGainedText.text = "Interest Gained: £" + PlayerStats.interestGained.ToString();
        }

        if (PlayerStats.bankMoney - (int)PlayerStats.bankMoney != 0)
        {
            inBankText.text = "£" + PlayerStats.bankMoney.ToString("F2");
        }
        else
        {
            inBankText.text = "£" + PlayerStats.bankMoney.ToString();
        }
    }

    public void IncreaseAmount()
    {
        amount += increment;
        amountField.text = "£" + amount.ToString();
    }

    public void DecreaseAmount()
    {
        if ((amount - increment) >= 0)
        {
            amount -= increment;
            amountField.text = "£" + amount.ToString();
        }
    }

    public void SetInputValue()
    {
        amount = float.Parse(amountField.text);

        Debug.Log(amount);
    }
}
