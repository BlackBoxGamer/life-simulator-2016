﻿using UnityEngine;
using System.Collections;

public class Tutorial : MonoBehaviour
{
    public GameObject homeTutorial;
    public GameObject locationTutorial;
    public GameObject workTutorial;

    public GameObject activeGO;



    public void Switch(GameObject switchTo)
    {
        activeGO.SetActive(false);
        switchTo.SetActive(true);
        activeGO = switchTo;
    }

    public void EnableTutorials()
    {
        PlayerStats.tutorials = true;

        homeTutorial.SetActive(true);
        locationTutorial.SetActive(true);
        workTutorial.SetActive(true);

    }
}
