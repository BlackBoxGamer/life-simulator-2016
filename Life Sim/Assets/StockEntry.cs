﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PlayFab;
using PlayFab.ClientModels;

public class StockEntry : MonoBehaviour 
{
	[Header("Item Info")]
	public string itemID;

	public string itemClass;
	public int numOfItem;

	public string DisplayName;
	public string Description;

	public string tag;

	public string imageUrl;
	public Sprite icon;

	public int cost;
	public int lastCost;

	public string instanceId;
	public string catalogVersion;
	public string customData;

	public string data;

	[Header("Functional Objects")]
	public Text companyHeader;
	public Text companyDesc;

	public Image companyLogo;

	public Text shareCost;
	public Text lastShareCost;
	public Text costText;

	public GameObject lowerArrow;
	public GameObject higherArrow;

	public Button purchaseButton;

	public StockHandler handler;

	public Color increaseColour;
	public Color decreaseColour;

	public GameObject sellSingle;
	public GameObject sellAll;

	public GameObject sellSingleText;
	public GameObject sellAllText;
	public GameObject sellImage;

	public Text earnFromAllText;
	public Text earnFromOneText;

	public GameObject loadScreen;

	public GameObject nonSelectedPanel;
	public GameObject selectedPanel;

	[Header("Extras")]
	public List<ItemInstance> playerInventory;

	public int numOfThisStock;
	public bool hasStock;

	void OnEnable()
	{
		GetComponentInChildren<Text> ().text = DisplayName;

		StockInfo stockData = JsonUtility.FromJson<StockInfo> (customData);

		Description = stockData.Description;
		lastCost = stockData.Previous;
	}

	public void SelectStock()
	{
		hasStock = false;

		// The UI will be handled from this script, and then this componenet will be passed on to the StockHandler.
		loadScreen.SetActive(true);

		companyHeader.text = DisplayName;
		companyDesc.text = Description;
		companyLogo.sprite = icon;

		shareCost.text = cost.ToString ();
		lastShareCost.text = lastCost.ToString ();

		if (lastCost > cost) {
			lastShareCost.color = decreaseColour;
			lowerArrow.SetActive (true);
			higherArrow.SetActive (false);
		} else {
			lastShareCost.color = increaseColour;
			lowerArrow.SetActive (false);
			higherArrow.SetActive (true);
		}
			
		if (PlayerStats.stockTokens >= cost)
			purchaseButton.interactable = true;
		else
			purchaseButton.interactable = false;

		nonSelectedPanel.SetActive (false);
		selectedPanel.SetActive (true);

		GetInventory ();

		handler.currentlySelectedStock = this.GetComponent<StockEntry> ();
	}

	void GetInventory()
	{
		GetUserInventoryRequest request = new GetUserInventoryRequest () {

		};

		PlayFabClientAPI.GetUserInventory (request, (result) => {
			playerInventory = result.Inventory;
			SortInventory();
		},
			(error) => {
				Debug.Log ("Error creating a list of inventory items");
				Debug.Log (error.ErrorMessage);

			});
	}

	void SortInventory()
	{
		for (int i = 0; i < playerInventory.Count; i++) 
		{
			if (playerInventory [i].ItemId == itemID) 
			{
				hasStock = true;

				instanceId = playerInventory [i].ItemInstanceId;
				numOfItem = playerInventory [i].RemainingUses.Value;

				if (numOfItem > 0)
					UpdateUI (true);
				else
					UpdateUI (false);
				break;
			}
		} 

		if (hasStock)
			return;

		UpdateUI (false);
	}

	void UpdateUI(bool hasMultiple)
	{
		if (hasStock) 
		{
			if (hasMultiple) 
			{
				sellSingle.SetActive (true);
				sellAll.SetActive (true);
				sellSingleText.SetActive (true);
				sellAllText.SetActive (true);
				sellImage.SetActive (true);

				earnFromOneText.text = "+" + cost.ToString ();
				earnFromAllText.text = "+" + (cost * numOfItem).ToString ();

				costText.text = "-" + cost.ToString ();
			} 
			else 
			{
				sellSingle.SetActive (true);
				sellAll.SetActive (false);
				sellSingleText.SetActive (true);
				sellAllText.SetActive (false);
				sellImage.SetActive (true);

				earnFromOneText.text = "+" + cost.ToString ();

				costText.text = "-" + cost.ToString ();
			}
		} 
		else 
		{
			sellSingle.SetActive (false);
			sellAll.SetActive (false);
			sellSingleText.SetActive (false);
			sellAllText.SetActive (false);
			sellImage.SetActive (false);

			costText.text = "-" + cost.ToString ();
		}

		loadScreen.SetActive (false);
	}
}

class StockInfo
{
	public string Description;

	public int Previous;
}
