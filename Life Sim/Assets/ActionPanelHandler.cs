﻿using UnityEngine;
using System.Collections;

public class ActionPanelHandler : MonoBehaviour
{
    public GameObject noSelectionPanel;
    public GameObject lifestylePanel;
    public GameObject entertainmentPanel;
    public GameObject activitiesPanel;
    public GameObject socialPanel;
    public GameObject actionsPanel;

    void Start()
    {
        noSelectionPanel.SetActive(true);

        lifestylePanel.SetActive(false);
        entertainmentPanel.SetActive(false);
        activitiesPanel.SetActive(false);
        if (socialPanel != null)
        socialPanel.SetActive(false);
        if (actionsPanel != null)
        actionsPanel.SetActive(false);
    }

    void ChangePanel(GameObject panelToLoad)
    {
        gameObject.BroadcastMessage("ResetClicks");
        noSelectionPanel.SetActive(false);
        lifestylePanel.SetActive(false);
        entertainmentPanel.SetActive(false);
        activitiesPanel.SetActive(false);
        if (socialPanel != null)
        socialPanel.SetActive(false);
        if (actionsPanel != null)
        actionsPanel.SetActive(false);

        panelToLoad.SetActive(true);

    }

    public void OpenEntertainment()
    {
        ChangePanel(entertainmentPanel);
    }

    public void OpenLifestyle()
    {
        ChangePanel(lifestylePanel);
    }

    public void OpenActivities()
    {
        ChangePanel(activitiesPanel);
    }

    public void OpenSocial()
    {
        ChangePanel(socialPanel);
    }

    public void OpenActions()
    {
        ChangePanel(actionsPanel);
    }

    public void ClosePanels()
    {
        ChangePanel(noSelectionPanel);
    }

}
