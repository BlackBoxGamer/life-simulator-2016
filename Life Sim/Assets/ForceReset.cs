﻿using UnityEngine;
using System.Collections;
using System.IO;

public class ForceReset : MonoBehaviour
{
    public void Reset()
    {
        if (File.Exists(Application.persistentDataPath + "/lifeSim.Life"))
        {
            File.Delete(Application.persistentDataPath + "/lifeSim.Life");
            File.Delete(Application.persistentDataPath + "/inventory.Life");
            File.Delete(Application.persistentDataPath + "/challenges.Life");

            PlayerStats.resetting = true;

            Application.Quit();
        }
    }
}
