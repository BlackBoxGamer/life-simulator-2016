﻿using UnityEngine;
using System.Collections;

public class MenuButtonController : MonoBehaviour 
{
	public Animator anim;

	bool open = true;

	void Start()
	{
		anim = GetComponent<Animator> ();
	}

	public void ChangeState()
	{
		Debug.Log ("Button has been pressed!");
		if (open) 
		{
			anim.SetBool ("open", false);
			anim.SetBool ("close", true);
			open = false;
		} 
		else 
		{
			anim.SetBool ("open", true);
			anim.SetBool ("close", false);
			open = true;
		}
	}
}
