﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InterviewHandler : MonoBehaviour
{
    public JobUI job;
    public JobList jobs;
    public JobInfo selectedJob;

    public GameObject jobPanel;
    public GameObject currentJobPanel;
    public GameObject interviewPanel;

    public GameObject interviewButton;

    public Text questionText;
    public Text answerOneText;
    public Text answerTwoText;
    public Text answerThreeText;
    public Text questionNumberText;

    public string[] questions;

    public string[] answers;

    public int[] correct;

    public int questionNumber = 1;
    public int answerStart = 0;

    public int numberCorrect;

    public GameObject closeButton;

    bool inInterview;

    public void StartInterview()
    {
        jobPanel.SetActive(false);
        interviewPanel.SetActive(true);

        selectedJob = jobs.jobs[PlayerStats.interviewJobID];

        questions = selectedJob.questions;
        answers = selectedJob.answers;
        correct = selectedJob.correct;

        inInterview = true;

        questionNumberText.text = questionNumber + "/5";

        questionText.text = questions[questionNumber - 1];

        answerOneText.text = answers[answerStart];
        answerTwoText.text = answers[answerStart + 1];
        answerThreeText.text = answers[answerStart + 2];
    }

    public void Answer(int answered)
    {
        if (inInterview)
        {
            int correctAnswer = correct[questionNumber - 1];

            if (correctAnswer == answered)
            {
                numberCorrect++;
            }

            questionNumber++;
            answerStart += 3;

            if (answerStart >= 15)
            {
                EndInterview();
                Debug.Log("Ending Interview");
                return;
            }

            questionNumberText.text = questionNumber + "/5";

            questionText.text = questions[questionNumber - 1];

            answerOneText.text = answers[answerStart];
            answerTwoText.text = answers[answerStart + 1];
            answerThreeText.text = answers[answerStart + 2];
        }
    }

    void EndInterview()
    {
        int randomNum = Random.Range(0, 6);

        if (randomNum <= numberCorrect)
        {
            // They got the job!
            questionText.text = "You got the job, congratulations! \n \n You start tomorrow, don't be late!";

            PlayerStats.jobId = selectedJob.id;
            PlayerStats.jobName = selectedJob.jobName;
            PlayerStats.wage = selectedJob.wage;
            PlayerStats.jobDesc = selectedJob.desc;
            PlayerStats.workStartTime = selectedJob.startTime;
            PlayerStats.workEndTime = selectedJob.endTime;
            PlayerStats.lastWorkDay = PlayerStats.days;
            PlayerStats.workEnergyRequired = selectedJob.energyNeeded;
            PlayerStats.hasJob = true;

            job.UpdateText();
        } else
        {
            questionText.text = "Sorry, you're not what we're looking for. You can try again in a few days.";
            PlayerStats.jobApplyDay = PlayerStats.days + selectedJob.applicationDelay;
            selectedJob.nextInterviewDay = PlayerStats.days + selectedJob.applicationDelay;
        }

        interviewButton.SetActive(false);
        PlayerStats.waitingForInterview = false;
        job.interviewTimeText.gameObject.SetActive(false);
        closeButton.SetActive(true);

        // Reset this
        inInterview = false;
        questionNumber = 1;
        answerStart = 0;
    }
}
