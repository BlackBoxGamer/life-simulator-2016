﻿using UnityEngine;
using System.Collections;

public class BookCheck : MonoBehaviour
{
    // This script checks to see if a book is owned.

    public GameObject worgensTale;
    public GameObject unforgiven;
    public GameObject loopLife;
    public GameObject buyOnline;

    void Start()
    {
        CheckBooks();
    }

    public void CheckBooks()
    {
        if (PlayerInventory.bookWorgensTale)
            worgensTale.SetActive(true);
        if (PlayerInventory.bookUnforgiven)
            unforgiven.SetActive(true);
        if (PlayerInventory.bookLoopOfLife)
            loopLife.SetActive(true);

    }

}
