﻿using UnityEngine;
using UnityEngine.UI;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;
using System;

public class AdCashReward : MonoBehaviour, IRewardedVideoAdListener
{
    public DateTime lastTime;
    public DateTime nextTime;

    public int cashReward;

    public Button b;
    public Text t;

    private TimeSpan ts;

    public GameObject notif;
    public GameObject thisNotif;

    bool shownNotif;

    void Update()
    {
        ts = nextTime.Subtract(DateTime.Now);

        if (DateTime.Now >= nextTime)
        {
            b.interactable = true;
            t.text = "Ad Reward (" + PlayerStats.currency + cashReward.ToString() + ")";

            if (!notif.activeSelf)
            {
                notif.SetActive(true);
            }

            if (!thisNotif.activeSelf)
            {
                thisNotif.SetActive(true);
            }
        }
        else
        {
            b.interactable = false;
            t.text = "0" + ts.Hours + ":" + "0" + ts.Minutes + ":" + ts.Seconds;

            if (thisNotif.activeSelf)
            {
                thisNotif.SetActive(false);
                notif.SetActive(false);
            }
        }
    }

    public void WatchAd()
    {
        Appodeal.show(Appodeal.REWARDED_VIDEO);
        Appodeal.setRewardedVideoCallbacks(this);
    }

    #region Rewarded Video callback handlers
    public void onRewardedVideoLoaded() { print("Video loaded"); }
    public void onRewardedVideoFailedToLoad() { print("Video failed"); }
    public void onRewardedVideoShown() { print("Video shown"); }
    public void onRewardedVideoClosed() { print("Video closed"); }
    public void onRewardedVideoFinished(int amount, string name) { Reward(); }
    #endregion

    void Reward()
    {
        lastTime = System.DateTime.Now;
        nextTime = System.DateTime.Now.AddMinutes(5);

        PlayerStats.money += cashReward;
    }
}
