﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HomeActionButtonHandler : MonoBehaviour
{
    public GameObject televisionButton, booksButton, xboxButton, pS4Button, laptopButton;

    void Start()
    {
        CheckItems();
    }

    public void CheckItems()
    {
        if (PlayerInventory.hasStandardTelevision || PlayerInventory.hasFullHdTv)
            televisionButton.SetActive(true);

        if (PlayerInventory.hasBooks)
            booksButton.SetActive(true);

        if (PlayerInventory.hasXboxOne)
            xboxButton.SetActive(true);

        if (PlayerInventory.hasPS4)
            pS4Button.SetActive(true);

        if (PlayerInventory.hasLaptop)
            laptopButton.SetActive(true);
    }


}
