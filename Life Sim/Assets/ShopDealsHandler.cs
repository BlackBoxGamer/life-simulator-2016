﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShopDealsHandler : MonoBehaviour
{
    public GameObject shopItem;
    public ItemCheck updateItems;

    public GameObject itemInfoPanel;
    public Text itemNameText;
    public Text itemDescText;
    public Image itemIconImage;
    public Image itemIconImage2;
    public Image itemIconImage3;
    public Button purchaseButton;
    public Text priceText;

    public Sprite xboxSprite;
    public Sprite ps4Sprite;
    public Sprite tvSprite;
    public Sprite lampSprite;
    public Sprite laptopSprite;
    public Sprite booksSprite;

    public ShopBuy xbox;
    public ShopBuy ps4;
    public ShopBuy tv;
    public ShopBuy lamp;
    public ShopBuy laptop;
    public ShopBuy books;

    bool isXbox;
    bool isPS4;
    bool isTV;
    bool isLamp;
    bool islaptop;
    bool isBooks;

    public string offerName;
    public string offerDesc;
    public float offerPrice;
    public Sprite offerItem1Sprite;
    public Sprite offerItem2Sprite;
    public Sprite offerItem3Sprite;

    public Color canBuyCol;
    public Color cannotBuyCol;

    public int itemsInOffer;
    public string item1Call;
    public string item2Call;
    public string item3Call;

    bool isSelected;

    void Start()
    {
        itemNameText = itemInfoPanel.transform.GetChild(0).GetComponent<Text>();
        itemDescText = itemInfoPanel.transform.GetChild(1).GetComponent<Text>();
        itemIconImage = itemInfoPanel.transform.GetChild(2).GetChild(0).GetComponent<Image>();
        itemIconImage2 = itemInfoPanel.transform.GetChild(2).GetChild(1).GetComponent<Image>();
        itemIconImage3 = itemInfoPanel.transform.GetChild(2).GetChild(2).GetComponent<Image>();
        purchaseButton = itemInfoPanel.transform.GetChild(3).GetComponent<Button>();
        priceText = itemInfoPanel.transform.GetChild(3).GetComponentInChildren<Text>();

        NewOffer();
    }

    public void NewOffer()
    {
        int num = Random.Range(0, 3);

        if (num == 0)
        {
            offerItem1Sprite = xboxSprite;
            offerItem2Sprite = tvSprite;
            offerItem3Sprite = lampSprite;

            offerName = "Late Night Gamer Pack";
            offerDesc = "For all of you players that enjoy to marvel at your games late at night, this is for you! Get all that you need for your stay up late troubles";
            offerPrice = 350;

            item1Call = "XboxOne";
            item2Call = "FullHDTV";
            item3Call = "Lamp";

            itemsInOffer = 3;

            isXbox = true;
            isTV = true;
            isLamp = true;
        }

        if (num == 1)
        {
            offerItem1Sprite = lampSprite;
            offerItem2Sprite = booksSprite;

            offerName = "Late Night Reader";
            offerDesc = "Ever wanted to read after hours? Well this package will make sure that you can! Get a lamp and some books at a discount to fulfill your reading needs.";
            offerPrice = 75;

            item1Call = "Lamp";
            item2Call = "Books";

            itemsInOffer = 2;

            isBooks = true;
            isLamp = true;
        }

        if (num == 2)
        {
            offerItem1Sprite = tvSprite;
            offerItem2Sprite = ps4Sprite;

            offerName = "Gamer";
            offerDesc = "Ever wanted to play video games? Well for that you'll need both a television and a games console, so here they are, at a discounted price too! See you on the Battlefied.";
            offerPrice = 300;

            item1Call = "FullHDTV";
            item2Call = "PS4";

            itemsInOffer = 2;

            isTV = true;
            isPS4 = true;
        }
    }

    public void ShowOffer()
    {
        transform.parent.parent.gameObject.BroadcastMessage("ResetPanel");

        itemNameText.text = offerName;
        itemDescText.text = offerDesc;
        itemIconImage.sprite = offerItem1Sprite;

        if (itemsInOffer >= 2)
        {
            if (itemsInOffer == 2)
                itemIconImage3.gameObject.SetActive(false);
            itemIconImage2.sprite = offerItem2Sprite;
            itemIconImage2.gameObject.SetActive(true);
        }
        if (itemsInOffer == 3)
        {
            itemIconImage3.sprite = offerItem3Sprite;
            itemIconImage3.gameObject.SetActive(true);
        }

        priceText.text = "£" + offerPrice.ToString();

        isSelected = true;

        if (PlayerStats.money >= offerPrice)
        {
            purchaseButton.interactable = true;
            purchaseButton.image.color = canBuyCol;
        }
        else
        {
            purchaseButton.interactable = false;
            purchaseButton.image.color = cannotBuyCol;
        }

        itemInfoPanel.SetActive(true);
        itemInfoPanel.GetComponent<EasyTween>().OpenCloseObjectAnimation();
        Debug.Log("Opened Panel");
    }

    public void BuyItem()
    {
        if (isSelected)
        {
            PlayerStats.money -= offerPrice;
            PlayerStats.moneySpent += offerPrice;
            AlertManager.Alert("-£" + offerPrice.ToString(), Color.red, AlertType.Bottom, 1f);

            PlayerStats.entertainmentItems+= itemsInOffer;

            shopItem.SendMessage(item1Call);
            shopItem.SendMessage(item2Call);
            shopItem.SendMessage(item3Call);

            if (isXbox)
                xbox.EnableBuyState();
            if (isPS4)
                ps4.EnableBuyState();
            if (isTV)
                tv.EnableBuyState();
            if (isLamp)
                lamp.EnableBuyState();
            if (islaptop)
                laptop.EnableBuyState();
            if (isBooks)
                books.EnableBuyState();

            updateItems.UpdateMyItems();

            isSelected = false;
        }
    }

    public void ResetPanel()
    {
        if (itemInfoPanel.activeSelf)
        {
            itemInfoPanel.SetActive(false);
        }

        isSelected = false;
    }
}
