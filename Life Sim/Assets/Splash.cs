﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Splash : MonoBehaviour
{
    public EasyTween anim;

    void Start()
    {
        StartCoroutine("ShowSplash");
    }

    IEnumerator ShowSplash()
    {
        yield return new WaitForSeconds(0.5f);

        anim.OpenCloseObjectAnimation();

        yield return new WaitForSeconds(2.75f);

        anim.OpenCloseObjectAnimation();

        yield return new WaitForSeconds(1.2f);

        LoadScreenManager.LoadScene(1);
    }

}
