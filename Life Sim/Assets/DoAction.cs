﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DoAction : MonoBehaviour
{
    public int energyCost;
    public int hoursTaken;
    public int minutesTaken;

    void Start()
    {
        if (hoursTaken >= 1)
        {
            if (minutesTaken >= 1)
            {
                if (minutesTaken < 10)
                {
                    gameObject.GetComponentInChildren<Text>().text = hoursTaken + ".0" + minutesTaken;
                }
                gameObject.GetComponentInChildren<Text>().text = hoursTaken + "." + minutesTaken;
            }
            gameObject.GetComponentInChildren<Text>().text = hoursTaken.ToString();
        }
        else
        {
            gameObject.GetComponentInChildren<Text>().text = "." + minutesTaken;
        }
    }

    void Update()
    {
        if (energyCost > PlayerStats.energy)
        {
            GetComponent<Button>().interactable = false;
        }
        else
        {
            GetComponent<Button>().interactable = true;
        }
    }

    public void WatchDocumentary()
    {
        PlayerStats.energy -= energyCost;
        PlayerStats.hours += hoursTaken;
        PlayerStats.minutes += minutesTaken;

        PlayerStats.knowledge += 2;

        int r = Random.Range(0, 3);

        if (r == 0)
            PlayerStats.physics++;
        if (r == 1)
            PlayerStats.biology++;
        if (r == 2)
            PlayerStats.chemistry++;

        PlayerStats.mood += 5;
        PlayerStats.social -= 1;
    }

    public void WatchAction()
    {
        PlayerStats.energy -= energyCost;
        PlayerStats.hours += hoursTaken;
        PlayerStats.minutes += minutesTaken;

        PlayerStats.social += 2;
        PlayerStats.mood += 10;

        PlayerStats.knowledge -= 1;
    }

    public void WatchDrama()
    {
        PlayerStats.energy -= energyCost;
        PlayerStats.hours += hoursTaken;
        PlayerStats.minutes += minutesTaken;

        PlayerStats.social += 2;
        PlayerStats.mood += 10;

        PlayerStats.knowledge -= 1;
    }





}
