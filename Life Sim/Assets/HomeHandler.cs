﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HomeHandler : MonoBehaviour
{
    public GameObject homelessHome;
    public GameObject level1Home;
    public GameObject level2Home;
    public GameObject level3Home;

    private bool upgradeToLevel1;
    private bool upgradeToLevel2;
    private bool upgradeToLevel3;

    private bool downgradeToHomeless;
    private bool downgradeToLevel1;
    private bool downgradeToLevel2;

    LevelManager LM;

    void Start()
    {
        LM = GetComponent<LevelManager>();
    }

    public void Upgrade1()
    {
        Refresh();

        LM.homePanel = level1Home;

        GetComponent<CloseTab>().open = level1Home;
        GetComponent<CloseTab>().Open();

        PlayerStats.hasHome = true;
        PlayerInventory.hasLevel1Home = true;
    }

    public void Upgrade2()
    {
        Refresh();

        LM.homePanel = level2Home;

        GetComponent<CloseTab>().open = level2Home;
        GetComponent<CloseTab>().Open();

        PlayerStats.hasHome = true;
        PlayerInventory.hasLevel2Home = true;
    }

    public void Upgrade3()
    {
        Refresh();

        LM.homePanel = level3Home;

        GetComponent<CloseTab>().open = level3Home;
        GetComponent<CloseTab>().Open();

        PlayerStats.hasHome = true;
        PlayerInventory.hasLevel3Home = true;
    }

    void Refresh()
    {
        PlayerInventory.hasLevel1Home = false;
        PlayerInventory.hasLevel2Home = false;
        PlayerInventory.hasLevel3Home = false;
    }

    public void KickOut()
    {
        Refresh();

        LM.homePanel = homelessHome;

        GetComponent<CloseTab>().open = homelessHome;
        GetComponent<CloseTab>().Open();

        PlayerStats.hasHome = false;
        PlayerInventory.hasLevel1Home = false;
        PlayerInventory.hasLevel2Home = false;
        PlayerInventory.hasLevel3Home = false;
        PlayerInventory.hasLevel4Home = false;
    }
}
