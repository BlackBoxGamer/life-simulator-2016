﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HousingCheck : MonoBehaviour
{
    public Button b;
    public Text t;

    void Awake()
    {
        b = GetComponent<Button>();
        t = GetComponentInChildren<Text>();
    }

    void Update()
    {
        if (PlayerStats.hasHome == true)
        {
            ChangePanel(true);
        }
        else
        {
            ChangePanel(false);
        }

    }

    void ChangePanel(bool hasHome)
    {
        if (hasHome)
        {
            b.interactable = true;
            t.text = "Entertainment";
        }
        else
        {
            b.interactable = false;
            t.text = "You need to buy a home first!";
        }
    }


}
