using System.Collections;
using UnityEngine;

public class DemoCube : MonoBehaviour
{
    void Move()
    {
        // Simply utilize MoveTo with a random position
        MoveTo(Random.onUnitSphere * 5f);
    }

    void MoveTo(Vector3 location)
    {
        // Ensures we don't have two coroutines modifying the position at once
        StopAllCoroutines();

        // Start our move animation
        StartCoroutine(DoMove(location));
    }

    IEnumerator DoMove(Vector3 location)
    {
        Vector3 start = transform.position;

        // Wait a frame before starting to move
        yield return null;

        // Animate for 1 second
        const float duration = 1f;

        float t = 0;
        while (t < duration)
        {
            // Update our interpolation value
            t += Time.deltaTime;

            // Lerp between our start and target locations
            transform.position = Vector3.Lerp(start, location, t / duration);

            // Yield control until the next frame
            yield return null;
        }

        // At the end set our position to our target so we end up exactly where we want
        transform.position = location;
    }
}
