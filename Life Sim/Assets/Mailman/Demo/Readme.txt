Demo Instructions
=================

Welcome to the Mailman demo! This demo is pretty simple but that's because Mailman is pretty simple, too. Follow these steps to explore the power of Mailman:


Move
----

1. Open the Demo scene and press play to start the game.
2. Select the cube in the scene.
3. Go up to the "Window" menu and choose "Mailman" to open the Mailman window.
4. In the "Message" field, type "Move" (without the quotes) and then press the "Send Message" button.

This sends a "Move" message to the cube which has a DemoCube script on it. The DemoCube script receives this message and starts a coroutine to move the cube to a random location. Pretty neat, huh?


MoveTo
------

1. Open the Demo scene and press play to start the game.
2. Select the cube in the scene.
3. Go up to the "Window" menu and choose "Mailman" to open the Mailman window.
4. In the "Message" field, type "MoveTo" (without the quotes).
5. Change the "Argument Type" to "Vector3". Enter a value into the XYZ fields that appear.
6. Press "Send Message".

This sends a "MoveTo" message to the cube passing a Vector3 as an argument. The DemoCube script receives this message and starts a coroutine to move the cube to the given position. Mailman lets you attach a number of different types of data to the messagesyou attach

- Float
- Integer
- String
- Vector2
- Vector3
- Vector4
- Quaternion (edited as XYZ euler angles)
- Transform
