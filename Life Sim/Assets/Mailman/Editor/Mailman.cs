//
// Mailman.cs
// (c) 2012, Nick Gravelyn.
//

using UnityEditor;
using UnityEngine;

public class Mailman : EditorWindow
{
    // The message to send
    private string _message = string.Empty;

    // Do we require a receiver?
    private bool _requireReceiver = true;

    // Argument type and all the different argument values.
    private MessageArgumentType _argType = MessageArgumentType.None;
    private float _argFloat;
    private int _argInt;
    private string _argString = string.Empty;
    private Vector2 _argVec2;
    private Vector3 _argVec3;
    private Vector4 _argVec4;
    private Quaternion _argQuaternion;
    private Transform _argTransform;

    [MenuItem("Window/Mailman")]
    static void StartMailman()
    {
        GetWindow<Mailman>().Show();
    }

    void OnEnable()
    {
        ResetData();
        EditorApplication.playmodeStateChanged += OnPlaymodeStateChanged;
    }

    void OnDisable()
    {
        EditorApplication.playmodeStateChanged -= OnPlaymodeStateChanged;
    }

    void OnPlaymodeStateChanged()
    {
        // When the play mode changes, we need to update the GUI
        Repaint();
    }

    void OnSelectionChange()
    {
        // If nothing is selected, reset all our data
        if (Selection.transforms.Length == 0)
        {
            ResetData();
        }

        // Repaint the GUI to update the display information
        Repaint();
    }

    void OnGUI()
    {
        bool guiEnabled = GUI.enabled;
        EditorGUIUtility.LookLikeControls();

        // SendMessage and BroadcastMessage only work if the game is playing
        if (!Application.isPlaying)
        {
            EditorGUILayout.HelpBox("Mailman only works when the game is playing.", MessageType.Info);
        }
        // SendMessage and BroadcastMessage also need a target
        else if (Selection.transforms.Length == 0)
        {
            EditorGUILayout.HelpBox("Mailman only works if you have an object in the scene selected.", MessageType.Info);
        }
        else
        {
            // GUI for the standard data
            _message = EditorGUILayout.TextField(new GUIContent("Message", "The message to send or broadcast to the selected object(s)."), _message);
            _requireReceiver = EditorGUILayout.Toggle(new GUIContent("Require Receiver", "Whether or not to generate an error if no object receives the message."), _requireReceiver);
            _argType = (MessageArgumentType)EditorGUILayout.EnumPopup(new GUIContent("Argument Type", "The type of argument to send with the message."), _argType);

            // Conditionally display GUI for the selected argument type
            switch (_argType)
            {
                case MessageArgumentType.Float: _argFloat = EditorGUILayout.FloatField("Argument", _argFloat); break;
                case MessageArgumentType.Int: _argInt = EditorGUILayout.IntField("Argument", _argInt); break;
                case MessageArgumentType.String: _argString = EditorGUILayout.TextField("Argument", _argString); break;
                case MessageArgumentType.Vector2: _argVec2 = EditorGUILayout.Vector2Field("Argument", _argVec2); break;
                case MessageArgumentType.Vector3: _argVec3 = EditorGUILayout.Vector3Field("Argument", _argVec3); break;
                case MessageArgumentType.Vector4: _argVec4 = EditorGUILayout.Vector4Field("Argument", _argVec4); break;
                case MessageArgumentType.Quaternion: _argQuaternion.eulerAngles = EditorGUILayout.Vector3Field("Argument", _argQuaternion.eulerAngles); break;
                case MessageArgumentType.Transform: _argTransform = (Transform)EditorGUILayout.ObjectField("Argument", _argTransform, typeof(Transform), true); break;
                default: break;
            }

            // Enable the buttons only if there is a message to send
            GUI.enabled = guiEnabled && _message != null && !string.IsNullOrEmpty(_message.Trim());

            // Show two buttons so we can either use SendMessage (which delivers only to the selected objects) or BroadcastMessage
            // (which delivers to the selected objects and all their children).
            GUILayout.BeginHorizontal();
            if (GUILayout.Button(new GUIContent("Send Message", "Sends the message to the selected object(s) using SendMessage.")))
            {
                Deliver(true);
            }
            if (GUILayout.Button(new GUIContent("Broadcast Message", "Broadcasts the message to the selected object(s) using BroadcastMessage.")))
            {
                Deliver(false);
            }
            GUILayout.EndHorizontal();
        }
    }

    /// <summary>
    /// Resets all data to the initial state.
    /// </summary>
    private void ResetData()
    {
        _message = string.Empty;
        _requireReceiver = true;
        _argType = MessageArgumentType.None;
        _argFloat = 0;
        _argInt = 0;
        _argString = string.Empty;
        _argVec2 = new Vector2();
        _argVec3 = new Vector3();
        _argVec4 = new Vector4();
        _argQuaternion = new Quaternion();
        _argTransform = null;

        Repaint();
    }

    /// <summary>
    /// Delivers the message using SendMessage if the argument is 'true' or BroadcastMessage if the argument is 'false'.
    /// </summary>
    /// <param name="send">True to use SendMessage, false to use BroadcastMessage.</param>
    private void Deliver(bool send)
    {
        // Pick the right message options
        SendMessageOptions options = _requireReceiver 
            ? SendMessageOptions.RequireReceiver 
            : SendMessageOptions.DontRequireReceiver;

        foreach (var t in Selection.transforms)
        {
            // Pick the right method to call based on the 'send' parameter
            System.Action<string, object, SendMessageOptions> func = send 
                ? (System.Action<string, object, SendMessageOptions>)t.SendMessage 
                : (System.Action<string, object, SendMessageOptions>)t.BroadcastMessage;

            // Call the function with the right argument
            switch (_argType)
            {
                case MessageArgumentType.None: func(_message, null, options); break;
                case MessageArgumentType.Float: func(_message, _argFloat, options); break;
                case MessageArgumentType.Int: func(_message, _argInt, options); break;
                case MessageArgumentType.String: func(_message, _argString, options); break;
                case MessageArgumentType.Vector2: func(_message, _argVec2, options); break;
                case MessageArgumentType.Vector3: func(_message, _argVec3, options); break;
                case MessageArgumentType.Vector4: func(_message, _argVec4, options); break;
                case MessageArgumentType.Quaternion: func(_message, _argQuaternion, options); break;
                case MessageArgumentType.Transform: func(_message, _argTransform, options); break;
            }
        }
    }

    /// <summary>
    /// Type of argument to use for the message.
    /// </summary>
    private enum MessageArgumentType
    {
        None,
        Float,
        Int,
        String,
        Vector2,
        Vector3,
        Vector4,
        Quaternion,
        Transform
    }
}
