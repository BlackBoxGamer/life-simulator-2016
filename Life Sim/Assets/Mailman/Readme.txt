Mailman
=======

Mailman is a simple Unity editor extension that allows you to manually send or broadcast messages to any game objects in the scene while the game is running. This is great for testing your components' behavior even if you don't yet have anything set up to invoke the code automatically.

Using Mailman is super easy:

1. Go up to the Window menu and choose "Mailman" to open the Mailman window.
2. Start the game.
3. Use the Mailman window to enter the name of the message to send, whether or not you wish to require a receiver, and optionally configure an argument for the message.
4. Press "Send Message" to use Unity's SendMessage function to deliver the message to the selected object(s). Use "Broadcast Message" to use Unity's BroadcastMessage function to deliver the message to the selected object(s) and any of their children.

Mailman allows you to send messages with the following argument types:

- None
- Float
- Integer
- String
- Vector2
- Vector3
- Vector4
- Quaternion (edited as XYZ euler angles)
- Transform

If you need any other features or argument types supported, please feel free to comment in the Unity forum thread for Mailman: http://forum.unity3d.com/threads/161899-Mailman-Deliver-messages-to-your-objects-from-the-editor
