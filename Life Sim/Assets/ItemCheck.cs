﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ItemCheck : MonoBehaviour 
{
    // This script checks if a player has bought the item previsously 
    // then disables the GO based on that

    public bool shopDisplay = false;

    public bool isShopItem;

    public bool isHostelHome;
	public bool isL1Home;
	public bool isL2Home;
	public bool isL3Home;
	public bool isL4Home;
	public bool isL5Home;
	public bool isL6Home;
	public bool isL7Home;

    public GameObject XboxOne;
    public GameObject PS4;
    public GameObject HDTV;
    public GameObject Books;
    public GameObject Laptop;
    public GameObject Lamp;

    void Start()
    {
        UpdateMyItems();
    }

	public void UpdateMyItems () 
	{
        if (PlayerInventory.hasStandardTelevision)
        {
            Enable(null);
            Debug.Log("Player has standard television, enabling.");
        }
        if (PlayerInventory.hasMusicPlayer)
        {
            Enable(null);
            Debug.Log("Player has music player, enabling.");
        }
        if (PlayerInventory.hasLaptop)
        {
            Enable(Laptop);
            Debug.Log("Player has laptop, enabling.");
        }
        if (PlayerInventory.hasInternet)
        {
            Enable(null);
            Debug.Log("Player has internet, enabling.");
        }
        if (PlayerInventory.hasFullHdTv)
        {
            Enable(HDTV);
            Debug.Log("Player has Full HDTV, enabling.");
        }
        if (PlayerInventory.hasGamingPC)
        {
            Enable(null);
            Debug.Log("Player has Gaming PC, enabling.");
        }
        if (PlayerInventory.hasVirtualReality)
        {
            Enable(null);
            Debug.Log("Player has VR Headset, enabling.");
        }
        if (PlayerInventory.hasXboxOne)
        {
            Enable(XboxOne);
            Debug.Log("Player has Xbox One, enabling.");
        }
        if (PlayerInventory.hasPS4)
        {
            Enable(PS4);
            Debug.Log("Player has PS4, enabling.");
        }
        if (PlayerInventory.hasBooks)
        {
            Enable(Books);
            Debug.Log("Player has Books, enabling.");
        }
        if (PlayerInventory.hasLamp)
        {
            Enable(Lamp);
            Debug.Log("Play has Lamp, enabling.");
        }
        if (PlayerInventory.hasTimeMachine)
        {
            Enable(null);
            Debug.Log("Player has Time Machine, enabling.");
        }
	}

	void Enable(GameObject item)
	{
        if (isShopItem)
        {
            item.SendMessage("EnableBuyState");
        }
        else
        {
            item.transform.GetChild(0).gameObject.SetActive(true);
        }
	}

    void Disable()
    {
        if (isShopItem)
        {
            GetComponent<ShopBuy>().isOwned = false;
        }
        else
        {
            transform.GetChild(0).gameObject.SetActive(false);
        }
    }
}
