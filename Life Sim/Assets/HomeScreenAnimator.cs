﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HomeScreenAnimator : MonoBehaviour
{
    public EasyTween energyAnim;
    public EasyTween healthAnim;
    public EasyTween moodAnim;

    public EasyTween curAnim;

    public EasyTween sidePanel;
    public RectTransform homePanel;

    public AnimationCurve animCurve;

    private Button homeButton;

    public GameObject swipe;
    public GameObject backImage;

    void Start()
    {
        homeButton = GetComponent<Button>();
    }

    public void GoHome()
    {
        curAnim.OpenCloseObjectAnimation();

        swipe.SetActive(true);

        backImage.SetActive(false);
        homeButton.interactable = false;
    }

    public void EnergyAnimation()
    {
        homeButton.interactable = true;

        energyAnim.OpenCloseObjectAnimation();
        curAnim = energyAnim;

        swipe.SetActive(false);
        backImage.SetActive(true);
    }

    public void HealthAnimation()
    {
        homeButton.interactable = true;

        healthAnim.OpenCloseObjectAnimation();
        curAnim = healthAnim;

        swipe.SetActive(false);
        backImage.SetActive(true);
    }

    public void MoodAnimation()
    {
        homeButton.interactable = true;

        moodAnim.OpenCloseObjectAnimation();
        curAnim = moodAnim;

        swipe.SetActive(false);
        backImage.SetActive(true);
    }
}
