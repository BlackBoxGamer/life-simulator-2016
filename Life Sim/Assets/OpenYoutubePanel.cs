﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OpenYoutubePanel : MonoBehaviour
{
    public GameObject channelPanel;
    public GameObject createPanel;

    public void OpenYoutube()
    {
        if (YoutubeManager.hasChannel)
        {
            channelPanel.SetActive(true);
            createPanel.SetActive(false);
        }
        else
        {
            createPanel.SetActive(true);
            channelPanel.SetActive(false);
        }
    }
}
