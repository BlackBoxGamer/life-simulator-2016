﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CollegeCourse : MonoBehaviour 
{
	[Header("Info")]
	[Space(10)]
	public int id;
	public string courseName;

	public Sprite availableColour;
	public Sprite unavailableColour;

	public int courseLength;

	public string[] learned;
	public string[] randomEvents;

	[Header("Requirements")]
	[Space(10)]
	public int strengthNeeded;
	public int knowledgeNeeded;
	public int mathsNeeded;
	public int ictNeeded;
	public int englishNeeded;
	public int physicsNeeded;
	public int chemistryNeeded;
	public int biologyNeeded;
	public int geographyNeeded;
	public int historyNeeded;
	public int businessNeeded;
	public int programmingNeeded;
	public int computingNeeded;
	public int economicsNeeded;
	public int calculusNeeded;
	public int artNeeded;
	public int animationNeeded;
	public int lawNeeded;
	public int socialNeeded;
	public int creativityNeeded;

	public bool hasRequirements;

	public bool hasQualification;

	public bool isDrivingLicense;
	public bool isBrickLaying;
	public bool isGamesDesign;
	public bool isBanking;
	public bool isFinance;
	public bool isCatering;
	public bool isTrainLicense;
	public bool isArcheology;
	public bool isFirstAid;

	[Header("Extras")]
	[Space(10)]
	public CourseHandler handler;
	public GameObject reqPanel;
	public Text reqText;
	public Text nedText;

	void Start()
	{
		reqPanel = handler.reqPanel;
		nedText = handler.nedText;
		reqText = handler.reqText;
		Check ();
	}

	public void Check()
	{
		// Check to see what qualification this is, and then check if you already have the requirement.
		if (isDrivingLicense)
		{
			if (PlayerStats.drivingLicense) 
			{
				hasQualification = true;
			} 
			else 
			{
				hasQualification = false;
			}
		}

		if (isBrickLaying)
		{
			if (PlayerStats.brickLaying) 
			{
				hasQualification = true;
			} 
			else 
			{
				hasQualification = false;
			}
		}

		if (isGamesDesign)
		{
			if (PlayerStats.gameDesign) 
			{
				hasQualification = true;
			} 
			else 
			{
				hasQualification = false;
			}
		}

		if (isBanking)
		{
			if (PlayerStats.banking) 
			{
				hasQualification = true;
			} 
			else 
			{
				hasQualification = false;
			}
		}

		if (isFinance)
		{
			if (PlayerStats.finance) 
			{
				hasQualification = true;
			} 
			else 
			{
				hasQualification = false;
			}
		}
		if (isCatering)
		{
			if (PlayerStats.catering) 
			{
				hasQualification = true;
			} 
			else 
			{
				hasQualification = false;
			}
		}
		if (isTrainLicense)
		{
			if (PlayerStats.trainLicense) 
			{
				hasQualification = true;
			} 
			else 
			{
				hasQualification = false;
			}
		}
		if (isArcheology)
		{
			if (PlayerStats.archeology) 
			{
				hasQualification = true;
			} 
			else 
			{
				hasQualification = false;
			}
		}
		if (isFirstAid)
		{
			if (PlayerStats.firstAid) 
			{
				hasQualification = true;
			} 
			else 
			{
				hasQualification = false;
			}
		}

		// Check if requirements are met or not, then adjust accordingly the button accordingly.
		if (PlayerStats.strength >= strengthNeeded &&
		    PlayerStats.knowledge >= knowledgeNeeded &&
		    PlayerStats.maths >= mathsNeeded &&
		    PlayerStats.ict >= ictNeeded &&
		    PlayerStats.english >= englishNeeded &&
		    PlayerStats.physics >= physicsNeeded &&
		    PlayerStats.chemistry >= chemistryNeeded &&
		    PlayerStats.biology >= biologyNeeded &&
		    PlayerStats.geography >= geographyNeeded &&
		    PlayerStats.history >= historyNeeded &&
		    PlayerStats.business >= businessNeeded &&
		    PlayerStats.programming >= programmingNeeded &&
		    PlayerStats.computing >= computingNeeded &&
		    PlayerStats.economics >= economicsNeeded &&
		    PlayerStats.calculus >= calculusNeeded &&
		    PlayerStats.art >= artNeeded &&
		    PlayerStats.animation >= animationNeeded &&
		    PlayerStats.law >= lawNeeded &&
		    PlayerStats.social >= socialNeeded &&
		    PlayerStats.creativity >= creativityNeeded &&
			PlayerStats.onCourse == false && 
			hasQualification == false) 
		{
			hasRequirements = true;
			ChangeButton ();
		} 
		else 
		{
			hasRequirements = false;
			ChangeButton ();
		}
	}

	public void Interact()
	{
		if (hasRequirements) 
		{
			StartCourse ();
		} 
		else 
		{
			ViewRequirements ();
		}
	}

	void ChangeButton()
	{
		if (hasRequirements) 
		{
			GetComponent<Image> ().sprite = availableColour;
		} 
		else 
		{
			GetComponent<Image> ().sprite = unavailableColour;
		}
	}

	void StartCourse()
	{
		// Start the course, requires access to courseHandler and set all courses to not interactable.
		handler.SetCourse(id);
	}

	void ViewRequirements()
	{
		// Player doesn't have the right stats to do the course, show them where they need to improve.
		// Open requirements panel


		reqText.text = FindRequirements ();
		nedText.text = FindNeeded ();
		reqPanel.SetActive (true);
	}

	public void GetQualification()
	{
		if (isDrivingLicense)
		{
			PlayerStats.drivingLicense = true;
		}

		if (isBrickLaying)
		{
			PlayerStats.brickLaying = true;
		}

		if (isGamesDesign)
		{
			PlayerStats.gameDesign = true;
		}

		if (isBanking)
		{
			PlayerStats.banking = true;
		}

		if (isFinance)
		{
			PlayerStats.finance = true;
		}
		if (isCatering)
		{
			PlayerStats.catering = true;
		}
		if (isTrainLicense)
		{
			PlayerStats.trainLicense = true;
		}
		if (isArcheology)
		{
			PlayerStats.archeology = true;
		}
		if (isFirstAid)
		{
			PlayerStats.firstAid = true;
		}
	}

	string FindRequirements()
	{
		string s = "<size=25><color=black>Required</color></size>";

		if (strengthNeeded > 0) 
		{
			s = s + "\n Strength: " + strengthNeeded;
		}

		if (knowledgeNeeded > 0) 
		{
			s = s + "\n Knowledge: " + knowledgeNeeded;
		}

		if (mathsNeeded > 0) 
		{
			s = s + "\n Maths: " + mathsNeeded;
		}

		if (ictNeeded > 0) 
		{
			s = s  + "\n ICT: " + ictNeeded;
		}

		if (englishNeeded > 0) 
		{
			s = s + "\n English: " + englishNeeded;
		}

		if (physicsNeeded > 0) 
		{
			s = s + "\n Physics: " + physicsNeeded;
		}

		if (chemistryNeeded > 0) 
		{
			s = s + "\n Chemistry: " + chemistryNeeded;
		}

		if (biologyNeeded > 0) 
		{
			s = s + "\n Biology: " + biologyNeeded;
		}

		if (geographyNeeded > 0) 
		{
			s = s + "\n Geography: " + geographyNeeded;
		}

		if (historyNeeded > 0) 
		{
			s = s + "\n History: " + historyNeeded;
		}

		if (businessNeeded > 0) 
		{
			s = s + "\n Business: " + businessNeeded;
		}

		if (programmingNeeded > 0) 
		{
			s = s + "\n Programming: " + programmingNeeded;
		}

		if (computingNeeded > 0) 
		{
			s = s + "\n Computing: " + computingNeeded;
		}

		if (economicsNeeded > 0) 
		{
			s = s + "\n Economics: " + economicsNeeded;
		}

		if (calculusNeeded > 0) 
		{
			s = s + "\n Calculus: " + calculusNeeded;
		}

		if (artNeeded > 0) 
		{
			s = s + "\n Art: " + artNeeded;
		}

		if (animationNeeded > 0) 
		{
			s = s + "\n Animation: " + animationNeeded;
		}

		if (lawNeeded > 0) 
		{
			s = s + "\n Law: " + lawNeeded;
		}

		if (socialNeeded > 0) 
		{
			s = s + "\n Social: " + socialNeeded;
		}

		if (creativityNeeded > 0) 
		{
			s = s + "\n Creativity: " + creativityNeeded;
		}
		return s;
	}

	string FindNeeded()
	{
		string s = "<size=22><color=black>Needed</color></size>";

		if (strengthNeeded > 0 && PlayerStats.strength < strengthNeeded) 
		{
			s = s + "\n +" + (strengthNeeded - PlayerStats.strength).ToString ();
		}

		if (knowledgeNeeded > 0 && PlayerStats.knowledge < knowledgeNeeded) 
		{
			s = s + "\n +" + (knowledgeNeeded - PlayerStats.knowledge).ToString();
		}

		if (mathsNeeded > 0 && PlayerStats.maths < mathsNeeded) 
		{
			s = s + "\n +" + (mathsNeeded - PlayerStats.maths).ToString();
		}

		if (ictNeeded > 0 && PlayerStats.ict < ictNeeded) 
		{
			s = s + "\n +" + (ictNeeded - PlayerStats.ict).ToString();
		}

		if (englishNeeded > 0 && PlayerStats.english < englishNeeded) 
		{
			s = s + "\n +" + (englishNeeded - PlayerStats.english).ToString();
		}

		if (physicsNeeded > 0 && PlayerStats.physics < physicsNeeded) 
		{
			s = s + "\n +" + (physicsNeeded - PlayerStats.physics).ToString();
		}

		if (chemistryNeeded > 0 && PlayerStats.chemistry < chemistryNeeded) 
		{
			s = s + "\n +" + (chemistryNeeded - PlayerStats.chemistry).ToString();
		}

		if (biologyNeeded > 0 && PlayerStats.biology < biologyNeeded) 
		{
			s = s + "\n +" + (biologyNeeded - PlayerStats.biology).ToString();
		}

		if (geographyNeeded > 0 && PlayerStats.geography < geographyNeeded) 
		{
			s = s + "\n +" + (geographyNeeded - PlayerStats.geography).ToString();
		}

		if (historyNeeded > 0 && PlayerStats.history < historyNeeded) 
		{
			s = s + "\n +" + (historyNeeded - PlayerStats.history).ToString();
		}

		if (businessNeeded > 0 && PlayerStats.business < businessNeeded) 
		{
			s = s + "\n +" + (businessNeeded - PlayerStats.business).ToString();
		}

		if (programmingNeeded > 0 && PlayerStats.programming < programmingNeeded) 
		{
			s = s + "\n +" + (programmingNeeded - PlayerStats.programming).ToString();
		}

		if (computingNeeded > 0 && PlayerStats.computing < computingNeeded) 
		{
			s = s + "\n +" + (computingNeeded - PlayerStats.computing).ToString();
		}

		if (economicsNeeded > 0 && PlayerStats.economics < economicsNeeded) 
		{
			s = s + "\n +" + (economicsNeeded - PlayerStats.economics).ToString();
		}

		if (calculusNeeded > 0 && PlayerStats.calculus < calculusNeeded) 
		{
			s = s + "\n +" + (calculusNeeded - PlayerStats.calculus).ToString();
		}

		if (artNeeded > 0 && PlayerStats.art < artNeeded) 
		{
			s = s + "\n +" + (artNeeded - PlayerStats.art).ToString();
		}

		if (animationNeeded > 0 && PlayerStats.animation < animationNeeded) 
		{
			s = s + "\n +" + (animationNeeded - PlayerStats.animation).ToString();
		}

		if (lawNeeded > 0 && PlayerStats.law < lawNeeded) 
		{
			s = s + "\n +" + (lawNeeded - PlayerStats.law).ToString();
		}

		if (socialNeeded > 0 && PlayerStats.social < socialNeeded) 
		{
			s = s + "\n +" + (socialNeeded - PlayerStats.social).ToString();
		}

		if (creativityNeeded > 0 && PlayerStats.creativity < creativityNeeded) 
		{
			s = s + "\n +" + (creativityNeeded - PlayerStats.creativity).ToString();
		}

		return s;
	}
}
